import {lazy, FC, Suspense} from 'react'
import {Route, Routes, Navigate} from 'react-router-dom'
import {MasterLayout} from '../../_metronic/layout/MasterLayout'
import TopBarProgress from 'react-topbar-progress-indicator'
import {DashboardWrapper} from '../pages/dashboard/DashboardWrapper'
import {MenuTestPage} from '../pages/MenuTestPage'
import {getCSSVariableValue} from '../../_metronic/assets/ts/_utils'
import {WithChildren} from '../../_metronic/helpers'
import BuilderPageWrapper from '../pages/layout-builder/BuilderPageWrapper'

const PrivateRoutes = () => {
  const ProfilePage = lazy(() => import('../modules/profile/ProfilePage'))
  const MasterPage = lazy(() => import('../modules/master/MaterPage'))
  const SeriesPage = lazy(() => import('../modules/seriesMatches/SeriesPage'))
  const ContestPage = lazy(() => import('../modules/contest/ContestPage'))
  const CurrentPage = lazy(() => import('../modules/current/CurrentPage'))
  const OperatorsPage = lazy(() => import('../modules/operators/OperatorsPage'))
  const Managepage = lazy(() => import('../modules/manage/ManagePage'))
  const UserPage = lazy(() => import('../modules/userManagement/UserPage'))
  const ReportPage = lazy(() => import('../modules/reports/ReportPage'))
  const WalletPage = lazy(() => import('../modules/wallet/walletRoute'))
  const FundsPage = lazy(() => import('../modules/fund/FundPage'))
  const AdvertisementPage = lazy(() => import('../modules/advertisement/Advertisementpage'))
  const NotificationsPage = lazy(() => import('../modules/notifications/NotificationPage'))
  const EnquiryPage = lazy(() => import('../modules/enquiry/EnquiryPage'))
  const MatchPage = lazy(() => import('../modules/matches/MatchPage'))
  const ModeratorPage = lazy(() => import('../modules/moderator/moderator'))
  const PrizePage = lazy(() => import('../modules/prizepool/prizePool'))
  const SportPage = lazy(() => import('../modules/sports/sport'))
  const Profit_Loss_Page = lazy(() => import('../modules/contest-profit-loss/Profit_Loss_Page'))
  const App_Download_Page = lazy(() => import('../modules/app_downloads/App_Download_Page'))
  const SettingPage = lazy(() => import('../modules/setting/SettingPage'))
  const PanelPage = lazy(() => import('../modules/panelNotification/PanelPage'))
  const Offer_Page = lazy(() => import('../modules/offerCode/Offer_page'))
  const WizardsPage = lazy(() => import('../modules/wizards/WizardsPage'))
  const AccountPage = lazy(() => import('../modules/accounts/AccountPage'))
  const WidgetsPage = lazy(() => import('../modules/widgets/WidgetsPage'))
  const ChatPage = lazy(() => import('../modules/apps/chat/ChatPage'))
  const UsersPage = lazy(() => import('../modules/apps/user-management/UsersPage'))

  return (
    <Routes>
      <Route element={<MasterLayout />}>
        {/* Redirect to Dashboard after success login/registartion */}
        <Route path='auth/*' element={<Navigate to='/dashboard' />} />

        {/* Dashboard */}
        <Route
          path='dashboard'
          element={
            <SuspensedView>
              <DashboardWrapper />
            </SuspensedView>
          }
        />

        {/* Lazy Modules */}
        <Route
          path='master/*'
          element={
            <SuspensedView>
              <MasterPage />
            </SuspensedView>
          }
        />
        <Route
          path='series/*'
          element={
            <SuspensedView>
              <SeriesPage />
            </SuspensedView>
          }
        />
        <Route
          path='contest/*'
          element={
            <SuspensedView>
              <ContestPage />
            </SuspensedView>
          }
        />
        <Route
          path='current/*'
          element={
            <SuspensedView>
              <CurrentPage />
            </SuspensedView>
          }
        />
        <Route
          path='operators/*'
          element={
            <SuspensedView>
              <OperatorsPage />
            </SuspensedView>
          }
        />
        <Route
          path='user-management/*'
          element={
            <SuspensedView>
              <UserPage />
            </SuspensedView>
          }
        />
        <Route
          path='reports/*'
          element={
            <SuspensedView>
              <ReportPage />
            </SuspensedView>
          }
        />
        <Route
          path='manage-content/*'
          element={
            <SuspensedView>
              <Managepage />
            </SuspensedView>
          }
        />

        <Route
          path='wallet/*'
          element={
            <SuspensedView>
              <WalletPage />
            </SuspensedView>
          }
        />
        <Route
          path='funds/*'
          element={
            <SuspensedView>
              <FundsPage />
            </SuspensedView>
          }
        />
        <Route
          path='advertisement/*'
          element={
            <SuspensedView>
              <AdvertisementPage />
            </SuspensedView>
          }
        />
        <Route
          path='notifications/*'
          element={
            <SuspensedView>
              <NotificationsPage />
            </SuspensedView>
          }
        />

        <Route
          path='enquiries/*'
          element={
            <SuspensedView>
              <EnquiryPage />
            </SuspensedView>
          }
        />
        <Route
          path='matches/*'
          element={
            <SuspensedView>
              <MatchPage />
            </SuspensedView>
          }
        />
        <Route
          path='moderator/*'
          element={
            <SuspensedView>
              <ModeratorPage />
            </SuspensedView>
          }
        />

<Route
          path='prize_pool/*'
          element={
            <SuspensedView>
              <PrizePage />
            </SuspensedView>
          }
        />
        <Route
          path='sports/*'
          element={
            <SuspensedView>
              <SportPage />
            </SuspensedView>
          }
        />

        <Route
          path='contest_p_l/*'
          element={
            <SuspensedView>
              <Profit_Loss_Page />
            </SuspensedView>
          }
        />
        <Route
          path='app-download/*'
          element={
            <SuspensedView>
              <App_Download_Page />
            </SuspensedView>
          }
        />

        <Route
          path='settings/*'
          element={
            <SuspensedView>
              <SettingPage />
            </SuspensedView>
          }
        />

        <Route
          path='panel-notification/*'
          element={
            <SuspensedView>
              <PanelPage />
            </SuspensedView>
          }
        />

        <Route
          path='offer/*'
          element={
            <SuspensedView>
              <Offer_Page />
            </SuspensedView>
          }
        />

        <Route
          path='crafted/pages/wizards/*'
          element={
            <SuspensedView>
              <WizardsPage />
            </SuspensedView>
          }
        />
        <Route
          path='crafted/widgets/*'
          element={
            <SuspensedView>
              <WidgetsPage />
            </SuspensedView>
          }
        />
        <Route
          path='crafted/account/*'
          element={
            <SuspensedView>
              <AccountPage />
            </SuspensedView>
          }
        />
        <Route
          path='apps/chat/*'
          element={
            <SuspensedView>
              <ChatPage />
            </SuspensedView>
          }
        />
        <Route
          path='apps/user-management/*'
          element={
            <SuspensedView>
              <UsersPage />
            </SuspensedView>
          }
        />
        {/* Page Not Found */}
        <Route path='*' element={<Navigate to='/error/404' />} />
      </Route>
    </Routes>
  )
}

const SuspensedView: FC<WithChildren> = ({children}) => {
  const baseColor = getCSSVariableValue('--bs-primary')
  TopBarProgress.config({
    barColors: {
      '0': baseColor,
    },
    barThickness: 1,
    shadowBlur: 5,
  })
  return <Suspense fallback={<TopBarProgress />}>{children}</Suspense>
}

export {PrivateRoutes}
