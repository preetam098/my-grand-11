
const API_URL = process.env.REACT_APP_API_URL


// -------------PROFILE -----------//

export const GET_USER_DETAIL = `${API_URL}/auth/account`

// -------------UPDATE & DELETE -----------//

export const UPDATE_USER = `${API_URL}/auth/account`
export const DELETE_USER = `${API_URL}/public/account`

// ------------- Matches --------------- //

export const GET_MATCHES = `${API_URL}/auth/matches`
export const GET_MATCH_DETAIL = `${API_URL}/auth/match`


// -------------Moderator --------------- //

export const GET_MODERATOR = `${API_URL}/auth/account/moderators`
export const CREATE_MODERATOR = `${API_URL}/auth/account/moderator`

// -------------USER MANAGEMENT ----------- //

export const GET_ALL_ACCOUNT = `${API_URL}/auth/accounts`
export const CREATE_USER_ACCOUNT = `${API_URL}/public/account`
export const GET_USER_INFO = `${API_URL}/public/account/number`

// ------------ SPORTS ----------- //

export const CREATE_SPORTS = `${API_URL}/auth/sport`
export const GET_ALL_SPORTS = `${API_URL}/auth/sports`
export const UPDATE_SPORTS = `${API_URL}/auth/sport`
export const DELETE_SPORTS = `${API_URL}/auth/sport`
export const GET_SPORT_DETAIL = `${API_URL}/auth/sport`


// ------------ SPORTS_FORMAT----------- //

export const GET_ALL_SPORTS_FORMAT = `${API_URL}/auth/sportFormats`
export const CREATE_SPORTS_FORMAT = `${API_URL}/auth/sportFormat`
export const UPDATE_SPORTS_FORMAT = `${API_URL}/auth/sportFormat`
export const DELETE_SPORTS_FORMAT = `${API_URL}/auth/sportFormat`
export const GET_SPORT_FORMAT_DETAIL = `${API_URL}/auth/sportFormat`


// ------------ PRIZE_POOL----------- //

export const GET_PRIZE_POOLS = `${API_URL}/auth/pools`
export const CREATE_PRIZE_POOL = `${API_URL}/auth/pool`
export const UPDATE_PRIZE_POOL = `${API_URL}/auth/pool`
export const DELETE_PRIZE_POOL = `${API_URL}/auth/pool`
export const GET_PRIZE_DISTRIBUTION = `${API_URL}/auth/pool/prize`
export const GET_PRIZE_POOL= `${API_URL}/auth/pool`






