import { SPORTS_FORMAT_SUCCESS , SPORTS_FORMAT , SPORTS_FORMAT_FAIL} from '../actions/index'


const intialState = {
  loading: false,
  data: [],
}

const sportFormatReducer = (state = intialState, action) => {
  switch (action.type) {
    case SPORTS_FORMAT:
      return {...state, loading: true}
    case SPORTS_FORMAT_SUCCESS:
      return {...state, loading: false, data: action?.payload}
    case SPORTS_FORMAT_FAIL:
      return {...state, loading: false}
    default:
      return state
  }
}

export default sportFormatReducer
