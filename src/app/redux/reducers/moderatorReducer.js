import {MODERATOR, MODERATOR_SUCCESS, MODERATOR_FAIL} from '../actions/index'

const intialState = {
  loading: false,
  data: [],
}

const moderatorReducer = (state = intialState, action) => {
  switch (action.type) {
    case MODERATOR:
      return {...state, loading: true}
    case MODERATOR_SUCCESS:
      return {...state, loading: false, data: action?.payload}
    case MODERATOR_FAIL:
      return {...state, loading: false}
    default:
      return state
  }
}

export default moderatorReducer
