import {
   PROFILE_USER,
   PROFILE_USER_SUCCESS,
   PROFILE_USER_FAIL
    } from "../actions/index" 

    const intialState = {
      loading: false,
      data:[],
    };   

    const profileReducer = (state = intialState, action) => {
      switch (action.type) {
        case PROFILE_USER:
          return { ...state, loading: true };
        case PROFILE_USER_SUCCESS:
          return { ...state, loading: false , data:action?.payload};
        case PROFILE_USER_FAIL:
          return { ...state, loading: false };
        default:
          return state;
      }
    };
    
    export default profileReducer;
    