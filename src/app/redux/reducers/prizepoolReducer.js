import {PRIZE_POOL, PRIZE_POOL_SUCCESS, PRIZE_POOL_FAIL} from '../actions/index'

const intialState = {
  loading: false,
  data: [],
}

const prizePoolReducer = (state = intialState, action) => {
  switch (action.type) {
    case PRIZE_POOL:
      return {...state, loading: true}
    case PRIZE_POOL_SUCCESS:
      return {...state, loading: false, data: action?.payload}
    case PRIZE_POOL_FAIL:
      return {...state, loading: false}
    default:
      return state
  }
}

export default prizePoolReducer
