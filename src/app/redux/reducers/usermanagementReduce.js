import { USER_ACCOUNT, USER_ACCOUNT_SUCCESS, USER_ACCOUNT_FAIL } from '../actions/index';
  
  const intialState = {
    loading: false,
    data:[],
  };
  
  const userManage = (state = intialState, action) => {
    switch (action.type) {
      case USER_ACCOUNT:
        return { ...state, loading: true };
      case USER_ACCOUNT_SUCCESS:
        return { ...state, loading: false , data:action?.payload };
      case USER_ACCOUNT_FAIL:
        return { ...state, loading: false };
      default:
        return state;
    }
  };
  
  export default userManage;
  