import {combineReducers} from 'redux'
import userManage from './usermanagementReduce'
import moderatorReducer from './moderatorReducer'
import profileReducer from './profileReduce'
import sportReducer from './sportReducer'
import sportFormatReducer from '../reducers/sportFormat'
import matchesReducer from './matchReducer'
import prizePoolReducer from './prizepoolReducer'

const rootReducer = combineReducers({
  profileReducer,
  matchesReducer,
  userManage,
  moderatorReducer,
  sportReducer,
  sportFormatReducer,
  prizePoolReducer,
})

export default rootReducer
