import { SPORTS_SUCCESS , SPORTS , SPORTS_FAIL} from '../actions/index'


const intialState = {
  loading: false,
  data: [],
}

const sportFormatReducer = (state = intialState, action) => {
  switch (action.type) {
    case SPORTS:
      return {...state, loading: true}
    case SPORTS_SUCCESS:
      return {...state, loading: false, data: action?.payload}
    case SPORTS_FAIL:
      return {...state, loading: false}
    default:
      return state
  }
}

export default sportFormatReducer
