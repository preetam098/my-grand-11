import {MATCHES, MATCHES_SUCCESS, MATCHES_FAIL} from '../actions/index'

const intialState = {
  loading: false,
  data: [],
}

const matchesReducer = (state = intialState, action) => {
  switch (action.type) {
    case MATCHES:
      return {...state, loading: true}
    case MATCHES_SUCCESS:
      return {...state, loading: false, data: action?.payload}
    case MATCHES_FAIL:
      return {...state, loading: false}
    default:
      return state
  }
}

export default matchesReducer
