import {MODERATOR, MODERATOR_SUCCESS, MODERATOR_FAIL} from './index'
import {CREATE_MODERATOR, GET_MODERATOR , UPDATE_USER ,DELETE_USER} from '../../utils/api'
import axios from 'axios'
import {callApi} from '../../../_metronic/helpers/api'
import toast, {Toaster} from 'react-hot-toast'

export const getModerator = ( payload , callback) => async (dispatch) => {
  dispatch({type: MODERATOR})
  try {
    const response = await callApi(`${GET_MODERATOR}`,payload,"GET", )
    dispatch({type: MODERATOR_SUCCESS, payload: response?.data})
    callback()
  } catch (error) {
    dispatch({type:MODERATOR_FAIL, payload: error})
  }
}

export const createModerator = (payload, callback) => async (dispatch) => {
  dispatch({type: MODERATOR})
  try {
    const response = await callApi(CREATE_MODERATOR, payload, 'POST')
    if (response?.status === 200) {
      dispatch({type: MODERATOR_SUCCESS, payload: response.data})
      callback(response)
    } else {
      const errorMessage = response?.error?.message || 'Unknown error occurred'
      dispatch({type: MODERATOR_FAIL, payload: errorMessage})
      toast.error(errorMessage)
    }
  } catch (error) {
    const errorMessage = error.response.data.error.message || 'Unknown error occurred'
    console.log('catch error', errorMessage)
    dispatch({type: MODERATOR_FAIL, payload: errorMessage})
    toast.error(errorMessage)
  }
}


export const updateModerator = (id, payload, mask, callback) => async (dispatch) => {
  dispatch({ type: MODERATOR });
  try {
    const payloadWithMask = { ...payload, mask }; 
    const response = await callApi(`${UPDATE_USER}/${id}`, payloadWithMask, 'PUT');
    dispatch({ type: MODERATOR_SUCCESS, payload: response?.data });
    callback(response?.data);
  } catch (error) {
    dispatch({ type: MODERATOR_FAIL, payload: error });
  }
};


export const deleteModerator = (id , payload ,callback ) => async (dispatch) => {
  dispatch({type: MODERATOR})
  try {
    const response = await callApi(`${DELETE_USER}/${id}` , payload , "DELETE")
    dispatch({type: MODERATOR_SUCCESS, payload: response?.data})
    callback()
  } catch (error) {
    dispatch({type: MODERATOR_FAIL, payload: error})
  }
}
