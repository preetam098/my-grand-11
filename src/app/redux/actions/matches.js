import { callApi } from "../../../_metronic/helpers/api"
import {GET_MATCHES} from "../../utils/api"
import { MATCHES_SUCCESS , MATCHES_FAIL , MATCHES } from "./index"


export const getMatches = ( payload , callback) => async (dispatch) => {
  
    dispatch({type: MATCHES})
    try {
      const response = await callApi(`${GET_MATCHES}`,payload,"GET", )
      dispatch({type: MATCHES_SUCCESS, payload: response?.data})
      callback()
    } catch (error) {
      dispatch({type: MATCHES_FAIL, payload: error})
    }
  }
  