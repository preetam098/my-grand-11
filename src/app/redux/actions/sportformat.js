import toast from 'react-hot-toast'
import {callApi} from '../../../_metronic/helpers/api'
import {GET_ALL_SPORTS_FORMAT, CREATE_SPORTS_FORMAT, UPDATE_SPORTS_FORMAT, DELETE_SPORTS_FORMAT} from '../../utils/api'
import {SPORTS_FORMAT, SPORTS_FORMAT_SUCCESS, SPORTS_FORMAT_FAIL} from './index'

export const getSportFormatAction = (payload, callback) => async (dispatch) => {
  dispatch({type: SPORTS_FORMAT})
  try {
    const response = await callApi(`${GET_ALL_SPORTS_FORMAT}`, payload, 'GET')
    dispatch({type: SPORTS_FORMAT_SUCCESS, payload: response?.data})
    callback()
  } catch (error) {
    dispatch({type:SPORTS_FORMAT_FAIL, payload: error})
  }
}

export const createSportFormatAction = (payload, callback) => async (dispatch) => {
  dispatch({type: SPORTS_FORMAT})
  try {
    const response = await callApi(CREATE_SPORTS_FORMAT, payload, 'POST')
    if (response?.status === 200) {
      dispatch({type: SPORTS_FORMAT_SUCCESS, payload: response.data})
      callback()
    } else {
      const errorMessage = response?.error?.message || 'Unknown error occurred'
      dispatch({type: SPORTS_FORMAT_FAIL, payload: errorMessage})
      toast.error(errorMessage)
    }
  } catch (error) {
    const errorMessage = error.response.data.error.message || 'Unknown error occurred'
    console.log('catch error', errorMessage)
    dispatch({type: SPORTS_FORMAT_FAIL, payload: errorMessage})
    toast.error(errorMessage)
  }
}

export const updateSportFormatAction = (id, payload, mask, callback) => async (dispatch) => {
  dispatch({type: SPORTS_FORMAT})
  try {
    const payloadWithMask = {...payload, mask}
    const response = await callApi(`${UPDATE_SPORTS_FORMAT}/${id}`, payloadWithMask, 'PUT')
    dispatch({type: SPORTS_FORMAT_SUCCESS, payload: response?.data})
    callback()
  } catch (error) {
    dispatch({type: SPORTS_FORMAT_FAIL, payload: error})
  }
}

export const deleteSportFormatAction = (id, payload, callback) => async (dispatch) => {
  dispatch({type: SPORTS_FORMAT})
  try {
    const response = await callApi(`${DELETE_SPORTS_FORMAT}/${id}`, payload, 'DELETE')
    dispatch({type: SPORTS_FORMAT_SUCCESS, payload: response?.data})
    callback()
  } catch (error) {
    dispatch({type: SPORTS_FORMAT_FAIL, payload: error})
  }
}
