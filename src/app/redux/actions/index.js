// ------------- USER ---------------//

export const PROFILE_USER = 'PROFILE_USER'
export const PROFILE_USER_SUCCESS = 'PROFILE_USER_SUCCESS'
export const PROFILE_USER_FAIL = 'PROFILE_USER_FAIL'

// --------------MATCHES ------------//

export const MATCHES = 'MATCHES'
export const MATCHES_SUCCESS = 'MATCHES_SUCCESS'
export const MATCHES_FAIL = 'MATCHES_FAIL'


// --------------MODERATOR ------------//

export const MODERATOR = 'MODERATOR'
export const MODERATOR_SUCCESS = 'MODERATOR_SUCCESS'
export const MODERATOR_FAIL = 'MODERATOR_FAIL'

// --------------USER MANAGEMENT ------------//

export const USER_ACCOUNT = 'USER_ACCOUNT';
export const USER_ACCOUNT_SUCCESS = 'USER_ACCOUNT_SUCCESS';
export const USER_ACCOUNT_FAIL = 'USER_ACCOUNT_FAIL';

// --------------SPORTS ------------//

export const SPORTS = 'SPORTS';
export const SPORTS_SUCCESS = 'SPORTS_SUCCESS';
export const SPORTS_FAIL = 'SPORTS_FAIL';


// --------------SPORTS_FORMAT ------------//

export const SPORTS_FORMAT = 'SPORTS_FORMAT';
export const SPORTS_FORMAT_SUCCESS = 'SPORTS_FORMAT_SUCCESS';
export const SPORTS_FORMAT_FAIL = 'SPORTS_FORMAT_FAIL';


// --------------PRIZE_POOL ------------//

export const PRIZE_POOL = 'PRIZE_POOL';
export const PRIZE_POOL_SUCCESS = 'PRIZE_POOL_SUCCESS';
export const PRIZE_POOL_FAIL = 'PRIZE_POOL_FAIL';