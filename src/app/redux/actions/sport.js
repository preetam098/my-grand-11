import toast from 'react-hot-toast'
import {callApi} from '../../../_metronic/helpers/api'
import {GET_ALL_SPORTS, CREATE_SPORTS, UPDATE_SPORTS, DELETE_SPORTS} from '../../utils/api'
import {SPORTS_SUCCESS, SPORTS, SPORTS_FAIL} from './index'

export const getSportsAction = (payload, callback) => async (dispatch) => {
  dispatch({type: SPORTS})
  try {
    const response = await callApi(`${GET_ALL_SPORTS}`, payload, 'GET')
    dispatch({type: SPORTS_SUCCESS, payload: response?.data})
    callback()
  } catch (error) {
    dispatch({SPORTS_FAIL, payload: error})
  }
}

export const createSportAction = (payload, callback) => async (dispatch) => {
  dispatch({type: SPORTS})
  try {
    const response = await callApi(CREATE_SPORTS, payload, 'POST')
    if (response?.status === 200) {
      dispatch({type: SPORTS_SUCCESS, payload: response.data})
      callback()
    } else {
      const errorMessage = response?.error?.message || 'Unknown error occurred'
      dispatch({type: SPORTS_FAIL, payload: errorMessage})
      toast.error(errorMessage)
    }
  } catch (error) {
    const errorMessage = error.response.data.error.message || 'Unknown error occurred'
    console.log('catch error', errorMessage)
    dispatch({type: SPORTS_FAIL, payload: errorMessage})
    toast.error(errorMessage)
  }
}

export const updateSportAction = (id, payload, mask, callback) => async (dispatch) => {
  dispatch({type: SPORTS})
  try {
    const payloadWithMask = {...payload, mask}
    const response = await callApi(`${UPDATE_SPORTS}/${id}`, payloadWithMask, 'PUT')
    dispatch({type: SPORTS_SUCCESS, payload: response?.data})
    callback()
  } catch (error) {
    dispatch({type: SPORTS_FAIL, payload: error})
  }
}

export const deleteSportAction = (id, payload, callback) => async (dispatch) => {
  dispatch({type: SPORTS})
  try {
    const response = await callApi(`${DELETE_SPORTS}/${id}`, payload, 'DELETE')
    dispatch({type: SPORTS_SUCCESS, payload: response?.data})
    callback()
  } catch (error) {
    dispatch({type: SPORTS_FAIL, payload: error})
  }
}
