import {PRIZE_POOL, PRIZE_POOL_SUCCESS, PRIZE_POOL_FAIL} from './index'
import {
  GET_PRIZE_DISTRIBUTION,
  GET_PRIZE_POOLS,
  CREATE_PRIZE_POOL,
  UPDATE_PRIZE_POOL,
  DELETE_PRIZE_POOL,
} from '../../utils/api'
import axios from 'axios'
import {callApi} from '../../../_metronic/helpers/api'
import toast, {Toaster} from 'react-hot-toast'

export const getPrizeDistribution = (payload, callback) => async (dispatch) => {
  dispatch({type: PRIZE_POOL})
  try {
    const response = await callApi(`${GET_PRIZE_DISTRIBUTION}`, payload, 'POST')
    dispatch({type: PRIZE_POOL_SUCCESS, payload: response?.data})
    callback(response)

  } catch (error) {
    dispatch({type: PRIZE_POOL_FAIL, payload: error})
  }
}

export const getPrizePool = (payload ,callback) => async (dispatch) => {
  dispatch({type: PRIZE_POOL})
  try {
    const response = await callApi(`${GET_PRIZE_POOLS}?`,payload , 'GET')
    dispatch({type: PRIZE_POOL_SUCCESS, payload: response?.data})
    callback(response?.data)
  } catch (error) {
    dispatch({type: PRIZE_POOL_FAIL, payload: error})
  }
}

export const createPrizePool = (payload, callback) => async (dispatch) => {
  dispatch({type: PRIZE_POOL})
  try {
    const response = await callApi(`${CREATE_PRIZE_POOL}`, payload, 'POST')
    if (response?.status === 200) {
      dispatch({type: PRIZE_POOL_SUCCESS, payload: response.data})
      callback(response)
    } else {
      const errorMessage = response?.error?.message || 'Unknown error occurred'
      dispatch({type: PRIZE_POOL_FAIL, payload: errorMessage})
      toast.error(errorMessage)
    }
  } catch (error) {
    const errorMessage = error.response.data.error.message || 'Unknown error occurred'
    console.log('catch error', errorMessage)
    dispatch({type: PRIZE_POOL_FAIL, payload: errorMessage})
    toast.error(errorMessage)
  }
}

export const updatePrizePool = (id, payload, mask, callback) => async (dispatch) => {
  dispatch({type: PRIZE_POOL})
  try {
    const payloadWithMask = {...payload, mask}
    const response = await callApi(`${UPDATE_PRIZE_POOL}/${id}`, payloadWithMask, 'PUT')
    dispatch({type: PRIZE_POOL_SUCCESS, payload: response?.data})
    callback(response?.data)
  } catch (error) {
    dispatch({type: PRIZE_POOL_FAIL, payload: error})
  }
}

export const deletePrizePool = (id, payload, callback) => async (dispatch) => {
  dispatch({type: PRIZE_POOL})
  try {
    const response = await callApi(`${DELETE_PRIZE_POOL}/${id}`, payload, 'DELETE')
    dispatch({type: PRIZE_POOL_SUCCESS, payload: response?.data})
    callback()
  } catch (error) {
    dispatch({type: PRIZE_POOL_FAIL, payload: error})
  }
}
