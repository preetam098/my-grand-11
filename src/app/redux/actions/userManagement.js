import {

  USER_ACCOUNT,
  USER_ACCOUNT_SUCCESS,
  USER_ACCOUNT_FAIL,
} from './index'
import {GET_ALL_ACCOUNT, CREATE_USER_ACCOUNT, UPDATE_USER, DELETE_USER} from '../../utils/api'
import {callApi} from '../../../_metronic/helpers/api'
import toast, {Toaster} from 'react-hot-toast'



export const getUserAction = (payload) => async (dispatch) => {
  dispatch({type: USER_ACCOUNT})
  try {
    const response = await callApi(GET_ALL_ACCOUNT, payload)
    dispatch({type: USER_ACCOUNT_SUCCESS, payload: response?.data})
  } catch (error) {
    dispatch({type: USER_ACCOUNT_FAIL, payload: error})
  }
}

export const createUserAction = (payload , callback) => async (dispatch) => {
  dispatch({type: USER_ACCOUNT})
  try {
      const response = await callApi(CREATE_USER_ACCOUNT, payload, 'POST')
      if (response?.status === 200) {
        dispatch({type: USER_ACCOUNT_SUCCESS, payload: response?.data})
        callback(response)
      } else {
        const errorMessage = response?.error?.message || 'Unknown error occurred'
        dispatch({type: USER_ACCOUNT_FAIL, payload: errorMessage})
        toast.error(errorMessage)
      }
    } catch (error) {
      const errorMessage = error.response.data.error.message || 'Unknown error occurred'
    
      dispatch({type: USER_ACCOUNT_FAIL, payload: errorMessage})
      toast.error(errorMessage)
    }
  }


  export const updateUserAction = (id, payload, mask, callback) => async (dispatch) => {
    dispatch({ type: USER_ACCOUNT });
    try {
      const payloadWithMask = { ...payload, mask }; 
      const response = await callApi(`${UPDATE_USER}/${id}`, payloadWithMask, 'PUT');
      dispatch({ type: USER_ACCOUNT_SUCCESS, payload: response?.data });
      callback(response?.data);
    } catch (error) {
      dispatch({ type: USER_ACCOUNT_FAIL, payload: error });
    }
  };

  export const deleteUserAction = (id , payload ,callback ) => async (dispatch) => {
    dispatch({type: USER_ACCOUNT})
    try {
      const response = await callApi(`${DELETE_USER}/${id}` , payload , "DELETE")
      dispatch({type: USER_ACCOUNT_SUCCESS, payload: response?.data})
      callback()
    } catch (error) {
      dispatch({type: USER_ACCOUNT_FAIL, payload: error})
    }
  }
  
  

