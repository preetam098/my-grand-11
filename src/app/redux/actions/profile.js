import { callApi } from '../../../_metronic/helpers/api'
import {GET_USER_DETAIL} from '../../utils/api'
import {PROFILE_USER ,PROFILE_USER_SUCCESS ,PROFILE_USER_FAIL} from '../actions/index'

export const getUserProfile = (id , callback) => async (dispatch) => {
  dispatch({type: PROFILE_USER})
  try {
    const response = await callApi(`${GET_USER_DETAIL}/${id}`)
    console.log(response, "resss")
    dispatch({type: PROFILE_USER_SUCCESS, payload: response?.data})
    callback()
  } catch (error) {
    dispatch({type:PROFILE_USER_FAIL, payload: error})
  }
}

export const updateProfile = (id , payload,callback) => async (dispatch) => {
  dispatch({type: PROFILE_USER})
  try {
    const response = await callApi(`${GET_USER_DETAIL}/${id}` , payload)
    dispatch({type: PROFILE_USER_SUCCESS, payload: response?.data})
    callback()

  } catch (error) {
    dispatch({PROFILE_USER_FAIL, payload: error})
  }
}



