import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import Contest from './component/contest'
import Contestant from './component/contestant'
import Deposit from './component/deposit'
import Employee from './component/employee'
import Invoices from './component/invoices'
import Match from './component/match'
import Transactions from './component/transactions'
import Withdraw from './component/withdraw'



const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'reports',
    path: '/reports',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const OperatorsPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='contest'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Contest</PageTitle>
            <Contest />
          </>
        }
      />
      
      <Route
        path='cont-report'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}> Contestant</PageTitle>
            <Contestant />
          </>
        }
      />
      <Route
        path='deposit'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}> Deposit</PageTitle>
            <Deposit />
          </>
        }
      />
      <Route
        path='employee-logs'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}> Employee</PageTitle>
            <Employee />
          </>
        }
      />
      <Route
        path='invoices'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}> Invoices</PageTitle>
            <Invoices />
          </>
        }
      />
      <Route
        path='match'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}> Match</PageTitle>
            <Match />
          </>
        }
      />
      <Route
        path='transactions'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}> Transactions</PageTitle>
            <Transactions />
          </>
        }
      />
      <Route
        path='Withdraw'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}> Withdraw</PageTitle>
            <Withdraw />
          </>
        }
      />
      <Route index element={<Navigate to='/reports/contest' />} />
    </Route>
  </Routes>
)

export default OperatorsPage
