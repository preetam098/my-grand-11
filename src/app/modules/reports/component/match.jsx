import React, {useState} from 'react'
import {CommonTable} from '../../../component/CommonTable'
import SimpleReactValidator from 'simple-react-validator'
import {SelectComponent} from '../../../component/SelectComponent'

const option = [
  {name: 'option 1', value: 1, label: 'Option 1'},
  {name: 'option 2', value: 2, label: 'Option 2'},
  {name: 'option 3', value: 3, label: 'Option 3'},
  {name: 'option 4', value: 4, label: 'Option 4'},
  {name: 'option 5', value: 5, label: 'Option 5'},
]
const Match = () => {
  const [formValues, setFormValues] = useState({})
  const [errorMessage, setErrorMessage] = useState({})
  const validator = new SimpleReactValidator({})


  const handleSubmit = () => {
    if (validator.allValid()) {
      console.log('chl gya')
      setFormValues('')
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }

  const handleChange = (e) => {
    setFormValues({...formValues, [e.target.name]: e.target.value})
    setErrorMessage({...errorMessage, [e.target.name]: ''})
  }
  return (
    <section >
      <div className='rounded row px-5 md:py-4 mx-1 my-4  bg-body'>
        <div className=' col-md-4 my-2 '>
          <input
            onChange={handleChange}
            type='date'
            name='StartDate'
            value={formValues?.StartDate}
            className='form-control'
            placeholder='Start Date'
          />
          {validator.message('StartDate', formValues?.StartDate, 'required', {
            className: 'text-danger ',
          })}
          <div className='text-danger '>{errorMessage?.StartDate}</div>
        </div>
        <div className=' col-md-4 my-2 '>
          <input
            onChange={handleChange}
            type='date'
            name='EndDate'
            value={formValues?.EndDate}
            className='form-control'
            placeholder='End Date'
          />
          {validator.message('EndDate', formValues?.EndDate, 'required', {
            className: 'text-danger ',
          })}
          <div className='text-danger '>{errorMessage?.EndDate}</div>
        </div>

        <div className='col-md-4 my-2  card-header mb-2 border-0  '>
          <button className='btn mx-2 btn-light-primary' onClick={handleSubmit}>
            Submit
          </button>
        </div>
      </div>
      <CommonTable
        title='Match Report '
        total={100}
        handleOpen={''}
        className='mb-5 mb-xl-8'
        headData={[
          'Match Category',
          'Winning Amount (Total)',
          'Entry Fee',
          'Total Teams',
          'Current Joined Teams',
          ' Left Teams',
          ' Minimum Required Teams',
          'Fee Collection',
        ]}
      />
    </section>
  )
}

export default Match
