import React, {useState} from 'react'
import {CommonTable} from '../../../component/CommonTable'
import SimpleReactValidator from 'simple-react-validator'
import {SelectComponent} from '../../../component/SelectComponent'

const option = [
  {name: 'option 1', value: 1, label: 'Option 1'},
  {name: 'option 2', value: 2, label: 'Option 2'},
  {name: 'option 3', value: 3, label: 'Option 3'},
  {name: 'option 4', value: 4, label: 'Option 4'},
  {name: 'option 5', value: 5, label: 'Option 5'},
]

const Deposit = () => {
  const [formValues, setFormValues] = useState({})
  const [errorMessage, setErrorMessage] = useState({})
  const validator = new SimpleReactValidator({})

  const handleSelect = (e, name) => {
    console.log('e', e.value, name)
    setErrorMessage({...errorMessage, [name]: ''})
    setFormValues({...formValues, [name]: e.value})
  }

  const handleSubmit = () => {
    if (validator.allValid()) {
      console.log('chl gya')
      setFormValues('')
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }
  const handleReset = () => {
    setFormValues('')
    setErrorMessage('')
  }

  const handleChange = (e) => {
    setFormValues({...formValues, [e.target.name]: e.target.value})
    setErrorMessage({...errorMessage, [e.target.name]: ''})
  }
  return (
    <section className=' '>
      <div className='rounded bg-body mb-5'>
        <div className=' row  mx-1 px-5 py-2  my-2  '>
          <div className='col-md-6 mb-2  '>
            <input
              onChange={handleChange}
              type='date'
              name='StartDate'
              value={formValues?.StartDate}
              className='form-control'
              placeholder='Start Date'
            />
            {validator.message('StartDate', formValues?.StartDate, 'required', {
              className: 'text-danger ',
            })}
            <div className='text-danger '>{errorMessage?.StartDate}</div>
          </div>
          <div className='col-md-6 mb-2'>
            <input
              onChange={handleChange}
              type='date'
              name='EndDate'
              value={formValues?.EndDate}
              className='form-control'
              placeholder='End Date'
            />
            {validator.message('EndDate', formValues?.EndDate, 'required', {
              className: 'text-danger ',
            })}
            <div className='text-danger '>{errorMessage?.EndDate}</div>
          </div>
        </div>

          <div className='row px-5 card-header border-0 '>
            <div className='col-md-6 px-5 mb-4 flex-wrap gap-2 d-flex'>

            <div className='col-md-3'>
              <button className='btn btn-light-primary' onClick={handleSubmit}>
                Submit
              </button>
            </div>
            <div className='col-md-3'>
              <button className='btn btn-light-warning' onClick={handleSubmit}>
                Today
              </button>
            </div>
            <div className='col-md-3'>
              <button className='btn  btn-light-warning' onClick={handleSubmit}>
                Yesterday
              </button>
            </div>
            </div>
          </div>
      </div>
      <CommonTable
        title='Deposit'
        total={100}
        handleOpen={''}
        className='mb-5 mb-xl-8'
        headData={['Full Name', 'Email', 'Mobile', 'Amount', 'Date', 'Against']}
      />
    </section>
  )
}

export default Deposit
