import React, {useState} from 'react'
import {CommonTable} from '../../../component/CommonTable'
import SimpleReactValidator from 'simple-react-validator'
import {SelectComponent} from '../../../component/SelectComponent'

const option = [
  {name: 'option 1', value: 1, label: 'Option 1'},
  {name: 'option 2', value: 2, label: 'Option 2'},
  {name: 'option 3', value: 3, label: 'Option 3'},
  {name: 'option 4', value: 4, label: 'Option 4'},
  {name: 'option 5', value: 5, label: 'Option 5'},
]
const Contest = () => {
  const [openForm, setOpenForm] = useState(false)
  const [formValues, setFormValues] = useState({})
  const [errorMessage, setErrorMessage] = useState({})
  const validator = new SimpleReactValidator({})

  const handleSelect = (e, name) => {
    console.log('e', e.value, name)
    setErrorMessage({...errorMessage, [name]: ''})
    setFormValues({...formValues, [name]: e.value})
  }

  const handleSubmit = () => {
    if (validator.allValid()) {
      console.log('chl gya')
      setFormValues('')
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }
  return (
    <section>
      <div className='rounded flex flex-wrap  mx-1 my-4 row bg-body'>
        <div className=' pt-4 px-3 my-auto  '>
          <SelectComponent
            placeholder='Select Match'
            options={option}
            value={formValues?.ForTeam}
            handleChange={(e) => handleSelect(e, 'match')}
          />
          <div> {validator.message('match', formValues?.match, 'required')}</div>{' '}
          <p className='text-danger '>{errorMessage?.match}</p>
          
        </div>
        <div className=' gap-3 card-header border-0 p-5 text-end'>
          <button className='btn btn-sm mx-2 btn-light-primary' onClick={handleSubmit}>
            Submit
          </button>
        </div>
      
      </div>
      <CommonTable
        title='Contest Report '
        total={100}
        handleOpen={''}
        className='mb-5 mb-xl-8'
        headData={[
          'Contest Category',
          'Winning Amount (Total)',
          'Entry Fee',
          'Total Teams',
          'Current Joined Teams',
          ' Left Teams',
          ' Minimum Required Teams',
          'Fee Collection',
        ]}
      />
    </section>
  )
}

export default Contest
