import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import Enquiry from './components/Enquiry'

const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'Enquiry/Complaints',
    path:'/enquiries',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const EnquiryPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='enquiries'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Enquiry / Complants</PageTitle>
            <Enquiry />
          </>
        }
      />
      <Route index element={<Navigate to='enquiries' />} />
    </Route>
  </Routes>
)

export default EnquiryPage
