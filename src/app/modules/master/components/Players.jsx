import React, {useState} from 'react'
import {CommonTable} from '../../../component/CommonTable'
import {SelectComponent} from '../../../component/SelectComponent'
import SimpleReactValidator from 'simple-react-validator'

const option = [
  {name: 'option 1', value: 1, label: 'Option 1'},
  {name: 'option 2', value: 2, label: 'Option 2'},
  {name: 'option 3', value: 3, label: 'Option 3'},
  {name: 'option 4', value: 4, label: 'Option 4'},
  {name: 'option 5', value: 5, label: 'Option 5'},
]

const Players = () => {
  const [openForm, setOpenForm] = useState(false)

  const [formValues, setFormValues] = useState({})
  const [preview, setPreview] = useState('')
  const [errorMessage, setErrorMessage] = useState({})
  const validator = new SimpleReactValidator({
    className: 'text-danger',
    validators: {
      fileSize: {
        message: 'The :attribute must be max 1MB.',
        rule: function (val, maxSize, validator) {
          return val && val.size <= 1048576
        },
      },
    },
  })

  const handleChange = (event) => {
    const {name, value, type} = event.target
    console.log('first', name, value)
    setErrorMessage({...errorMessage, [name]: ''})
    if (type === 'file') {
      setPreview(URL.createObjectURL(event.target.files[0]))
      setFormValues({...formValues, [name]: event.target.files[0]})
    } else {
      setFormValues({...formValues, [name]: value})
    }
  }

  const handleSelect = (e, name) => {
    console.log('e', e.value, name)
    setErrorMessage({...errorMessage, [name]: ''})
    setFormValues({...formValues, [name]: e.value})
  }

  const handleDiscard=()=>{
    setFormValues("")
    setErrorMessage("")
    setOpenForm(false)
  }

  console.log('form', formValues)

  const handleSubmit = () => {
    if (validator.allValid()) {
      console.log('chl gya')
      setOpenForm(false)
      setFormValues('')
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }

  const handleOpen = () => {
    setOpenForm(true)
  }

  return (
    <section>
      {openForm && (
        <section className=' rounded bg-white mb-4 mb-xl-8'>
          <div className='border-1  border-gray-300 border-bottom p-5 '>
            <h4 className='mb-0'>Create New Players</h4>
          </div>
          <div className='px-5 my-5 py-1 d-flex gap-4'>
            <div>
              <input
                onChange={handleChange}
                type='text'
                name='PlayerName'
                value={formValues?.PlayerName}
                className='form-control'
                placeholder='PlayerName'
              />
              {validator.message('PlayerName', formValues?.PlayerName, 'required|string|min:3', {
                className: 'text-danger ',
              })}
              <div className='text-danger '>{errorMessage?.PlayerName}</div>
            </div>
            <div>
              <input
                onChange={handleChange}
                type='text'
                name='Credits'
                value={formValues?.Credits}
                className='form-control'
                placeholder='Credits'
              />
              {validator.message('Credits', formValues?.Credits, 'required|integer', {
                className: 'text-danger ',
              })}
              <div className='text-danger '>{errorMessage?.Credits}</div>
            </div>
            <div>
              <input
                onChange={handleChange}
                type='text'
                name='Points'
                value={formValues?.Points}
                className='form-control'
                placeholder='Points'
              />
              {validator.message('Points', formValues?.Points, 'required|integer', {
                className: 'text-danger ',
              })}
              <div className='text-danger '>{errorMessage?.Points}</div>
            </div>
            <div>
              <input
                onChange={handleChange}
                type='text'
                name='ApiId'
                value={formValues?.ApiId}
                className='form-control'
                placeholder='ApiId'
              />
              {validator.message('ApiId', formValues?.ApiId, 'required|string', {
                className: 'text-danger ',
              })}
              <div className='text-danger '>{errorMessage?.ApiId}</div>
            </div>
          </div>
          <div className='px-5 py-1 row '>
            <div className=' col-md-6'>
              <div className='mb-0'>
                <SelectComponent
                  className='mb-0'
                  placeholder='For Team'
                  options={option}
                  value={formValues?.ForTeam}
                  handleChange={(e) => handleSelect(e, 'ForTeam')}
                />
                <div> {validator.message('ForTeam', formValues?.ForTeam, 'required|string')}</div>{' '}
                <p className='text-danger '>{errorMessage?.ForTeam}</p>
              </div>

              <div className='mb-0'>
                <SelectComponent
                  className={''}
                  placeholder='Batsman'
                  options={option}
                  value={formValues?.Batsman}
                  handleChange={(e) => handleSelect(e, 'Batsman')}
                />
                <p className='text-danger '>{errorMessage?.Batsman}</p>
                <p> {validator.message('Batsman', formValues?.Batsman, 'required|string')}</p>{' '}

              </div>

              <div>
                <SelectComponent
                  className={''}
                  placeholder='Player Country'
                  options={option}
                  value={formValues?.Country}
                  handleChange={(e) => handleSelect(e, 'Country')}
                />
                <div> {validator.message('Country', formValues?.Country, 'required|string')}</div>{' '}
                <p className='text-danger '>{errorMessage?.Country}</p>
              </div>

              {/* <textarea className='form-control' rows={5} placeholder='Description' /> */}
            </div>
            <div className='col-md-6 gap-1  '>
              <div className='gap-1 p-6 h-47vh rounded border text-center border-blue '>
              <div className='align-items-center justify-content-center d-flex flex-column'>
                <label htmlFor='images' className='text-xs p-6 rounded h-44px  border-[1.5px] p-1'>
                  <div>
                    {formValues?.image && (
                      <img
                        src={formValues && !preview ? `${formValues?.image}` : preview}
                        alt='preview'
                        className='h-40px'
                      />
                    )}
                  </div>

                  <div className='my-2'>
                    {formValues?.image ? (
                      formValues?.image?.name
                    ) : (
                      <>
                      <div className='d-flex flex-column align-items-center'>
                        <i className='bi bi-cloud-arrow-up fs-1'></i>
                        <span>Upload Image</span>
                        </div>
                      </>
                    )}
                  </div>
                </label>
                <input
                  autoComplete='off'
                  id='images'
                  type='file'
                  name='image'
                  // accept={acceptImageFileType}
                  onChange={handleChange}
                  className='rounded py-1.5 px-2 d-none outline-none border'
                />
              </div>
              <div> {validator.message('image', formValues?.image, 'required|fileSize')}</div>{' '}
              </div>
              <div className='text-danger '>{errorMessage?.image}</div>
            </div>
          </div>

          <div className='gap-3 card-header border-0 px-5 py-4 text-start'>
            <button className='btn btn-sm mx-2 btn-light-primary' onClick={handleSubmit}>
              Submit
            </button>
            <button onClick={handleDiscard} className='btn btn-sm btn-light-danger'>
              Discard
            </button>
          </div>
        </section>
      )}
      <section>
        <CommonTable
          title='Players'
          total={100}
          handleOpen={handleOpen}
          className='mb-5 mb-xl-8'
          headData={[
            'APi-id',
            'Player Name',
            ' Credits',
            'Points',
            'Player Type',
            'Country',
            ' Team',
          ]}
        />
      </section>
    </section>
  )
}

export default Players
