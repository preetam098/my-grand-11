import React from 'react'
import { CommonTable } from '../../../component/CommonTable'

const option = [
  {value: 'option 1', label: 'Option 1'},
  {value: 'option 2', label: 'Option 2'},
  {value: 'option 3', label: 'Option 3'},
  {value: 'option 4', label: 'Option 4'},
  {value: 'option 5', label: 'Option 5'},
]

const Points= () => {
  return (
    <div>
    <CommonTable title="Points" total={100}  handleOpen={""}className='mb-5 mb-xl-8' headData={['Point Type', 'Point Details', 'T20' , 'ODI' , 'Test']} /> 
    </div>
  )
}

export default Points
