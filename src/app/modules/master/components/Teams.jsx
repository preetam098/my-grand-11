import {useState} from 'react'
import {CommonTable} from '../../../component/CommonTable'

import SimpleReactValidator from 'simple-react-validator'

const Teams = () => {
  const [openForm, setOpenForm] = useState(false)
  const [preview, setPreview] = useState('')
  const [formValues, setFormValues] = useState({})
  const [errorMessage, setErrorMessage] = useState({})
  const validator = new SimpleReactValidator({
    className: 'text-danger',
    validators: {
      fileSize: {
        message: 'The :attribute must be max 1MB.',
        rule: function (val, maxSize, validator) {
          return val && val.size <= 1048576
        },
      },
    },
  })

  const handleChange = (event) => {
    const {name, value, type} = event.target
    console.log('fdv', name, value, event.target)
    setErrorMessage({...errorMessage, [name]: ''})
    if (type === 'file') {
      setPreview(URL.createObjectURL(event.target.files[0]))
      setFormValues({...formValues, [name]: event.target.files[0]})
    } else {
      setFormValues({...formValues, [name]: value})
    }
  }

  const handleOpen = () => {
    setOpenForm(true)
  }

  const handleDiscard = () => {
    setFormValues('')
    setErrorMessage('')
    setOpenForm(false)
  }
  const handleSubmit = () => {
    if (validator.allValid()) {
      console.log('chl gya')
      setOpenForm(false)
      setFormValues('')
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }

  console.log('formValues', formValues)

  return (
    <>
      {openForm && (
        <section className='px-5 rounded bg-white mb-4 mb-xl-8'>
          <div className='border-1  border-gray-300 border-bottom py-5 '>
            <h4 className='mb-0'>Create New Team</h4>
          </div>
          <div className=' mt-5 py-1 row'>
            <div className='col-md-6'>
              <input
                onChange={handleChange}
                type='text'
                name='TeamName'
                value={formValues?.TeamName}
                className='form-control'
                placeholder='Team Name'
              />
              {validator.message('TeamName', formValues?.TeamName, 'required|TeamName', {
                className: 'text-danger ',
              })}
              <div className='text-danger '>{errorMessage?.TeamName}</div>
            </div>
            <div className='col-md-6'>
              <input
                onChange={handleChange}
                type='text'
                name='ShortName'
                value={formValues?.ShortName}
                className='form-control'
                placeholder='Short Name'
              />
              {validator.message('ShortName', formValues?.ShortName, 'required|ShortName')}
              <div className='text-danger '>{errorMessage?.ShortName}</div>
            </div>
          </div>
          <div className=' py-1 row '>
            <div className='col-md-6 mb-2'>
              <textarea
                name='description'
                onChange={handleChange}
                className='form-control'
                value={formValues?.description}
                rows={3}
                placeholder='description'
              />
              {validator.message('description', formValues?.description, 'required|description')}
              <div className='text-danger '>{errorMessage?.description}</div>
            </div>
            <div className='col-md-6 '>
              <div className='gap-1 border rounded border-secondary text-center border-blue '>
                <div>
                  <label
                    htmlFor='images'
                    className='text-xs p-6 d-flex flex-column gap-1 justify-content-center rounded  border-[1.5px] p-1 align-items-center'>
                    {formValues?.image && (
                      <img
                        src={formValues && !preview ? `${formValues?.image}` : preview}
                        alt='preview'
                        className='h-45px  '
                      />
                    )}
                    {formValues?.image ? (
                      formValues?.image?.name
                    ) : (
                      <>
                        <i className='bi bi-cloud-arrow-up fs-1'></i>
                        <span>Upload Image</span>
                      </>
                    )}
                  </label>
                  <input
                    autoComplete='off'
                    id='images'
                    type='file'
                    name='image'
                    // accept={acceptImageFileType}
                    onChange={handleChange}
                    className='rounded py-1.5 px-2 d-none outline-none border'
                  />
                </div>
                <div> {validator.message('image', formValues?.image, 'required|fileSize')}</div>{' '}
              </div>
              <div className='text-danger '>{errorMessage?.image}</div>
            </div>

            <div className='gap-3 card-header border-0 p-5 text-end'>
              <button className='btn btn-sm mx-2 btn-light-primary' onClick={handleSubmit}>
                Submit
              </button>
              <button onClick={handleDiscard} className='btn btn-sm btn-light-danger'>
                Discard
              </button>
            </div>
          </div>
        </section>
      )}

      <section>
        <CommonTable
          title='Series'
          total={100}
          handleOpen={handleOpen}
          className='mb-5 mb-xl-8'
          headData={['Team Name', 'Abbravation', ' Players']}
        />
      </section>
    </>
  )
}

export default Teams
