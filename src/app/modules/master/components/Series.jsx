import React, {useState} from 'react'
import {CommonTable} from '../../../component/CommonTable'
import {SelectComponent} from '../../../component/SelectComponent'
import SimpleReactValidator from 'simple-react-validator'



const Series = () => {
  const [openForm, setOpenForm] = useState(false)

  const [formValues, setFormValues] = useState({})
  const [errorMessage, setErrorMessage] = useState({})
  const validator = new SimpleReactValidator({})

  const handleChange = (e) => {
    console.log("event" , e)
    setFormValues({...formValues, [e.target.name]: e.target.value})
    setErrorMessage({...errorMessage, [e.target.name]: ''})
  }

  console.log("form" , formValues)
  const handleDiscard=()=>{
    setFormValues("")
    setErrorMessage("")
    setOpenForm(false)
  }

  const handleSubmit = () => {
    if (validator.allValid()) {
      console.log('chl gya')
      setOpenForm(false)
      setFormValues('')
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }

  const handleOpen = () => {
    setOpenForm(!openForm)
  }

  return (
    <>
      {openForm && (
        <section className=' rounded bg-white mb-4 mb-xl-8'>
          <div className='border-1  border-gray-300 border-bottom p-5 '>
            <h4 className='mb-0'>Create New Series</h4>
          </div>
          <div className='p-5 mt-2 py-1 row'>
            <div className='col-md-4 mb-2'>
              <input
                onChange={handleChange}
                type='text'
                name='SeriesName'
                value={formValues?.SeriesName}
                className='form-control'
                placeholder='SeriesName'
              />
              {validator.message('SeriesName', formValues?.SeriesName, 'required|string', {
                className: 'text-danger ',
              })}
              <div className='text-danger '>{errorMessage?.SeriesName}</div>
            </div>
            <div className='col-md-4 mb-2'>
              <input
                onChange={handleChange}
                type='text'
                name='ShortName'
                value={formValues?.ShortName}
                className='form-control'
                placeholder='Short Name'
              />
              {validator.message('ShortName', formValues?.ShortName, 'required|string', {
                className: 'text-danger ',
              })}
              <div className='text-danger '>{errorMessage?.ShortName}</div>
            </div>
            <div className='col-md-4 mb-2'>
              <input
                onChange={handleChange}
                type='text'
                name='TotalName'
                value={formValues?.TotalName}
                className='form-control'
                placeholder='Total Name'
              />
              {validator.message('TotalName', formValues?.TotalName, 'required|string', {
                className: 'text-danger ',
              })}
              <div className='text-danger '>{errorMessage?.TotalName}</div>
            </div>
          </div>
          <div className='row px-5 '>  
            <div className='col-md-6 mb-2'>
              <input
                onChange={handleChange}
                type='date'
                name='StartDate'
                value={formValues?.StartDate}
                className='form-control'
                placeholder='Start Date'
              />
              {validator.message('StartDate', formValues?.StartDate, 'required|number', {
                className: 'text-danger ',
              })}
              <div className='text-danger '>{errorMessage?.StartDate}</div>
            </div>
            <div className='col-md-6 mb-2'>
              <input
                onChange={handleChange}
                type='date'
                name='EndDate'
                value={formValues?.EndDate}
                className='form-control'
                placeholder='Total Name'
              />
              {validator.message('EndDate', formValues?.EndDate, 'required|number', {
                className: 'text-danger ',
              })}
              <div className='text-danger '>{errorMessage?.EndDate}</div>
            </div>
          </div>


          <div className='gap-3 card-header border-0 px-5 py-4 text-end'>
            <button className='btn btn-sm mx-2 btn-light-primary' onClick={handleSubmit}>Submit</button>
            <button onClick={handleDiscard} className='btn btn-sm btn-light-danger'>
              Discard
            </button>
          </div>
        </section>
      )}

      <section>
        <CommonTable
          title='Series'
          total={100}
          handleOpen={handleOpen}
          className='mb-5 mb-xl-8'
          headData={['Series Name', 'Total Team', 'Start Date', 'End Date', 'Series Status']}
        />
      </section>
    </>
  )
}

export default Series
