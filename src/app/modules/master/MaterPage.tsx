import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import Teams from './components/Teams'
import Players from './components/Players'
import Points from './components/Points'
import Series from './components/Series'

const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'Master',
    path: '/master',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const MasterPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='teams'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Teams</PageTitle>
            <Teams />
          </>
        }
      />
      <Route
        path='players'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Players</PageTitle>
            <Players />
          </>
        }
      />
      <Route
        path='points'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Points</PageTitle>
            <Points />
          </>
        }
      />
      <Route
        path='series'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Series</PageTitle>
            <Series />
          </>
        }
      />

      <Route index element={<Navigate to='/master/teams' />} />
    </Route>
  </Routes>
)

export default MasterPage
