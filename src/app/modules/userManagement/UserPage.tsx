import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import User from './components/AllUser'

const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'user-management',
    path:'/user-management',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const UserPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='users'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>User Management</PageTitle>
            <User />
          </>
        }
      />
      <Route index element={<Navigate to='users' />} />
    </Route>
  </Routes>
)

export default UserPage
