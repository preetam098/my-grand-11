import React, {useEffect, useState} from 'react'
import {CommonTable} from '../../../component/CommonPagiTable'
import SimpleReactValidator from 'simple-react-validator'
import {useSelector, useDispatch} from 'react-redux'
import {
  getUserAction,
  createUserAction,
  updateUserAction,
  deleteUserAction,
} from '../../../redux/actions/userManagement'
import toast, {Toaster} from 'react-hot-toast'
import Modal from '../../../component/Modal'
import { useLocation, useNavigate } from 'react-router-dom'

const User = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const location = useLocation()
  const pagination = useSelector((state) => state?.userManage?.data?.pagination)
  const getAllAccount = useSelector((state) => state?.userManage?.data?.accounts)
  const [isEdit, setIsEdit] = useState(false)
  const [modals, setModals] = useState(false)
  const [deleteData, setDelete] = useState('')
  const [openForm, setOpenForm] = useState(false)
  const [loading, setLoading] = useState(false)
  const [formValues, setFormValues] = useState({})
  const [errorMessage, setErrorMessage] = useState({})
  const [mask, setMask] = useState([])
  const [params, setparams] = useState({
    start: 0,
    size: 10,
  })

  const handlePrev = () => {
    params.start > 1 &&
      setparams({
        ...params,
        start: params.start - 1,
      })
    navigate(params)
  }

  const handleForw = () => {
    setparams({
      ...params,
      start: params.start + 1,
    })
  }



  useEffect(() => {
    const callBack = () => {}
    const searchParams = new URLSearchParams(location.search)
    const newQueryParams = {
      start: searchParams.get('start') || '',
      size: searchParams.get('size') || '10',
    }
    setparams(newQueryParams)
    dispatch(getUserAction(params, callBack))
  }, [])

  
  
  useEffect(() => {
    const callBack = () => {}
    const queryString = new URLSearchParams(params).toString()
    dispatch(getUserAction(params , callBack))
    navigate(`?${queryString}`)
  }, [params])
  
  // useEffect(() => {
  //   dispatch(getUserAction())
  // }, [])

  const validator = new SimpleReactValidator({})

  const handleOpen = () => {
    setOpenForm(!openForm)
  }

  const handleSubmit = () => {
    if (validator.allValid()) {
      const callback = (response) => {
        toast.success('Account Created SuccessFully')
        setFormValues('')
        dispatch(getUserAction())
      }
      dispatch(createUserAction(formValues, callback))
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }

  const handleDiscard = () => {
    setFormValues('')
    setErrorMessage('')
    setOpenForm(false)
    setIsEdit(false)
  }

  const handleUpdate = () => {
    if (mask.length === 0) {
      handleDiscard()

      }else{
    const callback = (response) => { 
      setFormValues('')
      setOpenForm(false)
      setLoading(false)
      dispatch(getUserAction())
      toast.success('Account Update Successfully')
    }
    dispatch(updateUserAction(formValues.id, formValues, mask, callback))
  }
  }

  const handleEdit = (data) => {
    setFormValues(data)
    setOpenForm(true)
    // Scroll to the top of the screen
    window.scrollTo({top: 0, behavior: 'smooth'})
    setIsEdit(true)
  }

  const handleDeleteUser = () => {
    const callback = () => {
      setModals(false)
      setLoading(false)
      dispatch(getUserAction())
      toast.success('Account Delete Successfull')
    }
    dispatch(deleteUserAction(deleteData.id, deleteData, callback))
  }

  const handleChange = (event) => {
    const {name, value} = event.target
    setFormValues({...formValues, [name]: value})
    setErrorMessage({...errorMessage, [name]: ''})
    if (!mask.includes(name)) {
      setMask([...mask, name])
    }
  }

  const handleModalOpen = (item) => {
    setDelete(item)
    setModals(true)
  }

  const handleCloseModal = (name) => {
    setModals(false)
  }

  return (
    <section>
      <main>
        {openForm && (
          <section className='px-5 rounded bg-white pb-4 mb-4 mb-xl-8'>
            <div className='border-1 mb-4 sm:mb-2 md:mb-2 border-gray-300 border-bottom py-5 '>
              <h4 className='mb-0'>Create New Users</h4>
            </div>
            <div className='row sm:gap-2 md:gap-0 '>
              <div className='col-md-6 mb-2'>
                <input
                  onChange={handleChange}
                  type='text'
                  name='username'
                  disabled={isEdit ? true : false}
                  value={formValues?.username}
                  className='form-control'
                  placeholder='username'
                />
                {validator.message('username', formValues?.username, 'required|string|min:3', {
                  className: 'text-danger ',
                })}
                <div className='text-danger '>{errorMessage?.username}</div>
              </div>

              <div className='col-md-6 mb-2'>
                <input
                  onChange={handleChange}
                  type='text'
                  name='mobile_no'
                  value={formValues?.mobile_no}
                  disabled={isEdit ? true : false}
                  className='form-control'
                  placeholder='mobile_no'
                />
                {validator.message(
                  'mobile_no',
                  formValues?.mobile_no,
                  'required|integer|max:12|min:12',
                  {
                    className: 'text-danger ',
                  }
                )}
                <div className='text-danger '>{errorMessage?.mobile_no}</div>
              </div>
            </div>

            <div className=' row sm:gap-2 md:gap-0 '>
              <div className='col-md-6 mb-2'>
                <input
                  onChange={handleChange}
                  type='text'
                  name='wallet'
                  disabled={isEdit ? true : false}
                  value={formValues?.wallet}
                  className='form-control'
                  placeholder='wallet'
                />
                {validator.message('wallet', formValues?.wallet, 'required|string', {
                  className: 'text-danger ',
                })}
                <div className='text-danger '>{errorMessage?.wallet}</div>
              </div>
              <div className='col-md-6 mb-2'>
                <input
                  onChange={handleChange}
                  type='text'
                  name='role'
                  value={formValues?.role}
                  disabled={isEdit ? true : false}
                  className='form-control'
                  placeholder='role'
                />
                {validator.message('role', formValues?.role, 'required|string')}
                <div className='text-danger '>{errorMessage?.role}</div>
              </div>
            </div>

            <div className=' row sm:gap-2 md:gap-0 '>
              <div className='col-md-6 mb-2'>
                <input
                  onChange={handleChange}
                  type='text'
                  name='aadhar_number'
                  value={formValues?.aadhar_number}
                  className='form-control'
                  placeholder='aadhar_number'
                />
                {validator.message(
                  'aadhar_number',
                  formValues?.aadhar_number,
                  'required|integer|max:12|min:12',
                  {
                    className: 'text-danger ',
                  }
                )}
                <div className='text-danger '>{errorMessage?.aadhar_number}</div>
              </div>

              <div className='col-md-6 mb-2'>
                <input
                  onChange={handleChange}
                  type='text'
                  name='aadhar_number_verified'
                  value={formValues?.aadhar_number_verified}
                  className='form-control'
                  placeholder='aadhar_number_verified'
                />
                {validator.message(
                  'aadhar_number_verified',
                  formValues?.aadhar_number_verified,
                  'required|string'
                )}
                <div className='text-danger '>{errorMessage?.aadhar_number_verified}</div>
              </div>
            </div>

            <div className=' row sm:gap-2 md:gap-0   '>
              <div className='col-md-6 mb-2'>
                <input
                  onChange={handleChange}
                  type='text'
                  name='account_status'
                  value={formValues?.account_status}
                  className='form-control'
                  placeholder='account_status'
                />
                {validator.message('account_status', formValues?.account_status, 'required|string')}
                <div className='text-danger '>{errorMessage?.account_status}</div>
              </div>
            </div>

            <div className='gap-3 card-header border-0 p-5 text-end'>
              {isEdit ? (
                <button className='btn btn-sm mx-2 btn-light-primary' onClick={handleUpdate}>
                  Update
                </button>
              ) : (
                <button className='btn btn-sm mx-2 btn-light-primary' onClick={handleSubmit}>
                  Submit
                </button>
              )}

              <button onClick={handleDiscard} className='btn btn-sm btn-light-danger'>
                Discard
              </button>
            </div>

          </section>
        )}
        <Toaster />
      </main>

      <Modal
        showModal={modals}
        handleClose={handleCloseModal}
        handleModal={handleModalOpen}
        handleDelete={handleDeleteUser}
      />

      <CommonTable
        title='Users'
        total={getAllAccount?.length}
        handleOpen={handleOpen}
        handleEdit={handleEdit}
        handleModel={handleModalOpen}
        className='mb-5 mb-xl-8'
        headData={getAllAccount ? Object.keys(getAllAccount?.[0]) : loading}
        bodyData={getAllAccount}
        handlePrev={handlePrev}
        handleForw={handleForw}
        params={params}
        pagination={pagination}
      />
    </section>
  )
}

export default User
