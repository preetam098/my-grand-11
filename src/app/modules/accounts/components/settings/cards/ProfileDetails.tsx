import React, {useState} from 'react'
import {toAbsoluteUrl} from '../../../../../../_metronic/helpers'
import {IProfileDetails, profileDetailsInitValues as initialValues} from '../SettingsModel'
import * as Yup from 'yup'
import {useFormik} from 'formik'
import {updateProfile} from '../../../../../redux/actions/profile'
import {useDispatch} from 'react-redux'
import {useLocation} from 'react-router-dom'

const profileDetailsSchema = Yup.object().shape({
  username: Yup.string().required('User name is required'),
  id: Yup.string().required('User Id name is required'),
  role: Yup.string().required('Role is required'),
  mobile_no: Yup.string().required('Mobile No is required'),
  wallet: Yup.string().required('Wallet is required'),
  account_status: Yup.string().required('Account status is required'),
  aadhar_number: Yup.string().required('Aadhar Number is required'),
  aadhar_number_verified: Yup.string().required('Aadhar Verified is required'),
})

const ProfileDetails: React.FC = () => {
  const {state} = useLocation()
  console.log(state)
  const dispatch = useDispatch()
  const [data, setData] = useState<IProfileDetails>(initialValues)
  
  const updateData = (fieldsToUpdate: Partial<IProfileDetails>): void => {
    const updatedData = Object.assign(data, fieldsToUpdate)
    setData(updatedData)
  }

  const [loading, setLoading] = useState(false)

  const formik = useFormik<IProfileDetails>({
    initialValues,
    validationSchema: profileDetailsSchema,
    onSubmit: (values) => {
      setLoading(true)
      setTimeout(() => {
        const updatedData = Object.assign(data, values)
        setData(updatedData)
        setLoading(false)
      }, 1000)
    },
  })

  return (
    <div className='card mb-5 mb-xl-10'>
      <div
        className='card-header border-0 cursor-pointer'
        role='button'
        data-bs-toggle='collapse'
        data-bs-target='#kt_account_profile_details'
        aria-expanded='true'
        aria-controls='kt_account_profile_details'
      >
        <div className='card-title m-0'>
          <h3 className='fw-bolder m-0'>Profile Details</h3>
        </div>
      </div>

      <div id='kt_account_profile_details' className='collapse show'>
        <form onSubmit={formik.handleSubmit} noValidate className='form'>
          <div className='card-body border-top p-9'>
            <div className='row mb-6'>
              <label className='col-lg-4 col-form-label fw-bold fs-6'>Avatar</label>
              <div className='col-lg-8'>
                <div
                  className='image-input image-input-outline'
                  data-kt-image-input='true'
                  style={{backgroundImage: `url(${toAbsoluteUrl('/media/avatars/blank.png')})`}}
                >
                  <div
                    className='image-input-wrapper w-125px h-125px'
                    style={{backgroundImage: `url(${toAbsoluteUrl(data.avatar)})`}}
                  ></div>
                </div>
              </div>
            </div>

            <div className='row mb-6'>
              <label className='col-lg-4 col-form-label required fw-bold fs-6'>UserName</label>

              <div className='col-lg-8 fv-row'>
                <input
                  type='text'
                  className='form-control form-control-lg form-control-solid'
                  placeholder='UserName'
                  {...formik.getFieldProps('username')}
                />
                {formik.touched.username && formik.errors.username && (
                  <div className='fv-plugins-message-container'>
                    <div className='fv-help-block'>{formik.errors.username}</div>
                  </div>
                )}
              </div>
            </div>

            <div className='row mb-6'>
              <label className='col-lg-4 col-form-label required fw-bold fs-6'>User Id</label>

              <div className='col-lg-8 fv-row'>
                <input
                  type='text'
                  className='form-control form-control-lg form-control-solid'
                  placeholder='User Id'
                  // disabled={true}
                  {...formik.getFieldProps('id')}
                />
                {formik.touched.id && formik.errors.id && (
                  <div className='fv-plugins-message-container'>
                    <div className='fv-help-block'>{formik.errors.id}</div>
                  </div>
                )}
              </div>
            </div>

            <div className='row mb-6'>
              <label className='col-lg-4 col-form-label fw-bold fs-6'>
                <span className='required'>Role</span>
              </label>

              <div className='col-lg-8 fv-row'>
                <select
                  className='form-select form-select-solid form-select-lg fw-bold'
                  {...formik.getFieldProps('role')}
                >
                  <option value=''>Select Role</option>
                  <option value='AF'>Admin</option>
                  <option value='AX'>Mod</option>
                </select>
                {formik.touched.role && formik.errors.role && (
                  <div className='fv-plugins-message-container'>
                    <div className='fv-help-block'>{formik.errors.role}</div>
                  </div>
                )}
              </div>
            </div>

            <div className='row mb-6'>
              <label className='col-lg-4 col-form-label fw-bold fs-6'>
                <span className='required'>Mobile no</span>
              </label>

              <div className='col-lg-8 fv-row'>
                <input
                  type='text'
                  className='form-control form-control-lg form-control-solid'
                  placeholder='Mobile No'
                  {...formik.getFieldProps('mobile_no')}
                />
                {formik.touched.mobile_no && formik.errors.mobile_no && (
                  <div className='fv-plugins-message-container'>
                    <div className='fv-help-block'>{formik.errors.mobile_no}</div>
                  </div>
                )}
              </div>
            </div>

            <div className='row mb-6'>
              <label className='col-lg-4 col-form-label fw-bold fs-6'>
                <span className='required'>Wallet</span>
              </label>

              <div className='col-lg-8 fv-row'>
                <input
                  type='text'
                  className='form-control form-control-lg form-control-solid'
                  placeholder='wallet'
                  {...formik.getFieldProps('wallet')}
                />
                {formik.touched.wallet && formik.errors.wallet && (
                  <div className='fv-plugins-message-container'>
                    <div className='fv-help-block'>{formik.errors.wallet}</div>
                  </div>
                )}
              </div>
            </div>

            <div className='row mb-6'>
              <label className='col-lg-4 col-form-label fw-bold fs-6'>
                <span className='required'>Account Status</span>
              </label>

              <div className='col-lg-8 fv-row'>
                <input
                  type='text'
                  className='form-control form-control-lg form-control-solid'
                  placeholder='Account Status'
                  {...formik.getFieldProps('account_status')}
                />
                {formik.touched.account_status && formik.errors.account_status && (
                  <div className='fv-plugins-message-container'>
                    <div className='fv-help-block'>{formik.errors.account_status}</div>
                  </div>
                )}
              </div>
            </div>

            <div className='row mb-6'>
              <label className='col-lg-4 col-form-label fw-bold fs-6'>
                <span className='required'>Aadhar Number</span>
              </label>

              <div className='col-lg-8 fv-row'>
                <input
                  type='text'
                  className='form-control form-control-lg form-control-solid'
                  placeholder='Aadhar Number'
                  {...formik.getFieldProps('aadhar_number')}
                />
                {formik.touched.aadhar_number && formik.errors.aadhar_number && (
                  <div className='fv-plugins-message-container'>
                    <div className='fv-help-block'>{formik.errors.aadhar_number}</div>
                  </div>
                )}
              </div>
            </div>

            <div className='row mb-6'>
              <label className='col-lg-4 col-form-label fw-bold fs-6'>
                <span className='required'>Aadhar Number Verified</span>
              </label>

              <div className='col-lg-8 fv-row'>
                {/* <input
                  type='text'
                  className='form-control form-control-lg form-control-solid'
                  placeholder='Aadhar Number'
                  {...formik.getFieldProps('aadhar_number_verified')}
                /> */}

                <a href='#' className='btn btn-sm btn-light-success fw-bolder ms-2 fs-16 py-1 px-3'>
                  true
                </a>
                {formik.touched.aadhar_number_verified && formik.errors.aadhar_number_verified && (
                  <div className='fv-plugins-message-container'>
                    <div className='fv-help-block'>{formik.errors.aadhar_number_verified}</div>
                  </div>
                )}
              </div>
            </div>
          </div>

          <div className='card-footer d-flex justify-content-end py-6 px-9'>
            <button type='submit' className='btn btn-primary' disabled={loading}>
              {!loading && 'Save Changes'}
              {loading && (
                <span className='indicator-progress' style={{display: 'block'}}>
                  Please wait...{' '}
                  <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
                </span>
              )}
            </button>
          </div>
        </form>
      </div>
    </div>
  )
}

export {ProfileDetails}
