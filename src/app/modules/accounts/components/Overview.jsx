/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {useEffect, useState} from 'react'
import {Link, useNavigate} from 'react-router-dom'
import {KTIcon} from '../../../../_metronic/helpers'
import {
  ChartsWidget1,
  ListsWidget5,
  TablesWidget1,
  TablesWidget5,
} from '../../../../_metronic/partials/widgets'
import {useDispatch} from 'react-redux'
import {useSelector} from 'react-redux'
import {getUserProfile} from '../../../redux/actions/profile'

export function Overview() {
  const dispatch = useDispatch()
  const navigate = useNavigate('')
  const [Loading, setLoading] = useState(false)
  const {account} = JSON.parse(localStorage.getItem('user'))
  const userDetail = useSelector((state) => state?.profileReducer?.data)

  console.log(userDetail, 'detail')

  useEffect(() => {
    dispatch(getUserProfile(account?.id))
    setLoading(false)
  }, [])

  const handleEdit = () => {
    navigate('/crafted/account/settings', {
      state: {
        userDetail,
      },
    })
  }

  return (
    <>
      <div className='card mb-5 mb-xl-10' id='kt_profile_details_view'>
        <div className='card-header cursor-pointer'>
          <div className='card-title m-0'>
            <h3 className='fw-bolder m-0'>Profile Details</h3>
          </div>

          <button onClick={handleEdit} className='btn btn-primary align-self-center'>
            Edit Profile
          </button>
        </div>

        {userDetail ? (
          <>
            <div className='card-body p-9'>
              <div className='row mb-7'>
                <label className='col-lg-4 fw-bold text-muted'>UserName</label>

                <div className='col-lg-8'>
                  <span className='fw-bolder fs-6 text-dark'> Rohan</span>
                </div>
              </div>

              <div className='row mb-7'>
                <label className='col-lg-4 fw-bold text-muted'>UserId</label>

                <div className='col-lg-8'>
                  <span className='fw-bolder fs-6 text-dark'> {userDetail?.id}</span>
                </div>
              </div>

              <div className='row mb-7'>
                <label className='col-lg-4 fw-bold text-muted'>Role</label>

                <div className='col-lg-8 fv-row'>
                  <span className='fw-bold fs-6'>{userDetail?.role}</span>
                </div>
              </div>

              <div className='row mb-7'>
                <label className='col-lg-4 fw-bold text-muted'>Mobile No</label>

                <div className='col-lg-8'>
                  <a href='#' className='fw-bold fs-6 text-dark text-hover-primary'>
                    {userDetail?.mobile_no}
                  </a>
                </div>
              </div>

              <div className='row mb-7'>
                <label className='col-lg-4 fw-bold text-muted'>
                  Wallet
                  <i
                    className='fas fa-exclamation-circle ms-1 fs-7'
                    data-bs-toggle='tooltip'
                    title='Country of origination'
                  ></i>
                </label>

                <div className='col-lg-8'>
                  <span className='fw-bolder fs-6 text-dark'>
                    {userDetail?.wallet !== undefined && userDetail?.wallet > 0
                      ? `${userDetail?.wallet}`
                      : '0$ Balance'}
                  </span>
                </div>
              </div>

              <div className='row mb-7'>
                <label className='col-lg-4 fw-bold text-muted'>Account Status</label>

                <div className='col-lg-8'>
                  <span className='fw-bolder fs-6 text-dark'>{userDetail?.account_status}</span>
                </div>
              </div>

              <div className='row mb-7'>
                <label className='col-lg-4 fw-bold text-muted'>Aadhar Number</label>

                <div className='col-lg-8'>
                  <span className='fw-bolder fs-6 text-dark'>{userDetail?.aadhar_number}</span>
                </div>
              </div>

              <div className='row mb-7'>
                <label className='col-lg-4 fw-bold text-muted'>Aadhar Number Verified</label>

                <div className='col-lg-8'>
                  <span className='fw-bolder fs-6 text-dark'>
                    {userDetail ? userDetail?.aadhar_number_verified?.toString() : ''}
                  </span>
                </div>
              </div>

              {/* <div className='notice d-flex bg-light-warning rounded border-warning border border-dashed p-6'>
            <KTIcon iconName='information-5' className='fs-2tx text-warning me-4' />
            <div className='d-flex flex-stack flex-grow-1'>
              <div className='fw-bold'>
                <h4 className='text-gray-800 fw-bolder'>We need your attention!</h4>
                <div className='fs-6 text-gray-600'>
                  Your payment was declined. To start using tools, please
                  <Link className='fw-bolder' to='/crafted/account/settings'>
                    {' '}
                    Add Payment Method
                  </Link>
                  .
                </div>
              </div>
            </div>
          </div> */}
            </div>
          </>
        ) : (
          <>
            {Loading && (
              <span className='indicator-progress'>
                Please wait...
                <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
              </span>
            )}
          </>
        )}
      </div>

      {/* <div className='row gy-10 gx-xl-10'>
        <div className='col-xl-6'>
          <ChartsWidget1 className='card-xxl-stretch mb-5 mb-xl-10' />
        </div>

        <div className='col-xl-6'>
          <TablesWidget1 className='card-xxl-stretch mb-5 mb-xl-10' />
        </div>
      </div>

      <div className='row gy-10 gx-xl-10'>
        <div className='col-xl-6'>
          <ListsWidget5 className='card-xxl-stretch mb-5 mb-xl-10' />
        </div>

        <div className='col-xl-6'>
          <TablesWidget5 className='card-xxl-stretch mb-5 mb-xl-10' />
        </div>
      </div> */}
    </>
  )
}
