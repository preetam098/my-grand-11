import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import AllModerator from './components/AllModerator'

const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'Moderator Management',
    path:'/moderator',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const ModeratorPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='allmoderator'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Moderator</PageTitle>
            <AllModerator />
          </>
        }
      />
      <Route index element={<Navigate to='allmoderator' />} />
    </Route>
  </Routes>
)

export default ModeratorPage
