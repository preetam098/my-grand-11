import React, {useState} from 'react'
import { CommonTable } from '../../../component/CommonTable'
import { SelectComponent } from '../../../component/SelectComponent'
import SimpleReactValidator from 'simple-react-validator'
const option = [
  {name: 'option 1', value: 1, label: 'Option 1'},
  {name: 'option 2', value: 2, label: 'Option 2'},
  {name: 'option 3', value: 3, label: 'Option 3'},
  {name: 'option 4', value: 4, label: 'Option 4'},
  {name: 'option 5', value: 5, label: 'Option 5'},
]


const ScoreWriter = () => {
  const [openForm, setOpenForm] = useState(false)
  const [formValues, setFormValues] = useState({})
  const [errorMessage, setErrorMessage] = useState({})
  const validator = new SimpleReactValidator({})
  const handleOpen = () => {
    setOpenForm(!openForm)
  }

  const handleReset = () => {
    setFormValues('')
    setErrorMessage('')
  }

  
  const handleChange = (e) => {
    setFormValues({...formValues, [e.target.name]: e.target.value})
    setErrorMessage({...errorMessage, [e.target.name]: ''})
  }

 
  const handleSelect = (e, name) => {
    console.log('e', e.value, name)
    setErrorMessage({...errorMessage, [name]: ''})
    setFormValues({...formValues, [name]: e.value})
  }


  const handleSubmit = () => {
    if (validator.allValid()) {
      console.log('chl gya')
      setFormValues('')
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }

  return (
    <>
    {openForm && (
      <section className='p-5 rounded bg-white mb-4 mb-xl-8'>
        <div className='border-1 border-gray-300 border-bottom py-2 '>
          <h4 className='mb-0'>Create New Operator</h4>
        </div>
        <div className='row  my-4'>
          <div className='col-md-4 '>
        
          <SelectComponent
            className='mb-0'
            placeholder='Select Match'
            options={option}
            value={formValues?.ForTeam}
            handleChange={(e) => handleSelect(e, 'match')}
          />
          <div> {validator.message('match', formValues?.match, 'required')}</div>{' '}
          <p className='text-danger '>{errorMessage?.match}</p>
          
          </div>
          <div className='col-md-4 '>
          <SelectComponent
            className='mb-0'
            placeholder='Select Operator'
            options={option}
            value={formValues?.ForTeam}
            handleChange={(e) => handleSelect(e, 'operator')}
          />
          <div> {validator.message('operator', formValues?.operator, 'required')}</div>{' '}
          <p className='text-danger '>{errorMessage?.operator}</p>
          
          </div>
          <div className='col-md-4 '>
          <SelectComponent
            className='mb-0'
            placeholder='Assign Score Role'
            options={option}
            value={formValues?.ForTeam}
            handleChange={(e) => handleSelect(e, 'assign_role')}
          />
          <div> {validator.message('assign_role', formValues?.assign_role, 'required')}</div>{' '}
          <p className='text-danger '>{errorMessage?.assign_role}</p>
          
          </div>
        <div className=' d-flex justify-content-center align-items-center '>
        <table className='text-center border border-black-400'>
          <th className='border p-4'>
            Operator
          </th>
          <th className='border border-black p-4'>
            Access For
          </th>
          <th className='p-4'>
            Action
          </th>
        </table>

        </div>
        </div>

        <div className='gap-3 card-header border-0 p-5 text-center '>
            <button className='btn btn-sm mx-2 my-2 btn-light-primary' onClick={handleSubmit}>Submit</button>
            <button onClick={handleReset} className='btn btn-sm btn-light-danger'>
              Reset
            </button>
          </div>
      </section>
    )}
    <CommonTable title="Score Writer" total={100}  handleOpen={handleOpen} className='mb-5 mb-xl-8' headData={['Match Details', 'Date & Time', 'Match Scheduled' , ' User Details' , 'Assigned']} /> 
</>
  )
}

export default ScoreWriter