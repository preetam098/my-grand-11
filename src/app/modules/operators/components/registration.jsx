import React, {useState} from 'react'
import {CommonTable} from '../../../component/CommonTable'
import {SelectComponent} from '../../../component/SelectComponent'
import SimpleReactValidator from 'simple-react-validator'

const option = [
  {name: 'option 1', value: 1, label: 'Option 1'},
  {name: 'option 2', value: 2, label: 'Option 2'},
  {name: 'option 3', value: 3, label: 'Option 3'},
  {name: 'option 4', value: 4, label: 'Option 4'},
  {name: 'option 5', value: 5, label: 'Option 5'},
]

const Registration = () => {
  const [openForm, setOpenForm] = useState(false)
  const [formValues, setFormValues] = useState({})
  const [errorMessage, setErrorMessage] = useState({})
  const validator = new SimpleReactValidator({})
  const handleOpen = () => {
    setOpenForm(!openForm)
  }

  const handleReset = () => {
    setFormValues('')
    setErrorMessage('')
  }

  const handleSelect = (e, name) => {
    console.log('e', e.value, name)
    setErrorMessage({...errorMessage, [name]: ''})
    setFormValues({...formValues, [name]: e.value})
  }

  const handleChange = (e) => {
    setFormValues({...formValues, [e.target.name]: e.target.value})
    setErrorMessage({...errorMessage, [e.target.name]: ''})
  }

  const handleSubmit = () => {
    if (validator.allValid()) {
      console.log('chl gya')
      setFormValues('')
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }

  return (
    <>
      {openForm && (
        <section className='p-5 rounded bg-white mb-4 mb-xl-8'>
          <div className='border-1  border-gray-300 border-bottom py-2 '>
            <h4 className='mb-0'>Create New Operator</h4>
          </div>
          <div className=' mt-2 py-1 row'>
            <div className='col-md-4 mb-2'>
              <input
                onChange={handleChange}
                type='text'
                name='operator'
                value={formValues?.operator}
                className='form-control'
                placeholder='Operator'
              />
              {validator.message('operator', formValues?.operator, 'required', {
                className: 'text-danger ',
              })}
              <div className='text-danger '>{errorMessage?.operator}</div>
            </div>

            <div className='col-md-4 mb-2'>
              <input
                onChange={handleChange}
                type='email'
                name='email'
                value={formValues?.email}
                className='form-control'
                placeholder='Email'
              />
              {validator.message('email', formValues?.email, 'required|string', {
                className: 'text-danger',
              })}
              <div className='text-danger '>{errorMessage?.email}</div>
            </div>
            <div className='col-md-4 mb-2'>
              <input
                onChange={handleChange}
                type='text'
                name='mobile'
                value={formValues?.mobile}
                className='form-control'
                placeholder='Mobile'
              />
              {validator.message('mobile', formValues?.mobile, 'required|integer|max:10|min:10', {
                className: 'text-danger ',
              })}
              <div className='text-danger '>{errorMessage?.mobile}</div>
            </div>
            <div className='col-md-4 mb-2'>
              <SelectComponent
                className='mb-0'
                placeholder='Select Role'
                options={option}
                value={formValues?.ForTeam}
                handleChange={(e) => handleSelect(e, 'role')}
              />
              <div> {validator.message('role', formValues?.role, 'required')}</div>{' '}
              <p className='text-danger '>{errorMessage?.role}</p>
            </div>
          </div>

          <div className='gap-3 card-header border-0 p-5 text-end'>
            <button className='btn btn-sm mx-2 btn-light-primary' onClick={handleSubmit}>
              Submit
            </button>
            <button onClick={handleReset} className='btn btn-sm btn-light-danger'>
              Reset
            </button>
          </div>
        </section>
      )}
      <CommonTable
        title='Registration'
        total={100}
        handleOpen={handleOpen}
        className='mb-5 mb-xl-8'
        headData={['Operator Name', 'Email', 'Mobile', 'Role', 'Registered Date']}
      />
    </>
  )
}

export default Registration
