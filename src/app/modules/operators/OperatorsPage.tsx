import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import Registration from './components/registration'
import ScoreWriter from '../operators/components/scoreWriter'


const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'operators',
    path: '/operators',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const OperatorsPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='registrations'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Registrations</PageTitle>
            <Registration />
          </>
        }
      />
      
      <Route
        path='score-writers'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Score writers</PageTitle>
            <ScoreWriter />
          </>
        }
      />
      <Route index element={<Navigate to='/operators/registrations' />} />
    </Route>
  </Routes>
)

export default OperatorsPage
