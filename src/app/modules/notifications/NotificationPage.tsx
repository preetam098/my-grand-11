
import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import AllUsers from './components/AllUsers'
import SingleUser from './components/SingleUser'
import Topic from './components/Topic'

const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'notifications',
    path: '/notifications',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const FundPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='all-users'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>All Users</PageTitle>
            <AllUsers />
          </>
        }
      />
      
      <Route
        path='single-user'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Single User</PageTitle>
            <SingleUser />
          </>
        }
      />
       <Route
        path='topic'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Topic</PageTitle>
            <Topic />
          </>
        }
      />
      <Route index element={<Navigate to='/notifications/all-users' />} />
    </Route>
  </Routes>
)

export default FundPage
