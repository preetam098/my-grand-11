import {useState} from 'react'
import {CommonTable} from '../../../component/CommonTable'

import SimpleReactValidator from 'simple-react-validator'
import {SelectComponent} from '../../../component/SelectComponent'

const option = [
  {name: 'option 1', value: 1, label: 'Option 1'},
  {name: 'option 2', value: 2, label: 'Option 2'},
  {name: 'option 3', value: 3, label: 'Option 3'},
  {name: 'option 4', value: 4, label: 'Option 4'},
  {name: 'option 5', value: 5, label: 'Option 5'},
]

const AllUsers = () => {
  const [openForm, setOpenForm] = useState(false)
  const [preview, setPreview] = useState('')
  const [formValues, setFormValues] = useState({})
  const [errorMessage, setErrorMessage] = useState({})
  const validator = new SimpleReactValidator({
    className: 'text-danger',
    validators: {
      fileSize: {
        message: 'The :attribute must be max 1MB.',
        rule: function (val, maxSize, validator) {
          return val && val.size <= 1048576
        },
      },
    },
  })

  const handleChange = (event) => {
    const {name, value, type} = event.target
    console.log('fdv', name, value, event.target)
    setErrorMessage({...errorMessage, [name]: ''})
    if (type === 'file') {
      setPreview(URL.createObjectURL(event.target.files[0]))
      setFormValues({...formValues, [name]: event.target.files[0]})
    } else {
      setFormValues({...formValues, [name]: value})
    }
  }

  const handleSelect = (e, name) => {
    console.log('e', e.value, name)
    setErrorMessage({...errorMessage, [name]: ''})
    setFormValues({...formValues, [name]: e.value})
  }

  const handleOpen = () => {
    setOpenForm(true)
  }

  const handleDiscard = () => {
    setFormValues('')
    setErrorMessage('')
    setOpenForm(false)
  }
  const handleSubmit = () => {
    if (validator.allValid()) {
      console.log('chl gya')
      setOpenForm(false)
      setFormValues('')
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }

  console.log('formValues', formValues)

  return (
    <>
      {openForm && (
        <section className='px-5 rounded bg-white mb-4 mb-xl-8'>
          <div className='border-1  border-gray-300 border-bottom py-5 '>
            <h4 className='mb-0'>Send New Notification</h4>
          </div>

          <div className='row my-2'>
            <div className='col-md-6'>
              <div >
                <label>Send To</label>
                <SelectComponent
                  placeholder='Android'
                  options={option}
                  value={formValues?.ForTeam}
                  handleChange={(e) => handleSelect(e, 'sendTO')}
                />
                <div> {validator.message('sendTO', formValues?.sendTO, 'required')}</div>{' '}
                <p className='text-danger '>{errorMessage?.sendTO}</p>
              </div>
            </div>
            <div className='col-md-6'>
              <div >
                <label>Send To</label>
                <SelectComponent
                  placeholder='Android'
                  options={option}
                  value={formValues?.ForTeam}
                  handleChange={(e) => handleSelect(e, 'users')}
                />
                <div> {validator.message('users', formValues?.users, 'required')}</div>{' '}
                <p className='text-danger '>{errorMessage?.users}</p>
              </div>
            </div>
            <div className=' col-md-6  '>
              <div >
              <label>Notification Title</label>
                <input
                  onChange={handleChange}
                  type='text'
                  name='notification'
                  value={formValues?.notification}
                  className='form-control'
                  placeholder='Notification'
                />
                {validator.message(
                  'notification',
                  formValues?.notification,
                  'required|notification',
                  {
                    className: 'text-danger ',
                  }
                )}
                <div className='text-danger '>{errorMessage?.notification}</div>
              </div>
            </div>
            <div className='col-md-6 '>
              <div>
              <label>Message</label>
                <textarea
                  name='message'
                  onChange={handleChange}
                  className='form-control'
                  value={formValues?.message}
                  rows={1}
                  placeholder='message'
                />
                {validator.message('message', formValues?.message, 'required')}
                <div className='text-danger '>{errorMessage?.message}</div>
              </div>
            </div>

            <div className='gap-3 card-header border-0 p-5 text-end'>
              <button className='btn btn-sm mx-2 btn-light-primary' onClick={handleSubmit}>
                Submit
              </button>
              <button onClick={handleDiscard} className='btn btn-sm btn-light-danger'>
                Discard
              </button>
            </div>
          </div>
        </section>
      )}

      <section>
        <CommonTable
          title='Series'
          total={100}
          handleOpen={handleOpen}
          className='mb-5 mb-xl-8'
          headData={['Team Name', 'Abbravation', ' Players']}
        />
      </section>
    </>
  )
}

export default AllUsers
