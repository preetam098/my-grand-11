import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import SetTeams from './components/setTeams'
import Score from './components/score'


const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'current',
    path: '/current',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const CurrentPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='set-teams'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Set Teams</PageTitle>
            <SetTeams />
          </>
        }
      />
      
      <Route
        path='score-entry'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Score</PageTitle>
            <Score />
          </>
        }
      />
      <Route index element={<Navigate to='/current/set-teams' />} />
    </Route>
  </Routes>
)

export default CurrentPage
