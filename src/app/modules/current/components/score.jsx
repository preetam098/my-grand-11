import React, {useState} from 'react'
import {CommonTable} from '../../../component/CommonTable'
import {SelectComponent} from '../../../component/SelectComponent'
import SimpleReactValidator from 'simple-react-validator'

const option = [
  {name: 'option 1', value: 1, label: 'Option 1'},
  {name: 'option 2', value: 2, label: 'Option 2'},
  {name: 'option 3', value: 3, label: 'Option 3'},
  {name: 'option 4', value: 4, label: 'Option 4'},
  {name: 'option 5', value: 5, label: 'Option 5'},
]

const Score = () => {
  const [formValues, setFormValues] = useState({})
  const [errorMessage, setErrorMessage] = useState({})
  const validator = new SimpleReactValidator({})

  const handleChange = (e) => {
    setFormValues({...formValues, [e.target.name]: e.target.value})
    setErrorMessage({...errorMessage, [e.target.name]: ''})
  }

  console.log('form', formValues)

  const handleReset = () => {
    setFormValues('')
    setErrorMessage('')
  }

  const handleSelect = (e, name) => {
    console.log('e', e.value, name)
    setErrorMessage({...errorMessage, [name]: ''})
    setFormValues({...formValues, [name]: e.value})
  }

  const handleSubmit = () => {
    if (validator.allValid()) {
      console.log('chl gya')
      setFormValues('')
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }

  return (
    <section className=' p-5 rounded bg-white mb-4 mb-xl-8'>
      <div className='border-1  border-gray-300 border-bottom py-2'>
        <h4 className='mb-0'>Score Entry</h4>
      </div>
      <div className='py-1 row mt-2'>
        <div className='col-md-4'>
          <SelectComponent
            className='mb-0'
            placeholder='Select Match'
            options={option}
            value={formValues?.ForTeam}
            handleChange={(e) => handleSelect(e, 'Match')}
          />
          <div> {validator.message('Match', formValues?.Match, 'required')}</div>{' '}
          <p className='text-danger '>{errorMessage?.Match}</p>
        </div>

        <div className='col-md-4'>
          <SelectComponent
            className='mb-0'
            placeholder='Select Player'
            options={option}
            value={formValues?.ForTeam}
            handleChange={(e) => handleSelect(e, 'Player')}
          />
          <div> {validator.message('Player', formValues?.Player, 'required')}</div>{' '}
          <p className='text-danger '>{errorMessage?.Player}</p>
        </div>

        <div className='col-md-4'>
          <h6 className='my-2'>Current Points {' :'}</h6>
        </div>
      </div>

      <div>
        <input type='number' className='form-control' disabled placeholder='Batting' />
      </div>

      <div className='my-2 row'>
        <div className='col-md-4'>
          <label>Ball Faced</label>
          <input
            onChange={handleChange}
            type='text'
            name='ballFaced'
            value={formValues?.ballFaced}
            className='form-control'
            placeholder='Ball Faced'
          />
          {validator.message('ballFaced', formValues?.ballFaced, 'required|integer', {
            className: 'text-danger ',
          })}
          <div className='text-danger '>{errorMessage?.ballFaced}</div>
        </div>

        <div className='col-md-4'>
          <label>Total Runs</label>
          <input
            onChange={handleChange}
            type='text'
            name='totalRuns'
            value={formValues?.totalRuns}
            className='form-control'
            placeholder='Total Runs'
          />
          {validator.message('totalRuns', formValues?.totalRuns, 'required|integer', {
            className: 'text-danger ',
          })}
          <div className='text-danger '>{errorMessage?.totalRuns}</div>
        </div>

        <div className='col-md-4'>
          <div>
            <label>Boundries</label>
            <input
              onChange={handleChange}
              type='text'
              name='Boundries'
              value={formValues?.Boundries}
              className='form-control'
              placeholder='Boundries'
            />
            {validator.message('Boundries', formValues?.Boundries, 'required|integer', {
              className: 'text-danger ',
            })}
            <div className='text-danger '>{errorMessage?.Boundries}</div>
          </div>
        </div>
      </div>
      <div className='my-2 row'>
        <div className='col-md-6'>
          <div>
            <label>Sixes</label>
            <input
              onChange={handleChange}
              type='text'
              name='sixes'
              value={formValues?.sixes}
              className='form-control'
              placeholder='sixes'
            />
            {validator.message('sixes', formValues?.sixes, 'required|integer', {
              className: 'text-danger ',
            })}
            <div className='text-danger '>{errorMessage?.sixes}</div>
          </div>
        </div>
        <div className='col-md-6'>
          <label>Out Of Zero</label>
          <SelectComponent
            className=''
            placeholder='Out of Zero'
            options={option}
            value={formValues?.ForTeam}
            handleChange={(e) => handleSelect(e, 'Player')}
          />
          <div> {validator.message('Player', formValues?.Player, 'required')}</div>{' '}
          <p className='text-danger '>{errorMessage?.Player}</p>
        </div>
      </div>

      <div>
        <input type='number' className='form-control' disabled placeholder='Bowling' />
      </div>

      <div className='my-2 row'>
        <div className='col-md-4'>
          <label>Overs</label>
          <input
            onChange={handleChange}
            type='text'
            name='overs'
            value={formValues?.overs}
            className='form-control'
            placeholder='Overs'
          />
          {validator.message('overs', formValues?.overs, 'required|string', {
            className: 'text-danger ',
          })}
          <div className='text-danger '>{errorMessage?.overs}</div>
        </div>

        <div className='col-md-4'>
          <div>
            <label>Dot balls </label>
            <input
              onChange={handleChange}
              type='text'
              name='dotBalls'
              value={formValues?.dotBalls}
              className='form-control'
              placeholder='Dot balls '
            />
            {validator.message('dotBalls', formValues?.dotBalls, 'required|integer', {
              className: 'text-danger ',
            })}
            <div className='text-danger '>{errorMessage?.dotBalls}</div>
          </div>
        </div>
        <div className='col-md-4'>
          <label>Runs Given</label>
          <input
            onChange={handleChange}
            type='text'
            name='runsGiven'
            value={formValues?.runsGiven}
            className='form-control'
            placeholder='Runs '
          />
          {validator.message('runsGiven', formValues?.runsGiven, 'required|integer', {
            className: 'text-danger ',
          })}
          <div className='text-danger '>{errorMessage?.runsGiven}</div>
        </div>
      </div>
      <div className='my-2 row'>
        <div className='col-md-6'>
          <label>Maiden over</label>
          <input
            onChange={handleChange}
            type='number'
            name='maiden_over'
            value={formValues?.maiden_over}
            className='form-control'
            placeholder='Maiden Over'
          />
          {validator.message('maiden_over', formValues?.maiden_over, 'required|integer', {
            className: 'text-danger ',
          })}
          <div className='text-danger '>{errorMessage?.maiden_over}</div>
        </div>
        <div className='col-md-6'>
          <label>Total Wickets</label>
          <SelectComponent
            className='mb-0'
            placeholder='Total Wickets'
            options={option}
            value={formValues?.ForTeam}
            handleChange={(e) => handleSelect(e, 'total_wickets')}
          />
          <div> {validator.message('total_wickets', formValues?.total_wickets, 'required')}</div>{' '}
          <p className='text-danger '>{errorMessage?.total_wickets}</p>
        </div>
      </div>

      <div>
        <input type='number' className='form-control' disabled placeholder='Fielding/Keeping' />
      </div>

      <div className='my-2 row '>
        <div className='col-md-4'>
          <div className=' p-2 mb-2 d-flex align-items-center rounded bg-gray-300 '>
            <input
              className='rounded mx-2'
              id='isCaught'
              name='isCaught'
              htmlFor='isCaught'
              type='checkbox'
            />
            <label id='isCaught' className='fs-4 font-weight-bold'>
              Is Caught & Bowled?
            </label>
          </div>
        </div>
        <div className='col-md-4'>
          <div className='p-2 mb-2  d-flex align-items-center rounded bg-gray-300 '>
            <input
              className='rounded mx-2'
              id='isTaken'
              name='isTaken'
              htmlFor='isTaken'
              type='checkbox'
            />
            <label id='isTaken' className='fs-4 font-weight-bold'>
              Is Caught & Taken?
            </label>
          </div>
        </div>
        <div className='col-md-4'>
          <div className='p-2 mb-2  d-flex align-items-center rounded bg-gray-300 '>
            <input
              className='rounded mx-2'
              id='isRunOut'
              name='isRunOut'
              htmlFor='isRunOut'
              type='checkbox'
            />
            <label id='isRunOut' className='fs-4 font-weight-bold'>
              Is Runout Thrower?
            </label>
          </div>
        </div>

        <div className='col-md-4'>

        <div className=' p-2 mb-2  d-flex align-items-center rounded bg-gray-300 '>
          <input
            className='rounded mx-2'
            id='isCatcher'
            name='isCatcher'
            htmlFor='isCatcher'
            type='checkbox'
          />
          <label id='isCatcher' className='fs-4 font-weight-bold'>
            Is RunOut Catcher?
          </label>
        </div>
        </div>
        <div className='col-md-4'>
        <div className=' p-2 mb-2 d-flex align-items-center rounded bg-gray-300 '>
          <input
            className='rounded mx-2'
            id='stump_runout'
            name='stump_runout'
            htmlFor='stump_runout'
            type='checkbox'
          />
          <label id='stump_runout' className='fs-4 font-weight-bold'>
            Direct Stumping or Runout?
          </label>
        </div>

        </div>

        
      </div>
      <div className='my-2 row gap-2'>
       
      
      </div>

      <div className='gap-3 card-header border-0 px-5 py-4 text-end'>
        <button className='btn btn-sm mx-2 btn-light-primary' onClick={handleSubmit}>
          Submit
        </button>
        <button onClick={handleReset} className='btn btn-sm btn-light-danger'>
          Reset
        </button>
      </div>
    </section>
  )
}

export default Score
