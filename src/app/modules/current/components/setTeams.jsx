import React, {useState} from 'react'
import {CommonTable} from '../../../component/CommonTable'
import {SelectComponent} from '../../../component/SelectComponent'
import SimpleReactValidator from 'simple-react-validator'

const option = [
  {name: 'option 1', value: 1, label: 'Option 1'},
  {name: 'option 2', value: 2, label: 'Option 2'},
  {name: 'option 3', value: 3, label: 'Option 3'},
  {name: 'option 4', value: 4, label: 'Option 4'},
  {name: 'option 5', value: 5, label: 'Option 5'},
]

const SetTeams = () => {
  const [formValues, setFormValues] = useState({})
  const [errorMessage, setErrorMessage] = useState({})
  const validator = new SimpleReactValidator({})
  console.log('fomr', formValues)

  const handleDiscard = () => {
    setFormValues('')
    setErrorMessage('')
  }

  const handleSelect = (e, name) => {
    console.log('e', e.value, name)
    setErrorMessage({...errorMessage, [name]: ''})
    setFormValues({...formValues, [name]: e.value})
  }
  const handleSubmit = () => {
    if (validator.allValid()) {
      console.log('chl gya')
      setFormValues('')
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }

  return (
    <section className=' rounded bg-white mb-4 mb-xl-8'>
      <div className='border-1  border-gray-300 border-bottom p-5 '>
        <h4 className='mb-0'>Set Teams</h4>
      </div>
      <div className='px-5 py-1 row my-4'>
        <div className='col-md-6'>
          <SelectComponent
            className='mb-0'
            placeholder='Select Match'
            options={option}
            value={formValues?.ForTeam}
            handleChange={(e) => handleSelect(e, 'match')}
          />
          <div> {validator.message('match', formValues?.match, 'required')}</div>{' '}
          <p className='text-danger '>{errorMessage?.match}</p>
        </div>
        <div className='row'>
          <div className='col-md-6'>
            <h4>Team One {':'}</h4>
          </div>
          <div className='col-md-6'>
            <h4>Team Two {':'}</h4>
          </div>
        </div>
      </div>

      <div className='gap-3 card-header border-0 px-5 py-4 text-end'>
        <button className='btn btn-sm mx-2 btn-light-primary' onClick={handleSubmit}>
          Submit
        </button>
        <button onClick={handleDiscard} className='btn btn-sm btn-light-danger'>
          Discard
        </button>
      </div>
    </section>
  )
}

export default SetTeams
