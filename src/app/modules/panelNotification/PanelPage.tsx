import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import Panel from './components/Panel'

const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'Panel',
    path:'/panel-notification',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const PanelPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='panel-notification'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Panel Notification</PageTitle>
            <Panel />
          </>
        }
      />
      <Route index element={<Navigate to='panel-notification' />} />
    </Route>
  </Routes>
)

export default PanelPage
