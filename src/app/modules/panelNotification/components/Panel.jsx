import {useState} from 'react'
import {CommonTable} from '../../../component/CommonTable'

import SimpleReactValidator from 'simple-react-validator'
import { SelectComponent } from '../../../component/SelectComponent'


const option = [
  {name: 'option 1', value: 1, label: 'Option 1'},
  {name: 'option 2', value: 2, label: 'Option 2'},
  {name: 'option 3', value: 3, label: 'Option 3'},
  {name: 'option 4', value: 4, label: 'Option 4'},
  {name: 'option 5', value: 5, label: 'Option 5'},
]


const Panel = () => {
  const [openForm, setOpenForm] = useState(false)
  const [preview, setPreview] = useState("")
  const [formValues, setFormValues] = useState({})
  const [errorMessage, setErrorMessage] = useState({})
  const validator = new SimpleReactValidator({
    className: 'text-danger',
    validators: {
      fileSize: {
        message: 'The :attribute must be max 1MB.',
        rule: function (val, maxSize, validator) {
          return val && val.size <= 1048576
        },
      },
    },
  })

  const handleChange = (event) => {
    const {name, value, type} = event.target
    console.log("fdv" , name , value , event.target)
    setErrorMessage({...errorMessage, [name]: ''})
    if (type === 'file') {
      setPreview(URL.createObjectURL(event.target.files[0]))
      setFormValues({...formValues, [name]: event.target.files[0]})
    } else {
      setFormValues({...formValues, [name]: value})
    }
  }

  const handleOpen = () => {
    setOpenForm(true)
  }

  const handleDiscard=()=>{
    setFormValues("")
    setErrorMessage("")
    setOpenForm(false)
  }
  const handleSubmit = () => {
    if (validator.allValid()) {
      console.log('chl gya')
    setOpenForm(false)
    setFormValues("")
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }

  
  const handleSelect = (e, name) => {
    console.log('e', e.value, name)
    setErrorMessage({...errorMessage, [name]: ''})
    setFormValues({...formValues, [name]: e.value})
  }

  console.log('formValues', formValues)

  return (
    <>
      {openForm && (
        <section className='px-5 rounded bg-white mb-4 mb-xl-8'>
          <div className='border-1  border-gray-300 border-bottom py-5 '>
            <h4 className='mb-0'>Home Screen Notification</h4>
          </div>
          <div className=' mt-5 py-1 row'>
            <div className='col-md-6'>
            <div>
                <label>Sport Type </label>
                <SelectComponent
                  className='mb-0'
                  placeholder='option'
                  options={option}
                  value={formValues?.sportype}
                  handleChange={(e) => handleSelect(e, 'sportype')}
                  
                />
                <div> {validator.message('sportype', formValues?.sportype, 'required')}</div>{' '}
                <p className='text-danger '>{errorMessage?.sportype}</p>
              </div>

            </div>
            <div className='col-md-6'>
            <label>Title</label>

              <input
                onChange={handleChange}
                type='text'
                name='title'
                value={formValues?.title}
                className='form-control'
                placeholder='title'
              />
              {validator.message('title', formValues?.title, 'required|string', {
                className: 'text-danger ',
              })}
              <div className='text-danger '>{errorMessage?.title}</div>
            </div>
            <div className='col-md-6'>
            <label>Code </label>

              <input
                onChange={handleChange}
                type='text'
                name='code'
                value={formValues?.code}
                className='form-control'
                placeholder='Code'
              />
              {validator.message('code', formValues?.code, 'required|code')}
              <div className='text-danger '>{errorMessage?.code}</div>
            </div>
            <div className='col-md-6'>
            <label>Sub Code </label>

              <input
                onChange={handleChange}
                type='text'
                name='subCode'
                value={formValues?.subCode}
                className='form-control'
                placeholder='sub code'
              />
              {validator.message('subCode', formValues?.subCode, 'required|subCode')}
              <div className='text-danger '>{errorMessage?.subCode}</div>
            </div>
          </div>
          <div className=' py-1 row '>
            <div className='col-md-6 mb-2'>
            <label>Message </label>
              <textarea
                name='message'
                onChange={handleChange}
                className='form-control'
                value={formValues?.message}
                rows={3}
                placeholder='message'
              />
              {validator.message('message', formValues?.message, 'required')}
              <div className='text-danger '>{errorMessage?.message}</div>
            </div>
            <div className='col-md-6 '>
            <label>Image </label>

              <div className='gap-1 border rounded border-secondary text-center border-blue '>
                <div>
                  <label
                    htmlFor='images'
                    className='text-xs p-6 d-flex flex-column gap-1 justify-content-center rounded  border-[1.5px] p-1 align-items-center'
                  >
                    {formValues?.image && (
                      <img
                        src={formValues && !preview ? `${formValues?.image}` : preview}
                        alt='preview'
                        className='h-45px  '
                      />
                    )}

                    {formValues?.image ? (
                      formValues?.image?.name
                    ) : (
                      <>
                        <i className='bi bi-cloud-arrow-up fs-1'></i>
                        <span>Upload Image</span>
                      </>
                    )}
                  </label>
                  <input
                    autoComplete='off'
                    id='images'
                    type='file'
                    name='image'
                    // accept={acceptImageFileType}
                    onChange={handleChange}
                    className='rounded py-1.5 px-2 d-none outline-none border'
                  />
                </div>
                <div> {validator.message('image', formValues?.image, 'required|fileSize')}</div>{' '}
              </div>
              <div className='text-danger '>{errorMessage?.image}</div>

            </div>

            <div className='gap-3 card-header border-0 p-5 text-end'>
              <button className='btn btn-sm mx-2 btn-light-primary' onClick={handleSubmit}>
                Submit
              </button>
              <button onClick={handleDiscard} className='btn btn-sm btn-light-danger'>
                Discard
              </button>
            </div>
          </div>
        </section>
      )}

      <section>
        <CommonTable
          title='Series'
          total={100}
          handleOpen={handleOpen}
          className='mb-5 mb-xl-8'
          headData={['Team Name', 'Abbravation', ' Players']}
        />
      </section>
    </>
  )
}

export default Panel
