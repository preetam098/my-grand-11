import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import About from './components/About'
import Contact from './components/Contact'
import Friends from './components/Friends'
import Help from './components/Help'
import Learn from './components/Learn'
import Legal from './components/Legal'




const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'Manage Content',
    path: '/manage-content',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const ManagePage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='about-us'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>About</PageTitle>
            <About />
          </>
        }
      />
      
      <Route
        path='contact-us'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Contact us</PageTitle>
            <Contact />
          </>
        }
      />


<Route
        path='help'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Help</PageTitle>
            <Help />
          </>
        }
      />
      <Route
        path='invitation'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Friends Invitation</PageTitle>
            <Friends />
          </>
        }
      />
      <Route
        path='learn-play'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Learn</PageTitle>
            <Learn />
          </>
        }
      />
      <Route
        path='legal'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Legal</PageTitle>
            <Legal />
          </>
        }
      />


      <Route index element={<Navigate to='/manage-content/about-us' />} />
    </Route>
  </Routes>
)

export default ManagePage
