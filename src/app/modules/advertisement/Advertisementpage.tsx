import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import Index from './components/index'

const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'advertisement',
    path:'/advertisement',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const AdvertisementPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='advertisement'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>advertisement</PageTitle>
            <Index />
          </>
        }
      />
      <Route index element={<Navigate to='advertisement' />} />
    </Route>
  </Routes>
)

export default AdvertisementPage
