/* eslint-disable jsx-a11y/anchor-is-valid */
import {useEffect} from 'react'
import {Outlet, Route, Routes} from 'react-router-dom'
import {Registration} from './components/Registration'
import {ForgotPassword} from './components/ForgotPassword'
import {Login} from './components/Login'
import {toAbsoluteUrl} from '../../../_metronic/helpers'
import Logo1 from '../../../_metronic/assets/images/logo/logo.png'
import Logo2 from '../../../_metronic/assets/images/logo/logo2.png'
import './components/auth.css'

const AuthLayout = () => {
  useEffect(() => {
    document.body.classList.add('bg-body')
    return () => {
      document.body.classList.remove('bg-body')
    }
  }, [])

  return (
    <div
      className='container-xxl '
      style={{
        height: '100vh',
        backgroundImage: `url(${toAbsoluteUrl('/media/auth/Bg.png')})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
      }}
    >
      <div className='section'>
        <div className=' d-flex flex-column flex-md-row justify-content-center align-items-center'>
          <div className='d-flex flex-column justify-content-center align-items-center px-4 py-12 py-md-0 mx-auto'>
            <img className='img-fluid mb-4' src={Logo1} alt='Logo1' />
            <img className='img-fluid mb-4' src={Logo2} alt='Logo2' />
            <div className='text-center'>
              <h1 className='text-gray'>Fast, Efficient and Productive</h1>
              <p className='fs-6' style={{color: '#5E6278'}}>
                In this kind of post, <span className='text-primary'>the blogger</span> introduces a
                person they’ve interviewed and <br /> provides some background information about{' '}
                <span className='text-primary'>the interviewee</span> and their
                <br />
                work following this is a transcript of the interview.
              </p>
            </div>
          </div>

          <div className='col-md-5 col-12 d-flex flex-column justify-content-center align-items-center bg-body p-4 my-4 rounded shadow-sm'>
            <Outlet />
          </div>
        </div>
      </div>
    </div>
  )
}

const AuthPage = () => (
  <Routes>
    <Route element={<AuthLayout />}>
      <Route path='login' element={<Login />} />
      {/* <Route path='registration' element={<Registration />} />
      <Route path='forgot-password' element={<ForgotPassword />} /> */}
      <Route index element={<Login />} />
    </Route>
  </Routes>
)

export {AuthPage}
