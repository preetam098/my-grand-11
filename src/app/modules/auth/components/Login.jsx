import {useState} from 'react'
import * as Yup from 'yup'
import clsx from 'clsx'
import {Link, Navigate, useNavigate} from 'react-router-dom'
import {useFormik} from 'formik'
import {login} from '../core/_requests'
import {useAuth} from '../core/Auth'
import {useDispatch, useSelector} from 'react-redux'

const loginSchema = Yup.object().shape({
  mobile_no: Yup.string().min(10, 'Minimum 10 digit').required('mobile_no is required'),
  password: Yup.string()
    .min(3, 'Minimum 3 symbols')
    .max(50, 'Maximum 50 symbols')
    .required('Password is required'),
})

const initialValues = {
  mobile_no: '919933944916',
  password: 'monty123*',
}

export function Login() {
  const [loading, setLoading] = useState(false)
  const {saveAuth, setCurrentUser} = useAuth('')
  const [showPassword, setShowPassword] = useState(false)
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const res = useSelector((state) => state.authReducer)

  const formik = useFormik({
    initialValues,
    validationSchema: loginSchema,
    onSubmit: async (values, {setStatus, setSubmitting}) => {
      setLoading(true)
      try {
        const {data: auth} = await login(values.mobile_no, values.password)
        saveAuth(auth)

        // const {data: user} = await getUserByToken(auth.api_token)
        setCurrentUser(auth)
      } catch (error) {
        console.error(error)
        saveAuth(undefined)
        setStatus('The login details are incorrect')
        setSubmitting(false)
        setLoading(false)
      }
    },
  })

  return (
    <form
      className='form py-10 w-100'
      onSubmit={formik.handleSubmit}
      noValidate
      id='kt_login_signin_form'
    >
      {/* begin::Heading */}
      <div className='text-center mb-11'>
        <h1 className='text-dark fw-bolder'>Sign In</h1>
        <p style={{color: '#A1A5B7', fontSize: '15px'}}>Neque porro quisquam est</p>
      </div>

      {/* begin::Heading */}

      {/* begin::Form group */}
      <div className='fv-row'>
        <input
          placeholder='Mobile No'
          {...formik.getFieldProps('mobile_no')}
          className={clsx(
            'form-control bg-transparent my-5',
            {'is-invalid': formik.touched.mobile_no && formik.errors.mobile_no},
            {
              'is-valid': formik.touched.mobile_no && !formik.errors.mobile_no,
            }
          )}
          type='mobile_no'
          name='mobile_no'
          autoComplete='off'
        />
        {formik.touched.mobile_no && formik.errors.mobile_no && (
          <div className='fv-plugins-message-container'>
            <span role='alert'>{formik.errors.mobile_no}</span>
          </div>
        )}
      </div>
      {/* end::Form group */}

      {/* begin::Form group */}
      <div className='fv-row '>
        <p
          className='mx-4 mb-1'
          onClick={() => setShowPassword(!showPassword)}
          style={{textAlign: 'right'}}
        >
          {showPassword ? (
            <>
              <i class='bi bi-eye-fill'></i>
            </>
          ) : (
            <>
              <i class='bi bi-eye-slash-fill'></i>
            </>
          )}
        </p>
        <input
          type={showPassword ? 'text' : 'password'}
          autoComplete='off'
          placeholder='password'
          {...formik.getFieldProps('password')}
          className={clsx(
            'form-control bg-transparent',
            {
              'is-invalid': formik.touched.password && formik.errors.password,
            },
            {
              'is-valid': formik.touched.password && !formik.errors.password,
            }
          )}
        />
        {formik.touched.password && formik.errors.password && (
          <div className='fv-plugins-message-container'>
            <div className='fv-help-block'>
              <span role='alert'>{formik.errors.password}</span>
            </div>
          </div>
        )}
      </div>

      <div className=' d-flex my-5 align-items-center gap-3 fs-base fw-semibold'>
        <input type='checkbox' id='rememberPassword' />
        <label htmlFor='rememberPassword'>
          Remember Password
        </label>
      </div>

      {/* begin::Action */}
      <div className='d-grid mb-10'>
        <button
          type='submit'
          id='kt_sign_in_submit'
          className='btn btn-primary'
          disabled={formik.isSubmitting || !formik.isValid}
        >
          {!loading && <span className='indicator-label'>Login</span>}
          {loading && (
            <span className='indicator-progress' style={{display: 'block'}}>
              Please wait...
              <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
            </span>
          )}
        </button>
      </div>
      {/* end::Action */}
    </form>
  )
}
