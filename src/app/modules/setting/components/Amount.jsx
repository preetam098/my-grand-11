import React, {useState} from 'react'
import {CommonTable} from '../../../component/CommonTable'
import SimpleReactValidator from 'simple-react-validator'
import {SelectComponent} from '../../../component/SelectComponent'

const Amount = () => {
  const [formValues, setFormValues] = useState({})
  const [errorMessage, setErrorMessage] = useState({})
  const validator = new SimpleReactValidator({})
  const [openForm, setOpenForm] = useState(false)

  const handleSubmit = () => {
    if (validator.allValid()) {
      console.log('chl gya')
      setFormValues('')
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }
  const handleOpen = () => {
    setOpenForm(!openForm)
  }
  const handleChange = (e) => {
    setFormValues({...formValues, [e.target.name]: e.target.value})
    setErrorMessage({...errorMessage, [e.target.name]: ''})
  }
  return (
    <>
      {openForm && (
        <section>
          <div className='d-flex justify-content-center align-items-center '>
            <div className=' rounded bg-body  p-5 w-100 mb-5'>
              <div className='border-1  border-gray-300 border-bottom py-2'>
                <h4 className='mb-0'>Update General Setting</h4>
              </div>
              <div className='mx-1 row py-2 my-2  '>
                <div className='col-md-6 mb-2  '>
                  <label>Welcome Registration Bonus:</label>
                  <input
                    onChange={handleChange}
                    type='text'
                    name='registrationBonus'
                    value={formValues?.registrationBonus}
                    className='form-control'
                    placeholder=''
                  />
                  {validator.message(
                    'registrationBonus',
                    formValues?.registrationBonus,
                    'required|string',
                    {
                      className: 'text-danger ',
                    }
                  )}
                  <div className='text-danger '>{errorMessage?.registrationBonus}</div>
                </div>

                <div className='col-md-6 mb-2'>
                  <label>Bonus Percentage use while contest joining:</label>
                  <input
                    onChange={handleChange}
                    type='text'
                    name='bonusPercentage'
                    value={formValues?.bonusPercentage}
                    className='form-control'
                    placeholder='Bonus Percentage'
                  />
                  {validator.message('bonusPercentage', formValues?.bonusPercentage, 'required', {
                    className: 'text-danger ',
                  })}
                  <div className='text-danger '>{errorMessage?.bonusPercentage}</div>
                </div>

                <div className='col-md-6 mb-2'>
                  <label>Withdraw Min & Max:</label>
                  <input
                    onChange={handleChange}
                    type='text'
                    name='withdrawMin'
                    value={formValues?.withdrawMin}
                    className='form-control'
                    placeholder='0'
                  />
                  {validator.message('withdrawMin', formValues?.withdrawMin, 'required', {
                    className: 'text-danger ',
                  })}
                  <div className='text-danger '>{errorMessage?.withdrawMin}</div>

                  <input
                    onChange={handleChange}
                    type='text'
                    name='withdrawMax'
                    value={formValues?.withdrawMax}
                    className='form-control  my-2'
                    placeholder='0'
                  />
                  {validator.message('withdrawMax', formValues?.withdrawMax, 'required', {
                    className: 'text-danger ',
                  })}
                  <div className='text-danger '>{errorMessage?.withdrawMax}</div>
                </div>

                <div className='col-md-6 mb-2'>
                  <label>Referral Bonus to Both Users:</label>
                  <input
                    onChange={handleChange}
                    type='text'
                    name='referral'
                    value={formValues?.referral}
                    className='form-control'
                    placeholder='Referral'
                  />
                  {validator.message('referral', formValues?.referral, 'required', {
                    className: 'text-danger ',
                  })}
                  <div className='text-danger '>{errorMessage?.referral}</div>
                </div>
              </div>

              <div className='col-md-6 px-5 mb-4 flex-wrap gap-2 d-flex'>
                <div className='col-md-3'>
                  <button className='btn btn-light-primary' onClick={handleSubmit}>
                    Update
                  </button>
                </div>
              </div>
            </div>
          </div>
        </section>
      )}
      <div>
        <CommonTable
          title='Amount'
          total={100}
          handleOpen={handleOpen}
          className='mb-5 mb-xl-8'
          headData={['Full Name', 'Email', 'Mobile', 'Amount', 'Date', 'Against']}
        />
      </div>
    </>
  )
}

export default Amount
