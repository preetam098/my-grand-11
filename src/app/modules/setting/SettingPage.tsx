import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import Amount from './components/Amount'
import Current from './components/Current'

const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'setting',
    path:'/settings',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const SettingPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='amount'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Amount</PageTitle>
            <Amount />
          </>
        }
      />
      <Route
        path='apk'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Current</PageTitle>
            <Current />
          </>
        }
      />
      <Route index element={<Navigate to='settings/amount' />} />
    </Route>
  </Routes>
)

export default SettingPage
