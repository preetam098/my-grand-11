import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import Initiated from './components/initiated'
import Inprocess from './components/Inprocess'
import Declined from './components/Declined'
import Approved from './components/Approved'



const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'Funds',
    path: '/funds',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const FundPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='initiated'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Initiated</PageTitle>
            <Initiated />
          </>
        }
      />
      
      <Route
        path='inprocess'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Inprocess</PageTitle>
            <Inprocess />
          </>
        }
      />
       <Route
        path='approved'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Approved</PageTitle>
            <Approved />
          </>
        }
      />
       <Route
        path='declined'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Declined</PageTitle>
            <Declined />
          </>
        }
      />
      <Route index element={<Navigate to='/funds/initiated' />} />
    </Route>
  </Routes>
)

export default FundPage
