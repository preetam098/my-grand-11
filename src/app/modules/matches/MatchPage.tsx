import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import GetMatches from './components/Matches'
import MatchDetail from './components/MatchDetail'

const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'Matches',
    path:'/matches',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const MatchPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='index'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Matches</PageTitle>
            <GetMatches />
          </>
        }
      />
       <Route
        path='detail'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Match Detail</PageTitle>
            <MatchDetail/>
          </>
        }
      />
      <Route index element={<Navigate to='index' />} />
    </Route>
  </Routes>
)

export default MatchPage
