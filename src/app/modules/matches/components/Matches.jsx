import React, {useEffect, useState} from 'react'
import {getMatches} from '../../../redux/actions/matches'
import {useDispatch, useSelector} from 'react-redux'
import {CommonTable} from '../../../component/CommonPagiTable'
import {useLocation, useSearchParams} from 'react-router-dom'
import {useNavigate} from 'react-router-dom'

const GetMatches = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const location = useLocation()
  const getAllMatches = useSelector((state) => state?.matchesReducer?.data?.matches)
  const pagination = useSelector((state) => state?.matchesReducer?.data?.pagination)
  const [params, setparams] = useState({
    start: 0,
    size: 10,
  })

  const handlePrev = () => {
    params.start > 1 &&
      setparams({
        ...params,
        start: params.start - 1,
      })
    navigate(params)
  }

  const handleForw = () => {
    setparams({
      ...params,
      start: params.start + 1,
    })
  }
  
  
  useEffect(() => {
    const callBack = () => {}
    const searchParams = new URLSearchParams(location.search)
    const newQueryParams = {
      start: searchParams.get('start') || '',
      size: searchParams.get('size') || '10',
    }
    setparams(newQueryParams)
    dispatch(getMatches(params, callBack))
  }, [])
  
  
  useEffect(() => {
    const callBack = () => {}
    const queryString = new URLSearchParams(params).toString()
    dispatch(getMatches(params , callBack))
    navigate(`?${queryString}`)
  }, [params])
  
  const handleDetail = (item) => {
    // const itemArray = Object.values(item);
    navigate('/prize_pool/index', {
      state: item,
    })
  }
  
  // useEffect(() => {
    //     const callBack = () => {}
//   dispatch(getMatches(params , callBack))
// }, [])

  return (
    <>
      <CommonTable
        className='mb-5 mb-xl-8'
        title='Matches'
        headData={getAllMatches ? Object.keys(getAllMatches?.[0]) : ''}
        bodyData={getAllMatches}
        total={getAllMatches?.length}
        handlePrev={handlePrev}
        handleForw={handleForw}
        handleDetail={handleDetail}
        params={params}
        pagination={pagination}
        loading={''}
      />
    </>
  )
}

export default GetMatches
