import React, {useState, useEffect} from 'react'
import {useLocation, useNavigate} from 'react-router-dom'
import {useDispatch, useSelector} from 'react-redux'
import {getPrizePool} from '../../../redux/actions/prizepool'

const MatchDetail = () => {
  const location = useLocation()
  const rowData = location.state
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const [params, setparams] = useState({
    match_id: rowData?.id,
  })
  const getPrizePools = useSelector((state) => state?.prizePoolReducer?.data?.pools)
  useEffect(() => {
    const callback = () => {}
    dispatch(getPrizePool(params, callback))
  }, [params])

  console.log(getPrizePools, 'get dtaa')

  return (
    <>
      {getPrizePools?.length > 0 ? (
        <>
          <div className='row'>
            {getPrizePools?.map((pool) => {
              return (
                <>
                  <div className='row sm:gap-2 md:gap-0 '>
                    <div className='col-md-6 mb-2'>
                      <label htmlFor={`amount-${pool.id}`}>Amount</label>
                      <input
                        //   onChange={handleChange}
                        type='text'
                        name='amount'
                        //   disabled={isEdit ? true : false}
                        value={pool?.amount}
                        className='form-control'
                        placeholder='amount'
                      />
                    </div>

                    <div className='col-md-6 mb-2'>
                      <label htmlFor={`entry_fee`}>entry_fee</label>
                      <input
                        //   onChange={handleChange}
                        type='text'
                        name='entry_fee'
                        value={pool?.entry_fee}
                        //   disabled={isEdit ? true : false}
                        className='form-control'
                        placeholder='entry_fee'
                      />
                    </div>
                  </div>

                  <div className='row sm:gap-2 md:gap-0 '>
                    <div className='col-md-6 mb-2'>
                      <label htmlFor={`filled_spots`}>filled_spots</label>
                      <input
                        //   onChange={handleChange}
                        type='text'
                        name='filled_spots'
                        //   disabled={isEdit ? true : false}
                        value={pool?.filled_spots}
                        className='form-control'
                        placeholder='filled_spots'
                      />
                    </div>

                    <div className='col-md-6 mb-2'>
                      <label htmlFor={`fixed_spots`}>fixed_spots</label>

                      <input
                        //   onChange={handleChange}
                        type='text'
                        name='fixed_spots'
                        value={pool?.fixed_spots}
                        //   disabled={isEdit ? true : false}
                        className='form-control'
                        placeholder='fixed_spots'
                      />
                    </div>
                  </div>

                  <div className='row sm:gap-2 md:gap-0 '>
                    <div className='col-md-6 mb-2'>
                      <label htmlFor={`max_prize`}>max_prize</label>

                      <input
                        //   onChange={handleChange}
                        type='text'
                        name='max_prize'
                        //   disabled={isEdit ? true : false}
                        value={pool?.max_prize}
                        className='form-control'
                        placeholder='max_prize'
                      />
                    </div>

                    <div className='col-md-6 mb-2'>
                      <label htmlFor={`max_teams_allowed`}>max_teams_allowed</label>
                      <input
                        //   onChange={handleChange}
                        type='text'
                        name='max_teams_allowed'
                        value={pool?.max_teams_allowed}
                        //   disabled={isEdit ? true : false}
                        className='form-control'
                        placeholder='max_teams_allowed'
                      />
                    </div>
                  </div>

                  <div className='row sm:gap-2 md:gap-0 '>
                    <div className='col-md-6 mb-2'>
                      <label htmlFor={`status`}>status</label>

                      <input
                        //   onChange={handleChange}
                        type='text'
                        name='status'
                        //   disabled={isEdit ? true : false}
                        value={pool?.status}
                        className='form-control'
                        placeholder='status'
                      />
                    </div>

                    <div className='col-md-6 mb-2'>
                      <label htmlFor={`winner_percentage`}>winner_percentage</label>
                      <input
                        //   onChange={handleChange}
                        type='text'
                        name='winner_percentage'
                        value={pool?.winner_percentage}
                        //   disabled={isEdit ? true : false}
                        className='form-control'
                        placeholder='winner_percentage'
                      />
                    </div>
                  </div>
                </>
              )
            })}
          </div>
        </>
      ) : (
        <>
          <div className='d-flex  h-100 justify-content-center align-items-center'>
            <div>
              <h2 className='text-danger'> No Match Detail Available</h2>
            </div>
          </div>
        </>
      )}
    </>
  )
}

export default MatchDetail
