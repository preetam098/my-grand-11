import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import Profit_Loss from './components/Profit-Loss'
import { PageLink, PageTitle } from '../../../_metronic/layout/core'

const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'Contest Profit / Loss',
    path:'/contest_p_l',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const Profit_Loss_Page = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='contest_p_l'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Profit / Loss</PageTitle>
            <Profit_Loss />
          </>
        }
      />
      <Route index element={<Navigate to='contest_p_l' />} />
    </Route>
  </Routes>
)

export default Profit_Loss_Page
