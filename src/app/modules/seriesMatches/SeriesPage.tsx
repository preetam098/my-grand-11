import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import AllMatches from './components/AllMatches'
import Assigned from './components/Assigned'
import Completed from './components/Completed'
import Running from './components/Running'
import Scheduled from './components/Schedule'
import Schedule_Running from './components/Scheduled-Running'
import Abondoned from './components/Abondoned'


const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'Series',
    path: '/series',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const SeriesPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='list'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>All Matches</PageTitle>
            <AllMatches />
          </>
        }
      />
          <Route
        path='schedule-running'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Scheduled-Running</PageTitle>
            <Schedule_Running />
          </>
        }
      />
      
     
      <Route
        path='todays-assigned'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Todays Assigned</PageTitle>
            <Assigned />
          </>
        }
      />
    <Route
        path='scheduled'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Scheduled</PageTitle>
            <Scheduled />
          </>
        }
      />


      <Route
        path='running-matches'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Running</PageTitle>
            <Running />
          </>
        }
      />
       <Route
        path='completed-matches'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Completed</PageTitle>
            <Completed />
          </>
        }
      />
       <Route
        path='abondoned-matches'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Abodoned</PageTitle>
            <Abondoned />
          </>
        }
      />
      <Route index element={<Navigate to='/series/list' />} />
    </Route>
  </Routes>
)

export default SeriesPage
