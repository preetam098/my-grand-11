import {useState} from 'react'
import {CommonTable} from '../../../component/CommonTable'

import SimpleReactValidator from 'simple-react-validator'
import { SelectComponent } from '../../../component/SelectComponent'


const option = [
  {name: 'option 1', value: 1, label: 'Option 1'},
  {name: 'option 2', value: 2, label: 'Option 2'},
  {name: 'option 3', value: 3, label: 'Option 3'},
  {name: 'option 4', value: 4, label: 'Option 4'},
  {name: 'option 5', value: 5, label: 'Option 5'},
]


const OfferCode = () => {
  const [openForm, setOpenForm] = useState(false)
  const [preview, setPreview] = useState("")
  const [formValues, setFormValues] = useState({})
  const [errorMessage, setErrorMessage] = useState({})
  const validator = new SimpleReactValidator({
    className: 'text-danger',
    validators: {
      fileSize: {
        message: 'The :attribute must be max 1MB.',
        rule: function (val, maxSize, validator) {
          return val && val.size <= 1048576
        },
      },
    },
  })

  const handleChange = (event) => {
    const {name, value, type} = event.target
    console.log("fdv" , name , value , event.target)
    setErrorMessage({...errorMessage, [name]: ''})
    if (type === 'file') {
      setPreview(URL.createObjectURL(event.target.files[0]))
      setFormValues({...formValues, [name]: event.target.files[0]})
    } else {
      setFormValues({...formValues, [name]: value})
    }
  }

  const handleOpen = () => {
    setOpenForm(true)
  }

  const handleDiscard=()=>{
    setFormValues("")
    setErrorMessage("")
    setOpenForm(false)
  }
  const handleSubmit = () => {
    if (validator.allValid()) {
      console.log('chl gya')
    setOpenForm(false)
    setFormValues("")
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }

  
  const handleSelect = (e, name) => {
    console.log('e', e.value, name)
    setErrorMessage({...errorMessage, [name]: ''})
    setFormValues({...formValues, [name]: e.value})
  }

  console.log('formValues', formValues)

  return (
    <>
      {openForm && (
        <section className='px-5 rounded bg-white mb-4 mb-xl-8'>
          <div className='border-1  border-gray-300 border-bottom py-5 '>
            <h4 className='mb-0'>Offer Per Match</h4>
          </div>
          <div className=' mt-5 py-1 row'>
            <div className='col-md-6'>
            <div>
                <label>Sport Type </label>
                <SelectComponent
                  className='mb-0'
                  placeholder='Option'
                  options={option}
                  value={formValues?.sportype}
                  handleChange={(e) => handleSelect(e, 'sportype')}
                />
                <div> {validator.message('sportype', formValues?.sportype, 'required')}</div>{' '}
                <p className='text-danger '>{errorMessage?.sportype}</p>
              </div>
            </div>
            <div className='col-md-6'>
            <div>
                <label>Match List </label>
                <SelectComponent
                  className='mb-0'
                  placeholder='Option'
                  options={option}
                  value={formValues?.matchList}
                  handleChange={(e) => handleSelect(e, 'matchList')}
                />
                <div> {validator.message('matchList', formValues?.matchList, 'required')}</div>{' '}
                <p className='text-danger '>{errorMessage?.matchList}</p>
              </div>
            </div>
            <div className='col-md-6'>
            <label>Name</label>

              <input
                onChange={handleChange}
                type='text'
                name='name'
                value={formValues?.name}
                className='form-control'
                placeholder='enter name'/>
              {validator.message('name', formValues?.name, 'required')}
              <div className='text-danger'>{errorMessage?.name}</div>
            </div>
            <div className='col-md-6'>
            <label>Offer</label>
              <input
                onChange={handleChange}
                type='text'
                name='offer'
                value={formValues?.offer}
                className='form-control'
                placeholder='offer'
              />
              {validator.message('offer', formValues?.offer, 'required')}
              <div className='text-danger '>{errorMessage?.offer}</div>
            </div>
          </div>
         

            <div className='gap-3 card-header border-0 p-5 text-end'>
              <button className='btn btn-sm mx-2 btn-light-primary' onClick={handleSubmit}>
                Submit
              </button>
    
           
          </div>
        </section>
      )}

      <section>
        <CommonTable
          title='Series'
          total={100}
          handleOpen={handleOpen}
          className='mb-5 mb-xl-8'
          headData={['Team Name', 'Abbravation', ' Players']}
        />
      </section>
    </>
  )
}

export default OfferCode
