import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import OfferCode from './components/OfferCode'

const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'offer',
    path: '/offer',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const OfferPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='offer'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Offer Code</PageTitle>
            <OfferCode />
          </>
        }
      />
      <Route index element={<Navigate to='offer' />} />
    </Route>
  </Routes>
)

export default OfferPage
