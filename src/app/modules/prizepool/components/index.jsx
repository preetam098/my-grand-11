import {useState} from 'react'
import {useSelector} from 'react-redux'
import MatchDetail from './MatchDetail'
import ContestList from './ContestList'

const Tabs = [
  {
    id: 1,
    name: 'Contest List',
  },
  {
    id: 2,
    name: 'Match Details',
  },
  // {

  //   name: '',
  // },
]

const PrizeIndex = () => {
  const [Tab, setTab] = useState(1)
  const handleTabChange = (id) => {
    setTab(id)
  }

  return (
    <>
      <main>
        <section className='d-flex  align-items-center gap-2'>
          {Tabs?.map((data, i) => {
            return (
              <>
                <div key={i}>
                  <button
                    onClick={() => handleTabChange(data.id)}
                    className={`btn rounded px-4 py-1 border-primary border ${
                      Tab === data.id ? ' btn-primary' : 'btn-light-primary'
                    }`}
                  >
                    {data.name}
                  </button>
                </div>
              </>
            )
          })}
        </section>
        <div>
          {Tab === 1 && <ContestList />}
          {Tab === 2 && <MatchDetail />}
        </div>

        <section></section>
      </main>
    </>
  )
}

export default PrizeIndex
