import {useEffect, useState} from 'react'
import {CommonTable} from '../../../component/CommonTable copy'
import SimpleReactValidator from 'simple-react-validator'
import {
  getPrizePool,
  createPrizePool,
  updatePrizePool,
  deletePrizePool,
  getPrizeDistribution,
} from '../../../redux/actions/prizepool'
import {useDispatch, useSelector} from 'react-redux'
import toast, {Toaster} from 'react-hot-toast'
import Modal from '../../../component/Modal'
import {getMatches} from '../../../redux/actions/matches'
import {SelectComponent} from '../../../component/SelectComponent'
import {useLocation, useNavigate} from 'react-router-dom'

const ContestList = () => {
  const location = useLocation()
  const navigate = useNavigate()
  const rowData = location.state
  const dispatch = useDispatch('')
  const getModeratorDetail = useSelector((state) => state?.moderatorReducer?.data?.accounts)
  const getAllMatches = useSelector((state) => state?.matchesReducer?.data?.matches)
  const prizeDistribution = useSelector((state) => state?.prizePoolReducer?.data)
  const getPrizePools = useSelector((state) => state?.prizePoolReducer?.data?.pools)
  const [openForm, setOpenForm] = useState(false)
  const [loading, setLoading] = useState(false)
  const [formValues, setFormValues] = useState({})
  const [errorMessage, setErrorMessage] = useState({})
  const [mask, setMask] = useState([])
  const [isEdit, setIsEdit] = useState(false)
  const [IsRes, setIsRes] = useState(false)
  const [modals, setModals] = useState(false)
  const [deleteData, setDelete] = useState('')
  const [ActiveTab, setActiveTab] = useState(1)
  const validator = new SimpleReactValidator({
    className: 'text-danger',
  })
  const [params, setparams] = useState({
    match_id: rowData?.id,
  })



  const handleChange = (event) => {
    const {name, value, type} = event.target
    setErrorMessage({...errorMessage, [name]: ''})
    setFormValues({...formValues, [name]: value})
    if (!mask.includes(name)) {
      setMask([...mask, name])
    }
  }


  console.log(rowData , 'rowwwdaara')

  const handleDiscard = () => {
    setActiveTab(1)
    setFormValues('')

    setErrorMessage('')
    setOpenForm(false)
    setIsEdit(false)
  }

  const handleSubmit = () => {
    setIsEdit(false)
    if (validator.allValid()) {
      const callback = (response) => {
        dispatch(getPrizePool())
        toast.success('Account Created Successfully')
        setOpenForm(false)
        setLoading(false)
        setFormValues('')
      }
      const payload = {
        ...formValues,
        entry_fee: parseInt(formValues?.entry_fee),
        fixed_spots: parseInt(formValues?.fixed_spots),
        match_id: formValues?.match_id?.id,
        max_teams_allowed: parseInt(formValues?.max_teams_allowed),
        prize_dist: {
          dist_list: formValues?.dist_list,
        },
        max_prize: parseInt(formValues?.max_prize),
        winnners: parseInt(formValues?.winnners),
      }
      dispatch(createPrizePool(payload, callback))
    } else {
      validator.showMessages()
      setIsEdit(false)
      setErrorMessage(validator.errorMessages)
    }
  }

  const handlePrizeDistribution = () => {
    setIsEdit(false)
    if (validator.allValid()) {
      const callback = (response) => {
        setActiveTab(2)
        dispatch(getMatches())
        // dispatch(getPrizePool(params, callback))
        setIsRes(true)
        setLoading(false)
        setFormValues(...formValues, response?.data?.distribution[0])
        setFormValues({...formValues , rowData})
      }
      const payload = {
        entry_fee: parseInt(formValues?.entry_fee),
        spot_size: parseInt(formValues?.spot_size),
      }
      dispatch(getPrizeDistribution(payload, callback))
    } else {
      validator.showMessages()
      setIsEdit(false)
      setErrorMessage(validator.errorMessages)
    }
  }

  const handleEdit = (data) => {
    setFormValues(data)
    setOpenForm(true)
    setActiveTab(2)
    window.scrollTo({top: 0, behavior: 'smooth'})
    setIsEdit(true)
  }

  const handleUpdate = () => {
    if (mask.length === 0) {
      handleDiscard()
    } else {
      const callback = (response) => {
        setOpenForm(false)
        setLoading(false)
        dispatch(getPrizePool(params, callback))

        toast.success('Account Update Successfully')
      }
      dispatch(updatePrizePool(formValues.id, formValues, mask, callback))
    }
  }

  useEffect(() => {
    const callback = () => {}
   
      dispatch(getMatches(callback))
 
  }, [params])

  useEffect(() => {
    const callback = (response) => {
    }
    dispatch(getPrizePool(params, callback))
  }, [params])

  const handleOpen = (item) => {
    setOpenForm(true)
  }

  const handleDeleteUser = () => {
    const callback = () => {
      setModals(false)
      setLoading(false)
      dispatch(getPrizePool())
      toast.success('Account Delete Successfull')
      navigate('/matches/index')
    }
    dispatch(deletePrizePool(deleteData.id, deleteData, callback))
  }

  const handleModalOpen = (item) => {
    setDelete(item)
    setModals(true)
  }

  const handleSelect = (e, name) => {
    setErrorMessage({...errorMessage, [name]: ''})
    setFormValues({...formValues, match_id: e})
  }

  const handleCloseModal = (name) => {
    setModals(false)
  }

  const handlePrizeValue = (item ) => {
    setFormValues(item )
    // setFormValues({
    //   ...formValues,
    //   prize_dist: item,
    // })
  }



  return (
    <>
      {!loading ? (
        <>
          <main>
            {openForm && (
              <section className='px-5 my-5 rounded bg-white pb-4 mb-4 mb-xl-8'>
                <div className='border-1 mb-4 sm:mb-2 md:mb-2 border-gray-300 border-bottom py-5 '>
                  <h4 className='mb-0'>Create New Prize Pool</h4>
                </div>
                <>
                  <div className='row sm:gap-2 md:gap-0 '>
                    <div className='col-md-6 mb-2'>
                      <input
                        onChange={handleChange}
                        type='text'
                        name='entry_fee'
                        disabled={isEdit ? true : false}
                        value={formValues?.entry_fee}
                        className='form-control'
                        placeholder='entry_fee'
                      />
                      {validator.message('entry_fee', formValues?.entry_fee, 'required|integer', {
                        className: 'text-danger ',
                      })}
                      <div className='text-danger '>{errorMessage?.entry_fee}</div>
                    </div>

                    <div className='col-md-6 mb-2'>
                      <input
                        onChange={handleChange}
                        type='text'
                        name='spot_size'
                        value={formValues?.spot_size}
                        disabled={isEdit ? true : false}
                        className='form-control'
                        placeholder='spot_size'
                      />
                      {validator.message('spot_size', formValues?.spot_size, 'required|integer', {
                        className: 'text-danger ',
                      })}
                      <div className='text-danger '>{errorMessage?.spot_size}</div>
                    </div>
                  </div>

                  <div className={`${ActiveTab === 1 ? 'd-none' : 'd-block'}`}>
                    <div className='d-flex'>
                      {prizeDistribution?.distribution?.map((item, index) => {
                        return (
                          <button
                            key={index}
                            onClick={() => handlePrizeValue(item)}
                            className={`d-flex btn ${formValues === item ? 'text-white bg-primary' : ' btn-light-primary'}   mx-2 py-1 mx-2 mb-4`}
                          >
                            Winner {index + 1}
                          </button>
                        )
                      })}
                    </div>

                    <div className=' row sm:gap-2 md:gap-0 '>
                      <div className='col-md-6 '>
                        <SelectComponent
                          className='mb-0'
                          placeholder='match name'
                          options={getAllMatches?.map((match) => ({
                            id: match.id,
                            label: match.name,
                          }))}
                          value={formValues?.match_id}
                          handleChange={(e) => handleSelect(e, 'match_id')}
                        />
                        <div>
                          {' '}
                          {IsRes && validator.message('match_id', formValues?.match_id, 'required')}
                        </div>{' '}
                        <p className='text-danger '>{errorMessage?.match_id}</p>
                      </div>

                      <div className='col-md-6 mb-2'>
                        <input
                          onChange={handleChange}
                          type='text'
                          name='max_teams_allowed'
                          value={formValues?.max_teams_allowed}
                          disabled={isEdit ? true : false}
                          className='form-control'
                          placeholder='max_teams_allowed'
                        />
                        {IsRes &&
                          validator.message(
                            'max_teams_allowed',
                            formValues?.max_teams_allowed,
                            'required|string'
                          )}
                        <div className='text-danger '>{errorMessage?.max_teams_allowed}</div>
                      </div>
                    </div>

                    <div className=' row sm:gap-2 md:gap-0 '>
                      <div className='col-md-6 mb-2'></div>
                      <div>
                        {formValues?.dist_list?.map((item, index) => (
                          <div className='row md:gap-0' key={index}>
                            <div className='col-md-6 mb-2'>
                              <input
                                onChange={handleChange}
                                type='text'
                                name={`winner-${index}`}
                                value={item.winner}
                                className='form-control'
                                placeholder='winner'
                              />
                            </div>
                            <div className='col-md-6 mb-2'>
                              <input
                                onChange={handleChange}
                                type='text'
                                name={`prize-${index}`}
                                value={item.prize}
                                className='form-control'
                                placeholder='prize'
                              />
                            </div>
                          </div>
                        ))}
                      </div>
                    </div>

                    <div className=' row sm:gap-2 md:gap-0   '>
                      <div className='col-md-6 mb-2'>
                        <input
                          onChange={handleChange}
                          type='text'
                          name='max_prize'
                          value={formValues?.max_prize}
                          className='form-control'
                          placeholder='max_prize'
                        />
                        {IsRes &&
                          validator.message('max_prize', formValues?.max_prize, 'required|string')}
                        <div className='text-danger '>{errorMessage?.max_prize}</div>
                      </div>

                      <div className='col-md-6 mb-2'>
                        <input
                          onChange={handleChange}
                          type='text'
                          name='winnners'
                          value={formValues?.winnners}
                          className='form-control'
                          placeholder='winnners'
                        />
                        {IsRes &&
                          validator.message('winnners', formValues?.winnners, 'required|string')}
                        <div className='text-danger '>{errorMessage?.winnners}</div>
                      </div>
                    </div>

                    <div className='row sm:gap-2 md:gap-0 '>
                      <div className='col-md-6 mb-2'>
                        <input
                          onChange={handleChange}
                          type='text'
                          name='fixed_spots'
                          value={formValues?.fixed_spots}
                          disabled={isEdit ? true : false}
                          className='form-control'
                          placeholder='fixed_spots'
                        />
                        {IsRes &&
                          validator.message(
                            'fixed_spots',
                            formValues?.fixed_spots,
                            'required|integer',
                            {
                              className: 'text-danger ',
                            }
                          )}
                        <div className='text-danger '>{errorMessage?.fixed_spots}</div>
                      </div>
                    </div>
                  </div>
                </>

                <div className='gap-3 card-header border-0 p-5 text-end'>
                  {ActiveTab === 1 ? (
                    <>
                      <button
                        className='btn btn-sm mx-2 btn-light-primary'
                        onClick={handlePrizeDistribution}
                      >
                        Get Prize
                      </button>
                    </>
                  ) : (
                    <>
                      {isEdit ? (
                        <button
                          className='btn btn-sm mx-2 btn-light-primary'
                          onClick={handleUpdate}
                        >
                          Update
                        </button>
                      ) : (
                        <button
                          className='btn btn-sm mx-2 btn-light-primary'
                          onClick={handleSubmit}
                        >
                          Submit
                        </button>
                      )}
                    </>
                  )}

                  <button onClick={handleDiscard} className='btn btn-sm btn-light-danger'>
                    Discard
                  </button>
                </div>
              </section>
            )}

            <Modal
              showModal={modals}
              handleClose={handleCloseModal}
              handleModal={handleModalOpen}
              handleDelete={handleDeleteUser}
              title={"Contest List"}
            />

            <section>

            <section className='my-5'>
              <CommonTable
                title='Contest List'
                total={getPrizePools?.length}
                handleOpen={handleOpen}
                handleEdit={handleEdit}
                handleModel={handleModalOpen}
                className='mb-5 mb-xl-8'
                loading={loading}
                headData={getPrizePools ? Object.keys(getPrizePools?.[0]) : loading}
                bodyData={getPrizePools}
              />
            </section>
            </section>
            <Toaster />
          </main>
        </>
      ) : (
        <>
          <div>please Wait</div>
        </>
      )}
    </>
  )
}

export default ContestList
