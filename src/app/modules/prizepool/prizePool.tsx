import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import PrizeIndex from './components/index'

const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'Contest List',
    path:'/prize_pool',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const ModeratorPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='index'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Contest List</PageTitle>
            <PrizeIndex />
          </>
        }
      />
      <Route index element={<Navigate to='index'/>} />
    </Route>
  </Routes>
)

export default ModeratorPage
