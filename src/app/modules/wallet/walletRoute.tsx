import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import View from './components/view'
import Gpay from './components/gpay'
import Paytm from './components/paytm'
import Payu from './components/payu'


const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'wallet',
    path: '/wallet',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const WalletPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='view'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>View</PageTitle>
            <View />
          </>
        }
      />
      
      <Route
        path='payu-transactions'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}> Payu Transactions</PageTitle>
            <Payu />
          </>
        }
      />
      <Route
        path='paytm-transactions'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}> Paytm Transactions</PageTitle>
            <Paytm />
          </>
        }
      />
      <Route
        path='gpay-transactions'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}> Gpay</PageTitle>
            <Gpay />
          </>
        }
      />
      <Route index element={<Navigate to='/wallet/paytm' />} />
    </Route>
  </Routes>
)

export default WalletPage
