import React, {useEffect, useState} from 'react'
import {CommonTable} from '../../../component/CommonTable copy'
import toast, {Toaster} from 'react-hot-toast'
import SimpleReactValidator from 'simple-react-validator'
import {
  createSportFormatAction,
  deleteSportFormatAction,
  getSportFormatAction,
  updateSportFormatAction,
} from '../../../redux/actions/sportformat'
import {useDispatch, useSelector} from 'react-redux'
import Modal from '../../../component/Modal'
import {SelectComponent} from '../../../component/SelectComponent'
import {getSportsAction} from '../../../redux/actions/sport'


const Sport_Format = () => {
  const dispatch = useDispatch()
  const getSportsFormat = useSelector((state) => state?.sportFormatReducer?.data?.sport_formats)
  const getAllSports = useSelector((state) => state?.sportReducer?.data?.sports)
  const [openform, setOpenForm] = useState(false)
  const [formValues, setFormValues] = useState({})
  const validator = new SimpleReactValidator({})
  const [errorMessage, setErrorMessage] = useState({})
  const [isEdit, setIsEdit] = useState(false)
  const [mask, setMask] = useState([])
  const [modals, setModals] = useState(false)
  const [deleteData, setDelete] = useState('')

  useEffect(() => {
    dispatch(getSportFormatAction())
    dispatch(getSportsAction())
  }, [])

  const handleOpen = () => {
    setOpenForm(!openform)
  }

  const handleSelect = (e, name) => {
    console.log('e', e)
    setErrorMessage({...errorMessage, [name]: ''})
    setFormValues({...formValues, sport_id: e})
  }

  const handleChange = (event) => {
    const {name, value} = event.target
    setFormValues({...formValues, [name]: value})
    setErrorMessage({...errorMessage, [name]: ''})
    if (!mask.includes(name)) {
      setMask([...mask, name])
    }
  }

  const handleDiscard = () => {
    setFormValues('')
    setErrorMessage('')
    setIsEdit(false)
    setOpenForm(false)
  }

  const handleSubmit = () => {
    if (validator.allValid()) {
      const callback = (response) => {
        toast.success('Account Created SuccessFully')
        setFormValues('')
        setOpenForm(false)
        dispatch(getSportFormatAction())
      }
      const payload = {
        ...formValues,
        sport_id: formValues.sport_id.value,
      }
      dispatch(createSportFormatAction(payload, callback))
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }

  const handleUpdate = () => {
    if (validator.allValid()) {
      if (mask.length === 0) {
        handleDiscard()
      } else {
        const callback = () => {
          setFormValues('')
          setOpenForm(false)
          dispatch(getSportFormatAction())
          toast.success('Account Update Successfully')
        }
        const payload = {
          ...formValues,
          sport_id: formValues.sport_id.value,
        }
        dispatch(updateSportFormatAction(formValues.id, payload, mask, callback))
      }
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }

  const handleEdit = (data) => {
    setIsEdit(true)
    const getFilter = getAllSports?.filter((item) => item.id == data.sport_id)
    const sportData = {
      value: getFilter[0]?.id,
      label: getFilter[0]?.name,
    }
    console.log(data, sportData)
    setFormValues({...data, sport_id: sportData})
    setOpenForm(true)
    // Scroll to the top of the screen
    window.scrollTo({top: 0, behavior: 'smooth'})
  }

  const handleDeleteUser = () => {
    const callback = () => {
      setModals(false)
      dispatch(getSportFormatAction())
      toast.success('Account Delete Successfull')
    }
    dispatch(deleteSportFormatAction(deleteData.id, deleteData, callback))
  }

  const handleModalOpen = (item) => {
    setDelete(item)
    setModals(true)
  }

  const handleCloseModal = (name) => {
    setDelete('')
    setModals(false)
  }

  return (
    <>
      <main>
        {openform && (
          <section className='px-5 rounded bg-white pb-4 mb-4 mb-xl-8'>
            <div className='border-1 mb-4 sm:mb-2 md:mb-2 border-gray-300 border-bottom py-5 '>
              <h4 className='mb-0'>Create New Sport</h4>
            </div>
            <div className='row sm:gap-2 md:gap-0 '>
              <>
                <div className='col-md-6 mb-0'>
                  <SelectComponent
                    className='mb-0'
                    placeholder='sport name'
                    options={getAllSports.map((sport) => ({value: sport.id, label: sport.name}))}
                    value={formValues?.sport_id}
                    handleChange={(e) => handleSelect(e)}
                  />
                  <div> {validator.message('sport_id', formValues?.sport_id, 'required')}</div>{' '}
                  <p className='text-danger '>{errorMessage?.sport_id}</p>
                </div>
              </>

              <div className='col-md-6 '>
                <input
                  onChange={handleChange}
                  type='text'
                  name={isEdit ? 'name' : 'format_name'}
                  value={isEdit ? formValues?.name : formValues?.format_name}
                  className='form-control'
                  placeholder={isEdit ? 'name' : 'format_name'}
                />
                {validator.message(
                  isEdit ? 'name' : 'format_name',
                  isEdit ? formValues?.name : formValues?.format_name,
                  'required|string',
                  {
                    className: 'text-danger ',
                  }
                )}
                <div className='text-danger'>
                  {isEdit ? errorMessage?.name : errorMessage?.format_name}
                </div>

                {/* <input
                  onChange={handleChange}
                  type='text'
                  name={isEdit ? 'name' : 'format_name'}
                  value={formValues?.name}
                  className='form-control'
                  placeholder={isEdit ? 'name' : 'format_name'}
                />
                {validator.message(
                  `${isEdit ? 'name' : 'format_name'}`,
                  `${isEdit ? formValues?.name : formValues?.format_name}`,
                  'required|string',
                  {
                    className: 'text-danger ',
                  }
                )}
                <div className='text-danger '>
                  {isEdit ? errorMessage?.name : errorMessage?.format_name} {}
                </div> */}
              </div>
            </div>

            <div className='gap-3 xs:text-center sm:text-center card-header border-0 p-5 text-end'>
              {isEdit ? (
                <button onClick={handleUpdate} className='btn btn-sm mx-2 btn-light-primary'>
                  Update
                </button>
              ) : (
                <button
                  onClick={handleSubmit}
                  className='btn btn-sm my-2 md:my-0 mx-2 btn-light-primary'
                >
                  Submit
                </button>
              )}

              <button onClick={handleDiscard} className='btn btn-sm btn-light-danger'>
                Discard
              </button>
            </div>
          </section>
        )}
        <Toaster />
      </main>

      <Modal
        showModal={modals}
        handleClose={handleCloseModal}
        handleModal={handleModalOpen}
        handleDelete={handleDeleteUser}
      />

      <CommonTable
        title='Sport Format'
        total={getSportsFormat?.length}
        handleOpen={handleOpen}
        handleEdit={handleEdit}
        handleModel={handleModalOpen}
        className='mb-5 mb-xl-8'
        headData={getSportsFormat ? Object.keys(getSportsFormat?.[0]) : []}
        bodyData={getSportsFormat}
      />
    </>
  )
}

export default Sport_Format
