import React, {useEffect, useState} from 'react'
import {CommonTable} from '../../../component/CommonTable copy'
import toast, {Toaster} from 'react-hot-toast'
import SimpleReactValidator from 'simple-react-validator'
import {
  createSportAction,
  deleteSportAction,
  getSportsAction,
  updateSportAction,
} from '../../../redux/actions/sport'
import {useDispatch, useSelector} from 'react-redux'
import Modal from '../../../component/Modal'

const AllSport = () => {
  const dispatch = useDispatch()
  const getAllSports = useSelector((state) => state?.sportReducer?.data?.sports)
  const [openform, setOpenForm] = useState(false)
  const [formValues, setFormValues] = useState({})
  const validator = new SimpleReactValidator({})
  const [errorMessage, setErrorMessage] = useState({})
  const [isEdit, setIsEdit] = useState(false)
  const [mask, setMask] = useState([])
  const [modals, setModals] = useState(false)
  const [deleteData, setDelete] = useState('')
  const [loading, setLoading] = useState(false)




  useEffect(() => {
    dispatch(getSportsAction())
  }, [])

  const handleOpen = () => {
    setOpenForm(!openform)
  }

  const handleChange = (event) => {
    const {name, value} = event.target
    setFormValues({...formValues, [name]: value})
    setErrorMessage({...errorMessage, [name]: ''})
    if (!mask.includes(name)) {
      setMask([...mask, name])
    }
  }

  const handleDiscard = () => {
    setFormValues('')
    setErrorMessage('')
    setIsEdit(false)
    setOpenForm(false)
  }

  const handleSubmit = () => {
    if (validator.allValid()) {
      const callback = (response) => {
        toast.success('Account Created SuccessFully')
        setFormValues('')
        setOpenForm(false)
        dispatch(getSportsAction())
      }
      dispatch(createSportAction(formValues, callback))
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }

  const handleUpdate = () => {
    if (mask.length === 0) {
      handleDiscard()

      }else{
    const callback = (response) => {
      setFormValues('')
      setOpenForm(false)
      setIsEdit(false)
      dispatch(getSportsAction())
      toast.success('Account Update Successfully')
    }
    dispatch(updateSportAction(formValues.id, formValues, mask, callback))
  }
  }

  const handleEdit = (data) => {
    setFormValues(data)
    setOpenForm(true)
    setIsEdit(true)
    // Scroll to the top of the screen
    window.scrollTo({top: 0, behavior: 'smooth'})
  }

  const handleDeleteUser = () => {
    const callback = () => {
      setModals(false)
      dispatch(getSportsAction())
      toast.success('Account Delete Successfull')
    }
    dispatch(deleteSportAction(deleteData.id, deleteData, callback))
  }

  const handleModalOpen = (item) => {
    setDelete(item)
    setModals(true)
  }

  const handleCloseModal = (name) => {
    setDelete('')
    setModals(false)
  }

  return (
    <>
      <main>
        {openform && (
          <section className='px-5 rounded bg-white pb-4 mb-4 mb-xl-8'>
            <div className='border-1 mb-4 sm:mb-2 md:mb-2 border-gray-300 border-bottom py-5 '>
              <h4 className='mb-0'>Create New Sport</h4>
            </div>
            <div className='row sm:gap-2 md:gap-0 '>
              <div className='col-md-6 mb-2'>
                <input
                  onChange={handleChange}
                  type='text'
                  name='name'
                  value={formValues?.name}
                  className='form-control'
                  placeholder='sport name'
                />
                {validator.message('name', formValues?.name, 'required|string', {
                  className: 'text-danger ',
                })}
                <div className='text-danger '>{errorMessage?.name}</div>
              </div>
            </div>

            <div className='gap-3 card-header border-0 p-5 text-end'>
              {isEdit ? (
                <button onClick={handleUpdate} className='btn btn-sm mx-2 btn-light-primary'>
                  Update
                </button>
              ) : (
                <button onClick={handleSubmit} className='btn btn-sm mx-2 btn-light-primary'>
                  Submit
                </button>
              )}

              <button onClick={handleDiscard} className='btn btn-sm btn-light-danger'>
                Discard
              </button>
            </div>
          </section>
        )}
        <Toaster />
      </main>


      <Modal
        showModal={modals}
        handleClose={handleCloseModal}
        handleModal={handleModalOpen}
        handleDelete={handleDeleteUser}
      />

      <CommonTable
        title='Sports'
        total={getAllSports?.length}
        handleOpen={handleOpen}
        handleEdit={handleEdit}
        handleModel={handleModalOpen}
        className='mb-5 mb-xl-8'
        headData={getAllSports ? Object.keys(getAllSports?.[0]) : []}
        bodyData={getAllSports}
      />
    </>
  )
}

export default AllSport
