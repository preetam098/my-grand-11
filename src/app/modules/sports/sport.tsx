import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import AllSport from './components/AllSport'
import Sport_Format from './components/SportFormat'

const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'sport',
    path:'/sports',
    isSeparator: false,
    isActive: false,
  },
]

const SportPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='index'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Sport</PageTitle>
            <AllSport />
          </>
        }
      />
      <Route
        path='sport-format'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Sport Format</PageTitle>
            <Sport_Format />
          </>
        }
      />
      <Route index element={<Navigate to='index' />} />
    </Route>
  </Routes>
)

export default SportPage
