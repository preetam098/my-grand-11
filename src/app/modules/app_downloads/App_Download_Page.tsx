import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import App_Download from './components/App_Download'

const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'App Download',
    path:'/app-download',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const App_Download_Page = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='app-download'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>App Download</PageTitle>
            <App_Download />
          </>
        }
      />
      <Route index element={<Navigate to='app-download' />} />
    </Route>
  </Routes>
)

export default App_Download_Page
