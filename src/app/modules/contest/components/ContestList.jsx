import React, {useState} from 'react'
import {CommonTable} from '../../../component/CommonTable'
import {SelectComponent} from '../../../component/SelectComponent'
import SimpleReactValidator from 'simple-react-validator'

const option = [
  {name: 'option 1', value: 1, label: 'Option 1'},
  {name: 'option 2', value: 2, label: 'Option 2'},
  {name: 'option 3', value: 3, label: 'Option 3'},
  {name: 'option 4', value: 4, label: 'Option 4'},
  {name: 'option 5', value: 5, label: 'Option 5'},
]

const ContentList = () => {
  const [openForm, setOpenForm] = useState(false)

  const [formValues, setFormValues] = useState({})
  const [errorMessage, setErrorMessage] = useState({})
  const validator = new SimpleReactValidator({})

  const handleChange = (e) => {
    setFormValues({...formValues, [e.target.name]: e.target.value})
    setErrorMessage({...errorMessage, [e.target.name]: ''})
  }

  console.log('form', formValues)

  const handleDiscard = () => {
    setFormValues('')
    setErrorMessage('')
    setOpenForm(false)
  }

  const handleSelect = (e, name) => {
    console.log('e', e.value, name)
    setErrorMessage({...errorMessage, [name]: ''})
    setFormValues({...formValues, [name]: e.value})
  }

  const handleSubmit = () => {
    if (validator.allValid()) {
      console.log('chl gya')
      setOpenForm(false)
      setFormValues('')
    } else {
      validator.showMessages()
      console.log(validator.errorMessages)
      setErrorMessage(validator.errorMessages)
    }
  }

  const handleOpen = () => {
    setOpenForm(!openForm)
  }

  return (
    <>
      {openForm && (
        <section className=' rounded bg-white mb-4 mb-xl-8'>
          <div className='border-1  border-gray-300 border-bottom p-5 '>
            <h4 className='mb-0'>Add Contest</h4>
          </div>
          <div className='px-5 my-2  row '>
            <div className='col-md-6 mb-2'>
              <div>
                <input
                  onChange={handleChange}
                  type='text'
                  name='ContestName'
                  value={formValues?.ContestName}
                  className='form-control'
                  placeholder='Contest Name'
                />
                {validator.message(
                  'ContestName',
                  formValues?.ContestName,
                  'required|string',
                  {
                    className: 'text-danger ',
                  }
                )}
                <div className='text-danger '>{errorMessage?.ContestName}</div>
              </div>

              <div className='mt-2'>
                <input
                  onChange={handleChange}
                  type='text'
                  name='EntryFee'
                  value={formValues?.EntryFee}
                  className='form-control'
                  placeholder='Entry Fee'
                />
                {validator.message('EntryFee', formValues?.EntryFee, 'required|integer', {
                  className: 'text-danger ',
                })}
                <div className='text-danger '>{errorMessage?.EntryFee}</div>
              </div>
            </div>
            <div className='col-md-6 '>
              <div>
                <input
                  onChange={handleChange}
                  type='text'
                  name='TotalWinningAmount'
                  value={formValues?.TotalWinningAmount}
                  className='form-control'
                  placeholder='Total Winning Amount'
                />
                {validator.message(
                  'TotalWinningAmount',
                  formValues?.TotalWinningAmount,
                  'required|integer',
                  {
                    className: 'text-danger ',
                  }
                )}
                <div className='text-danger '>{errorMessage?.TotalWinningAmount}</div>
              </div>
              <div className='mt-2'>
                <input
                  onChange={handleChange}
                  type='text'
                  name='TotalTeams'
                  value={formValues?.TotalTeams}
                  className='form-control'
                  placeholder='Total Teams'
                />
                {validator.message('TotalTeams', formValues?.TotalTeams, 'required|integer', {
                  className: 'text-danger ',
                })}
                <div className='text-danger '>{errorMessage?.TotalTeams}</div>
              </div>
            </div>
          </div>
          <div className='px-5 '>
            <div className='row gap-2px '>
              <div className='col-md-4 mb-2'>
                <input
                  onChange={handleChange}
                  type='text'
                  name='TeamsPerUser'
                  value={formValues?.TeamsPerUser}
                  className='form-control'
                  placeholder='Teams Per User'
                />
                {validator.message(
                  'TeamsPerUser',
                  formValues?.TeamsPerUser,
                  'required|integer',
                  {
                    className: 'text-danger ',
                  }
                )}
                <div className='text-danger '>{errorMessage?.TeamsPerUser}</div>
              </div>
              <div className='col-md-4 mb-2 '>
                <input
                  onChange={handleChange}
                  type='text'
                  name='MinimumReqTeams'
                  value={formValues?.MinimumReqTeams}
                  className='form-control'
                  placeholder='Minimum Req. Teams'
                />
                {validator.message(
                  'MinimumReqTeams',
                  formValues?.MinimumReqTeams,
                  'required|integer',
                  {
                    className: 'text-danger ',
                  }
                )}
                <div className='text-danger '>{errorMessage?.MinimumReqTeams}</div>
              </div>
              <div className='col-md-4 mb-2'>
                <input
                  onChange={handleChange}
                  type='text'
                  name='WinningBreakups'
                  value={formValues?.WinningBreakups}
                  className='form-control'
                  placeholder='Winning Breakups?'
                />
                {validator.message(
                  'WinningBreakups',
                  formValues?.WinningBreakups,
                  'required|integer',
                  {
                    className: 'text-danger ',
                  }
                )}
                <div className='text-danger '>{errorMessage?.WinningBreakups}</div>
              </div>
            </div>
          </div>
          <div className='px-5 py-1 row my-2'>
            <div className='col-md-6'>
              <div className='mb-0'>
                <SelectComponent
                  className='mb-0'
                  placeholder='Contest Category'
                  options={option}
                  value={formValues?.ContestCategory}
                  handleChange={(e) => handleSelect(e, 'ContestCategory')}
                />
                <div>
                  {' '}
                  {validator.message('ContestCategory', formValues?.ContestCategory, 'required')}
                </div>{' '}
                <p className='text-danger '>{errorMessage?.ContestCategory}</p>
              </div>
            </div>
            <div className='col-md-6'>
              <div className='mb-0'>
                <SelectComponent
                  className='mb-0'
                  placeholder='Is CanceLabel'
                  options={option}
                  value={formValues?.ForTeam}
                  handleChange={(e) => handleSelect(e, 'isCanceLabel')}
                />
                <div>
                  {' '}
                  {validator.message('isCanceLabel', formValues?.isCanceLabel, 'required')}
                </div>{' '}
                <p className='text-danger '>{errorMessage?.isCanceLabel}</p>
              </div>
            </div>
          </div>

          <div className=' my-3 d-flex justify-content-center gap-4 text-center'>
            <div>
              <h4>Rank From</h4>
              <span>0</span>
            </div>
            <div>
              <h4>Rank To</h4>
              <span>0</span>
            </div>
            <div>
              <h4>Winning Amount</h4>
              <span>0</span>
            </div>
          </div>

          <div className='gap-3 card-header border-0 px-5 py-4 text-end'>
            <button className='btn btn-sm mx-2 btn-light-primary' onClick={handleSubmit}>
              Submit
            </button>
            <button onClick={handleDiscard} className='btn btn-sm btn-light-danger  '>
              Discard
            </button>
          </div>
        </section>
      )}

      <section>
        <CommonTable
          title='Contest'
          total={100}
          handleOpen={handleOpen}
          className='mb-5 mb-xl-8'
          headData={['Series Name', 'Total Team', 'Start Date', 'End Date', 'Series Status']}
        />
      </section>
    </>
  )
}

export default ContentList
