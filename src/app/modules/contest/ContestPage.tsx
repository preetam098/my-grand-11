import {Navigate, Routes, Route, Outlet} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import ContentList from './components/ContestList'
import ContentAssignments from './components/ContestAssignments'


const masterBreadCrumbs: Array<PageLink> = [
  {
    title: 'contest',
    path: '/contest',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const ContestPage = () => (
  <Routes>
    <Route
      element={
        <>
          <Outlet />
        </>
      }
    >
      <Route
        path='list'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Content List</PageTitle>
            <ContentList />
          </>
        }
      />
      
      <Route
        path='assignments'
        element={
          <>
            <PageTitle breadcrumbs={masterBreadCrumbs}>Content Assignments</PageTitle>
            <ContentAssignments />
          </>
        }
      />
      <Route index element={<Navigate to='/contest/list' />} />
    </Route>
  </Routes>
)

export default ContestPage
