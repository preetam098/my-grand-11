import React from 'react'
import {FC} from 'react'
import Select from 'react-select'

type Props = {
  className: string
  options: Option[];
  value: any,
  placeholder:string,
  handleChange: (selectedOption: Option | null) => void;
}
interface Option {
    label: string;
    value: string;
  }

const SelectComponent: React.FC<Props> = ({ placeholder, options , value , handleChange}) => {
  return (
    <>
      <div className=''>
        <Select
          className='react-select-styled react-select-solid'
          classNamePrefix='react-select'
          options={options}
          value={value}
          onChange={handleChange}
          placeholder={placeholder}
        />
      </div>
    </>
  )
}

export {SelectComponent}
