import React, { useEffect } from "react";
// import { MdExpandLess } from "react-icons/md";

const Pagination = (props) => {
  const { handlePrev, from, to, total,isDisable , handleForw } = props;
  const end = to >= total ? total : to;

  
  useEffect(() => {
    if (from >= total) {
      handlePrev();
    }
  }, [total, from, handlePrev]);

  return (
    
    total > 0 && (
        <div class="mt-3 p-3 py-2.5 rounded border-gray-400 text-xs bg-gray-200 d-flex align-items-center justify-content-between">
        <div class="fs-6">
            {total > 0 ? from + 1 : from || 0}
            {` - ${end || 0}`} of {total || 0}
        </div>
        <div class="d-flex justify-content-end gap-1">
            {/* <!-- Previous --> */}
            <button type="button" onClick={handlePrev} class="
            btn btn-sm mx-2 btn-light-primary
            w-25  rounded-circle d-flex justify-content-center border-0  align-items-center cursor-pointer">
            <i class="bi bi-arrow-left-short fs-1 text-2xl"></i>  
            </button>
    
            {/* <!-- next --> */}
            <button disabled={!isDisable && end === total} type="button" onClick={handleForw} class="w-25 rounded-circle border-0 d-flex justify-content-center align-items-center  btn btn-sm mx-2 btn-light-primary cursor-pointer">
            <i class="bi bi-arrow-right-short fs-1 text-2xl "></i>    
            </button>
        </div>
    </div>  
    )
  );
};

export default Pagination;
