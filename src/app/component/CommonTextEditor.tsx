import React , {useState , useRef} from 'react'
import JoditEditor from 'jodit-react';


const CommonTextEditor = () => {
    const editor = useRef(null);
    const [content, setContent] = useState('');
  
    

  return (
    <JoditEditor
    ref={editor}
    value={content}
    onBlur={newContent => setContent(newContent)} 
    onChange={newContent => {}}
/>
  )
}

export default CommonTextEditor