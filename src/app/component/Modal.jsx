import React, { useState } from 'react';


const Modal = ({ showModal,  handleClose,handleModal  , handleDelete , title}) => {

  return (
    <>
      {showModal ? (
        <>
          <div className='modal-wrapper'>
            <div className='bg-secondary modal-backdrop opacity-75' onClick={handleModal}></div>
            <div className='modal fade show' tabIndex='-1' style={{ display: 'block' }}>
              <div className='modal-dialog modal-dialog-centered'>
                <div className='modal-content'>
                  <div className='modal-header'>
                    <h5 className='modal-title'>Are You Sure</h5>
                    <button type='button' className='btn-close' onClick={handleClose}>
                      </button>
                  </div>
                  {/* <div className='modal-body'>Are You Sure To Delete ?</div> */}
                  <div className='modal-footer'>
                    <button type='button' className='btn btn-light-danger' onClick={handleClose}>
                      Cancel
                    </button>
                    <button onClick={handleDelete} type='button' className='btn btn-light-primary'>
                    Delete {title}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : null}
    </>
  );
};

export default Modal;
