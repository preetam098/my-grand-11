/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {useState} from 'react'
import {KTIcon, toAbsoluteUrl} from '../../_metronic/helpers'
import {Dropdown1} from '../../_metronic/partials'
import {string} from 'yup'
import Modal from './Modal'
import Pagination from './Pagination'

const CommonTable = ({
  className,
  title,
  headData,
  bodyData,
  total,
  handleOpen,
  handleEdit,
  handleModel,
  handlePrev,
  params,
  pagination,
  loading,
  handleForw,
  handleDetail,
}) => {

 
  return (
    <>
      {!loading ? (
        <>
          <div className={`card ${className}`}>
            {/* begin::Header */}
            <div className='card-header border-0 pt-5'>
              <h3 className='card-title align-items-start flex-column'>
                <span className='card-label fw-bold fs-3 mb-1'>{title}</span>
                <span className='text-muted mt-1 fw-semibold fs-7'>
                  Over {total} {title}
                </span>
              </h3>
              <section className='d-flex align-items-center'>
                <div
                  className='card-toolbar p-5'
                  data-bs-toggle='tooltip'
                  data-bs-placement='top'
                  data-bs-trigger='hover'
                  title='Click to add a user'
                >
                  <button onClick={handleOpen} className='btn btn-sm btn-light-primary'>
                    <KTIcon iconName='plus' className='fs-3' />
                    New {title}
                  </button>
                </div>
                <div>
                  <button
                    type='button'
                    className='btn btn-clean btn-sm btn-icon btn-icon-primary btn-active-light-primary me-n3'
                    data-kt-menu-trigger='click'
                    data-kt-menu-placement='bottom-end'
                    data-kt-menu-flip='top-end'
                  >
                    <KTIcon iconName='category' className='fs-1 text-primary' />
                  </button>

                  <Dropdown1 />
                </div>
              </section>
            </div>
            {/* end::Header */}

            {/* begin::Body */}
            <div className='card-body py-3'>
              {/* begin::Table container */}
              <div className='table-responsive'>
                {/* begin::Table */}
                <table className='table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4'>
                  {/* begin::Table head */}
                  <thead>
                    <tr className='pe-auto fw-bold text-muted'>
                      {headData &&
                        Array.isArray(headData) &&
                        headData.map((item) => {
                          return (
                            <th key={item} className='text-capitalize min-w-150px'>
                              {item}
                            </th>
                          )
                        })}

                      <th className='min-w-100px text-end'>Actions</th>
                    </tr>
                  </thead>
                  {/* end::Table head */}
                  {/* begin::Table body */}
                  <tbody>
                    {bodyData &&
                      bodyData?.map((item, index) => {
                        return (
                          <tr key={index}>
                            {headData.map((key) => {
                              return (
                                <>
                                  <td  onClick= {()=> handleDetail(item)} key={key} className=' cursor-pointer text-capitalize min-w-150px'>
                                    {item[key]}
                                  </td>
                                </>
                              )
                            })}
                            <td>
                              <div className='d-flex justify-content-end flex-shrink-0'>
                                {/* <span   className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1'>
                                  <KTIcon iconName='switch' className='fs-3' />
                                </span> */}
                                <span
                                  onClick={() => handleEdit(item)}
                                  className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1'
                                >
                                  <KTIcon iconName='pencil' className='fs-3' />
                                </span>
                                <span
                                  onClick={() => handleModel(item)}
                                  className='btn btn-icon btn-bg-light btn-active-color-primary btn-sm'
                                >
                                  <KTIcon iconName='trash' className='fs-3' />
                                </span>
                              </div>
                            </td>
                          </tr>
                        )
                      })}
                  </tbody>
                  {/* end::Table body */}
                </table>
                {/* end::Table */}
              </div>
              <div className='w-full text-end'>
                <Pagination
                  from={(params.start - 1) * pagination?.page}
                  to={params.start * pagination?.page_size}
                  total={pagination?.total}
                  handleForw={handleForw}
                  handlePrev={handlePrev}
                />
                {/* <h6>Pagination</h6> */}
              </div>
              {/* end::Table container */}
            </div>
            {/* begin::Body */}
          </div>
        </>
      ) : (
        <>
          <span className='indicator-progress'>
            Please wait...
            <span className='spinner-border spinner-border-sm align-middle ms-2'></span>
          </span>
        </>
      )}
    </>
  )
}

export {CommonTable}
