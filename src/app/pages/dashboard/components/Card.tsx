/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import {toAbsoluteUrl} from '../../../../_metronic/helpers'

type Props = {
  className: string
  dataClassName: string
  cardTitle: string
  cardSubTitle: string
  isHeader: boolean
  buttonTitle: string
  headImg: string
  data: any
  cardCounts: number
}

const Card: React.FC<Props> = ({
  className,
  cardTitle,
  isHeader,
  buttonTitle,
  data,
  cardSubTitle,
  headImg,
  cardCounts,
}) => {
  return (
    <div className={`card ${className}`}>
      {/* Card Head */}
      {isHeader ? (
        <div className='card-header border-0 pt-5'>
          <h3 className='card-title align-items-start flex-column'>
            <span className='card-label fw-bolder fs-1'>{cardTitle}</span>
            <span className='text-muted fw-semibold fs-7'>{cardSubTitle}</span>
          </h3>
          <img src={toAbsoluteUrl(`/media/dashboard/head/${headImg}`)} alt='' />
        </div>
      ) : (
        <div className='pt-4'></div>
      )}

      {/* Card Data */}
      <div className='card-header px-8 border-0 pt-5'>
        {data?.map((item: any, index: number) => {
          const getWidth = () => {
            if (cardCounts == 1) {
              return 'w-100'
            } else if (cardCounts == 2) {
              return 'w-100 w-md-50'
            } else if (cardCounts == 4) {
              return 'w-100 w-md-50 w-xl-25'
            }
          }
          return (
            <div className={`${getWidth()} px-2`}>
              <div
                key={index}
                className={`border border-gray-300 border-1 border-dashed rounded py-1 px-3 ${
                  data.length - 1 != index && 'mb-4'
                }`}
              >
                <div className='d-flex align-items-center w-100 justify-content-between'>
                  <div className='d-flex align-items-center'>
                    <div className='card-title me-3'>
                      <div className='symbol d-flex justify-content-center align-items-center symbol-35px text-align-center w-35px bg-secondary'>
                        <img
                          src={toAbsoluteUrl(`/media/dashboard/card/${item.icon}`)}
                          className='w-35px'
                          alt=''
                        />   
                      </div>
                    </div>
                    <div className='fs-7 fw-bold'>{item.name}</div>
                  </div>
                  <div className='fw-bolder fs-3'>{item.value}</div>
                </div>
              </div>
            </div>
          )
        })}
      </div>

      {/* button */}
      <div className='card-header border-0 py-5 '>
        <button className='btn btn-sm py-0 btn-light-primary'>{buttonTitle}</button>
      </div>
    </div>
  )
}

export {Card}
