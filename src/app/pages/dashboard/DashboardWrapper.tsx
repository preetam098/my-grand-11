/* eslint-disable jsx-a11y/anchor-is-valid */
import {FC} from 'react'
import {useIntl} from 'react-intl'
import {Card} from './components/Card'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import {LineChart} from './components/LineChart'
import {LineChart2} from './components/LineChart2'
import {DonutChart} from './components/DonutChart'

const DashboardPage: FC = () => {
  // Row -1
  const allUsers = [
    {
      icon: 'android.svg',
      name: 'Android users',
      value: '1000',
    },
    {
      icon: 'apple.svg',
      name: 'Apple users',
      value: '1000',
    },
    {
      icon: 'android.svg',
      name: 'Today Android users',
      value: '1000',
    },
    {
      icon: 'apple.svg',
      name: 'Today Apple users',
      value: '1000',
    },
  ]
  const playingUsers = [
    {
      icon: 'profile-2user.svg',
      name: 'Playing users',
      value: '2034',
    },
    {
      icon: 'profile-2user.svg',
      name: 'Non-Playing users',
      value: '920',
    },
    {
      icon: 'profile-2user.svg',
      name: 'Joined Atleast Once',
      value: '12',
    },
  ]

  // Row -2
  const fundRequests = [
    {
      icon: 'abstract-24.svg',
      name: 'Pending',
      value: '2034',
    },
    {
      icon: 'arrow-two-diagonals.svg',
      name: 'Declined',
      value: '920',
    },
    {
      icon: 'verify.svg',
      name: 'Complete',
      value: '12',
    },
  ]

  const credits = {
    allCredits: [
      {
        icon: 'google-play.svg',
        name: 'Paytm',
        value: '2034',
      },
      {
        icon: 'credit-cart.svg',
        name: 'Payu',
        value: '2034',
      },
      {
        icon: 'google.svg',
        name: 'G-Pay',
        value: '2034',
      },
    ],
    
    todayCredits: [
      {
        icon: 'google-play.svg',
        name: 'Paytm',
        value: '2034',
      },
      {
        icon: 'credit-cart.svg',
        name: 'Payu',
        value: '2034',
      },
      {
        icon: 'google.svg',
        name: 'G-Pay',
        value: '2034',
      },
    ],
  }

  // Row -3
  const row3 = {
    card1: [
      {
        icon: 'share.svg',
        name: 'Joining',
        value: '2034',
      },
      {
        icon: 'cup.svg',
        name: 'Winnig',
        value: '920',
      },
    ],

    card2: [
      {
        icon: 'bank.svg',
        name: 'Deposits',
        value: '2034',
      },
      {
        icon: 'two-credit-cart.svg',
        name: 'Withdraw',
        value: '920',
      },
    ],

    card3: [
      {
        icon: 'graph-up.svg',
        name: 'Todays profit',
        value: '2034',
      },
      {
        icon: 'graph-4.svg',
        name: 'Yesterday profit',
        value: '920',
      },
    ],
  }

  // Row -4
  const allMatches = [
    {
      icon: 'call.svg',
      name: 'Scheduled',
      value: '2034',
    },
    {
      icon: 'loading.svg',
      name: 'Running',
      value: '920',
    },
    {
      icon: 'verify.svg',
      name: 'Completed',
      value: '12',
    },
    {
      icon: 'arrow-two-diagonals.svg',
      name: 'Abondoned',
      value: '12',
    },
  ]
  
  return (
    <>
      {/* Row - 1 */}
      <div className='row gy-5 gx-xl-7'>
        <div className='col-xl-6'>
          <Card
            headImg='people.svg'
            isHeader={true}
            data={allUsers}
            cardCounts={2}
            dataClassName='col-xl-6'
            className='card-xxl-stretch text-green mb-5 mb-xl-8'
            cardTitle='100'
            cardSubTitle='All users'
            buttonTitle='Get All users'
          />
        </div>
        <div className='col-xl-6'>
          <Card
            headImg='icon.svg'
            isHeader={true}
            data={playingUsers}
            cardCounts={2}
            dataClassName='col-xl-6'
            className='card-xxl-stretch mb-5 mb-xl-8'
            cardTitle='79'
            cardSubTitle='Playing users'
            buttonTitle='Playing | Non-Playing'
          />
        </div>
      </div>

      {/* Row - 2 */}
      <div className='row gy-5 gx-xl-7'>
        <div className='col-md-6 col-xl-4'>
          <Card
            headImg='wallet.svg'
            isHeader={true}
            data={fundRequests}
            cardCounts={1}
            dataClassName='col-xl-3'
            className='card-xxl-stretch mb-5 mb-xl-8'
            cardTitle='100'
            cardSubTitle='Todays Fund Requests'
            buttonTitle='Todays Withdraw Requests'
          />
        </div>
        <div className='col-md-6 col-xl-4'>
          <Card
            headImg='wallet.svg'
            isHeader={true}
            dataClassName='col-xl-3'
            data={credits.allCredits}
            cardCounts={1}
            className='card-xxl-stretch mb-5 mb-xl-8'
            cardTitle='79'
            cardSubTitle='All Credits'
            buttonTitle='Paytm | Payu | G-PAY'
          />
        </div>
        <div className='col-md-6 col-xl-4'>
          <Card
            headImg='wallet.svg'
            isHeader={true}
            dataClassName='col-xl-3'
            data={credits.todayCredits}
            cardCounts={1}
            className='card-xxl-stretch mb-5 mb-xl-8'
            cardTitle='100'
            cardSubTitle='Todays Credits'
            buttonTitle='Paytm | Payu | G-PAY'
          />
        </div>
      </div>

      {/* Row - 3 */}
      <div className='row gy-5 gx-xl-7'>
        <div className='col-md-6 col-xl-4'>
          <Card
            headImg='people.svg'
            isHeader={false}
            dataClassName='col-xl-3'
            data={row3.card1}
            cardCounts={1}
            className='card-xxl-stretch mb-5 mb-xl-8'
            cardTitle=''
            cardSubTitle=''
            buttonTitle='Joining & Winning'
          />
        </div>
        <div className='col-md-6 col-xl-4'>
          <Card
            headImg='people.svg'
            isHeader={false}
            dataClassName='col-xl-3'
            data={row3.card2}
            cardCounts={1}
            className='card-xxl-stretch mb-5 mb-xl-8'
            cardTitle=''
            cardSubTitle=''
            buttonTitle='Deposit & Withdraw'
          />
        </div>
        <div className='col-md-6 col-xl-4'>
          <Card
            headImg='people.svg'
            isHeader={false}
            dataClassName='col-xl-3'
            data={row3.card3}
            cardCounts={1}
            className='card-xxl-stretch mb-5 mb-xl-8'
            cardTitle=''
            cardSubTitle=''
            buttonTitle='Profit/Loss'
          />
        </div>
      </div>

      {/* Row - 4 */}
      <div className='row gy-5 gx-xl-7'>
        <div className='col-xl-12'>
          <Card
            headImg='crown-2.svg'
            isHeader={true}
            dataClassName='col-xl-3'
            data={allMatches}
            cardCounts={4}
            className='card-xxl-stretch mb-5 mb-xl-8'
            cardTitle='7389'
            cardSubTitle='All Matches'
            buttonTitle='Get All Matches'
          />
        </div>
      </div>

      {/* Row - 5 */}
      <div className='row gy-5 gx-xl-7'>
        <div className='col-xl-6'>
          <DonutChart
            title='All Users'
            labels={['Total IOS', 'Total Android', 'Verified', 'Not Verified']}
            data={[44, 55, 41, 17]}
            className='card-xxl-stretch'
          />
        </div>
        <div className='col-xl-6'>
          <DonutChart
            title='All Matches'
            labels={['Running ', 'Scheduled', 'Completed', 'Abundent']}
            data={[44, 55, 41, 17]}
            className='card-xxl-stretch'
          />
        </div>
      </div>

      {/* Row - 6 */}
      <div className='row gy-5 gx-xl-7'>
        <div className='col-xl-6 my-8'>
          <LineChart title='All Deposits' className='card-xxl-stretch' />
        </div>
        <div className='col-xl-6 my-8 '>
          <LineChart title='All Withdraw' className='card-xxl-stretch' />
        </div>
      </div>

      {/* Row - 7 */}
      <div className='row gy-5 gx-xl-7'>
        <div className='col-xl-12'>
          <LineChart2 title='Recent 5 days new users' className='card-xxl-stretch' />
        </div>
      </div>
    </>
  )
}

const dashboardBreadCrumbs: Array<PageLink> = [
  {
    title: 'Home',
    path: '/dashboard',
    isSeparator: false,
    isActive: false,
  },
  {
    title: '',
    path: '',
    isSeparator: true,
    isActive: false,
  },
]

const DashboardWrapper: FC = () => {
  const intl = useIntl()
  return (
    <>
      <PageTitle breadcrumbs={dashboardBreadCrumbs}>
        {intl.formatMessage({id: 'MENU.DASHBOARD'})}
      </PageTitle>
      <DashboardPage />
    </>
  )
}

export {DashboardWrapper}
