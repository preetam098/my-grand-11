/* eslint-disable react/jsx-no-target-blank */
import React from 'react'
import {useIntl} from 'react-intl'
import {KTIcon} from '../../../../helpers'
import {SidebarMenuItemWithSub} from './SidebarMenuItemWithSub'
import {SidebarMenuItem} from './SidebarMenuItem'

const SidebarMenuMain = () => {
  const intl = useIntl()

  const menus = [

    // 

     {
      to: '/matches',
      title: 'Matches',
      fontIcon: 'bi-archive',
      icon: 'bi bi-trophy',
      isMultiple: false,
    },
    // ------------ SPORTS PAGE ---------------- //

    {
      to: '/sports',
      icon: 'bi bi-controller',
      title: 'Sports',
      fontIcon: 'bi-layers',
      isMultiple: true,
      multipleData: [
        {
          to: '/sports/index',
          title: 'Sport',
        },
        {
          to: '/sports/sport-format',
          title: 'Sport Format ',
        },
      ],
    },

    // ------------- USER MANAGEMENT --------------- //

    {
      to: '/user-management',
      icon: 'bi bi-people',
      title: 'User management',
      fontIcon: 'bi-layers',
      isMultiple: false,
    },

    // -------------- MODERATOR -------------- //

    {
      to: '/moderator',
      icon: 'bi bi-person-square',
      title: 'Moderator Management',
      fontIcon: 'bi-layers',
      isMultiple: false,
    },


    // -------------- Prize Pool -------------- //

  


    // ---------------------------- //

    // {
    //   to: '/master',
    //   title: 'Master',
    //   fontIcon: 'bi-archive',
    //   icon: 'bi bi-list',
    //   isMultiple: true,
    //   multipleData: [
    //     {
    //       to: '/master/teams',
    //       title: 'Teams',
    //     },
    //     {
    //       to: '/master/players',
    //       title: 'Players',
    //     },
    //     {
    //       to: '/master/points',
    //       title: 'Points',
    //     },

    //     {
    //       to: '/master/series',
    //       title: 'Series',
    //     },
    //   ],
    // },

    // {
    //   to: '/series',
    //   title: 'Series Matches',
    //   fontIcon: 'bi-archive',
    //   icon: 'bi bi-trophy',
    //   isMultiple: true,
    //   multipleData: [
    //     {
    //       to: '/series/list',
    //       title: 'All matches',
    //     },
    //     {
    //       to: '/series/scheduled',
    //       title: 'Scheduled matches',
    //     },

    //     {
    //       to: '/series/todays-assigned',
    //       title: 'Todays Assigned',
    //     },
    //     {
    //       to: '/series/schedule-running',
    //       title: 'Scheduled / Running',
    //     },

    //     {
    //       to: '/series/running-matches',
    //       title: 'Running matches',
    //     },
    //     {
    //       to: '/series/completed-matches',
    //       title: 'Completed matches',
    //     },
    //     {
    //       to: '/series/abondoned-matches',
    //       title: 'Abondoned matches',
    //     },
    //   ],
    // },

    // {
    //   to: '/contest',
    //   title: 'Contest Master',
    //   fontIcon: 'bi-archive',
    //   icon: 'bi bi-database',
    //   isMultiple: true,
    //   multipleData: [
    //     {
    //       to: '/contest/list',
    //       title: 'Contest List',
    //     },
    //     {
    //       to: '/contest/assignments',
    //       title: 'Contest Assignments',
    //     },
    //   ],
    // },

    // {
    //   to: '/current',
    //   title: 'Current Match',
    //   fontIcon: 'bi-archive',
    //   icon: 'bi bi-controller',
    //   isMultiple: true,
    //   multipleData: [
    //     {
    //       to: '/current/set-teams',
    //       title: 'Set Teams',
    //     },
    //     {
    //       to: '/current/score-entry',
    //       title: 'Score Entry',
    //     },
    //   ],
    // },

    // {
    //   to: '/operators',
    //   title: 'Operators',
    //   fontIcon: 'bi-archive',
    //   icon: 'bi bi-person-add',
    //   isMultiple: true,
    //   multipleData: [
    //     {
    //       to: '/operators/registrations',
    //       title: 'Registration',
    //     },
    //     {
    //       to: '/operators/score-writers',
    //       title: 'Score Writers',
    //     },
    //   ],
    // },

    // {
    //   to: '/reports',
    //   title: 'Reports',
    //   fontIcon: 'bi-archive',
    //   icon: 'bi bi-file-earmark',
    //   isMultiple: true,
    //   multipleData: [
    //     {
    //       to: '/reports/contest',
    //       title: 'Contest Report',
    //     },
    //     {
    //       to: '/reports/cont-report',
    //       title: 'Contestant Report',
    //     },
    //     {
    //       to: '/reports/match',
    //       title: 'Match Report',
    //     },
    //     {
    //       to: '/reports/transactions',
    //       title: 'Transactions',
    //     },
    //     {
    //       to: '/reports/employee-logs',
    //       title: 'Employee logs',
    //     },
    //     {
    //       to: '/reports/deposit',
    //       title: 'Deposit',
    //     },
    //     {
    //       to: '/reports/withdraw',
    //       title: 'Withdraw',
    //     },
    //     {
    //       to: '/reports/invoices',
    //       title: 'Invoices',
    //     },
    //   ],
    // },

    // {
    //   to: '/manage-content',
    //   title: 'Manage Content',
    //   fontIcon: 'bi-archive',
    //   icon: 'bi bi-text-left',
    //   isMultiple: true,
    //   multipleData: [
    //     {
    //       to: '/manage-content/about-us',
    //       title: 'About Us',
    //     },
    //     {
    //       to: '/manage-content/contact-us',
    //       title: 'Contact Us',
    //     },
    //     {
    //       to: '/manage-content/help',
    //       title: 'Help',
    //     },
    //     {
    //       to: '/manage-content/invitation',
    //       title: 'Friends Invitation',
    //     },
    //     {
    //       to: '/manage-content/learn-play',
    //       title: 'Learn to play',
    //     },
    //     {
    //       to: '/manage-content/legal',
    //       title: 'Legal',
    //     },
    //   ],
    // },

    // {
    //   to: '/wallet',
    //   title: 'Wallet',
    //   fontIcon: 'bi-archive',
    //   icon: 'bi bi-wallet2',
    //   isMultiple: true,
    //   multipleData: [
    //     {
    //       to: '/wallet/view',
    //       title: 'View Wallet',
    //     },
    //     {
    //       to: '/wallet/payu-transactions',
    //       title: 'Payu Transactions',
    //     },
    //     {
    //       to: '/wallet/paytm-transactions',
    //       title: 'Paytm Transactions',
    //     },
    //     {
    //       to: '/wallet/gpay-transactions',
    //       title: 'G-Pay Transactions',
    //     },
    //   ],
    // },

    // {
    //   to: '/funds',
    //   title: 'Fund Requests',
    //   fontIcon: 'bi-archive',
    //   icon: 'bi bi-wallet',
    //   isMultiple: true,
    //   multipleData: [
    //     {
    //       to: '/funds/initiated',
    //       title: 'Initiated',
    //     },
    //     {
    //       to: '/funds/inprocess',
    //       title: 'Inprocess',
    //     },
    //     {
    //       to: '/funds/approved',
    //       title: 'Approved',
    //     },
    //     {
    //       to: '/funds/declined',
    //       title: 'Declined',
    //     },
    //   ],
    // },

    // {
    //   to: '/advertisement',
    //   title: 'Advertisement',
    //   fontIcon: 'bi-archive',
    //   icon: 'bi bi-megaphone',
    //   isMultiple: false,
    // },

    // {
    //   to: '/notifications',
    //   title: 'Notifications',
    //   fontIcon: 'bi-archive',
    //   icon: 'bi bi-bell',
    //   isMultiple: true,
    //   multipleData: [
    //     {
    //       to: '/notifications/all-users',
    //       title: 'To all users',
    //     },
    //     {
    //       to: '/notifications/single-user',
    //       title: 'To single user',
    //     },
    //     {
    //       to: '/notifications/topic',
    //       title: 'Topic notifications',
    //     },
    //   ],
    // },

    // {
    //   to: '/enquiries',
    //   title: 'Enquiry/Complaints',
    //   fontIcon: 'bi-archive',
    //   icon: 'bi bi-envelope',
    //   isMultiple: false,
    // },

   

    // {
    //   to: '/contest_p_l',
    //   title: 'Contest Profit/Loss',
    //   fontIcon: 'bi-archive',
    //   icon: 'bi bi-currency-rupee',
    //   isMultiple: false,
    // },
    // {
    //   to: '/app-download',
    //   title: 'App downloads',
    //   fontIcon: 'bi-archive',
    //   icon: 'bi bi-download',
    //   isMultiple: false,
    // },

    // {
    //   to: '/settings',
    //   title: 'Settings',
    //   fontIcon: 'bi-archive',
    //   icon: 'bi bi-lightning',
    //   isMultiple: true,
    //   multipleData: [
    //     {
    //       to: '/settings/amount',
    //       title: 'Amount Settings',
    //     },
    //     {
    //       to: '/settings/apk',
    //       title: 'Current Apk',
    //     },
    //   ],
    // },

    // {
    //   to: '/panel-notification',
    //   title: 'Panel Notification',
    //   fontIcon: 'bi-archive',
    //   icon: 'bi bi-app-indicator',
    //   isMultiple: false,
    // },
    // {
    //   to: '/offer',
    //   title: 'Offer Code',
    //   fontIcon: 'bi-archive',
    //   icon: 'bi bi-gift',
    //   isMultiple: false,
    // },
  ]

  return (
    <>
      {/* Dashboard */}
      <SidebarMenuItem
        to='/dashboard'
        icon='bi bi-speedometer2'
        title={intl.formatMessage({id: 'MENU.DASHBOARD'})}
        fontIcon='bi-app-indicator'
      />

      {/* Pages */}
      <div className='menu-item'>
        <div className='menu-content pt-8 pb-2'>
          <span className='menu-section text-muted text-uppercase fs-8 ls-1'>Pages</span>
        </div>
      </div>

      {menus.map((item) => {
        return item.isMultiple ? (
          <SidebarMenuItemWithSub
            to={item.to}
            title={item.title}
            fontIcon={item.fontIcon}
            icon={item.icon}
          >
            {item.multipleData?.map((val) => {
              return <SidebarMenuItem to={val.to} title={val.title} hasBullet={true} />
            })}
          </SidebarMenuItemWithSub>
        ) : (
          <SidebarMenuItem
            to={item.to}
            title={item.title}
            fontIcon={item.fontIcon}
            icon={item.icon}
          />
        )
      })}

      {/* Master */}
      {/* <SidebarMenuItemWithSub to='/master' title='Master' fontIcon='bi-archive' icon='element-plus'>
        <SidebarMenuItem to='/master/teams' title='Teams' hasBullet={true} />
        <SidebarMenuItem to='/master/players' title='Players' hasBullet={true} />
        <SidebarMenuItem to='/master/points' title='Points' hasBullet={true} />
        <SidebarMenuItem to='/master/series' title='Series' hasBullet={true} />
      </SidebarMenuItemWithSub> */}

      {/* Series Matches */}
      {/* <SidebarMenuItemWithSub
        to='/error'
        title='Series Master'
        fontIcon='bi-sticky'
        icon='cross-circle'
      >
        <SidebarMenuItem to='/error/404' title='Error 404' hasBullet={true} />
        <SidebarMenuItem to='/error/500' title='Error 500' hasBullet={true} />
      </SidebarMenuItemWithSub> */}

      {/* Contest Master */}
      <SidebarMenuItemWithSub
        to='/crafted/widgets'
        title='Components'
        icon='element-7'
        fontIcon='bi-layers'
      >
        <SidebarMenuItem to='/crafted/widgets/lists' title='Lists' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/statistics' title='Statistics' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/charts' title='Charts' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/mixed' title='Mixed' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/tables' title='Tables' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/feeds' title='Feeds' hasBullet={true} />
      </SidebarMenuItemWithSub>

      {/* Current Match */}
      {/* <SidebarMenuItemWithSub
        to='/crafted/widgets'
        title='Current Match'
        icon='element-7'
        fontIcon='bi-layers'
      >
        <SidebarMenuItem to='/crafted/widgets/lists' title='Lists' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/statistics' title='Statistics' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/charts' title='Charts' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/mixed' title='Mixed' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/tables' title='Tables' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/feeds' title='Feeds' hasBullet={true} />
      </SidebarMenuItemWithSub> */}

      {/* Operators */}
      {/* <SidebarMenuItemWithSub
        to='/crafted/widgets'
        title='Operators'
        icon='element-7'
        fontIcon='bi-layers'
      >
        <SidebarMenuItem to='/crafted/widgets/lists' title='Lists' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/statistics' title='Statistics' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/charts' title='Charts' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/mixed' title='Mixed' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/tables' title='Tables' hasBullet={true} />
        <SidebarMenuItem to='/crafted/widgets/feeds' title='Feeds' hasBullet={true} />
      </SidebarMenuItemWithSub> */}

      {/* User Mgmt */}
      {/* <SidebarMenuItem
        to='/apps/user-management/ussers'
        icon='abstract-28'
        title='User management'
        fontIcon='bi-layers'
      /> */}
    </>
  )
}

export {SidebarMenuMain}
