import Axios, { AxiosResponse, Method } from "axios";

interface AxiosParams {
  url: string;
  method: Method;
  headers: {
    "Content-Type": string;
    Authorization: any;
  };
  params?: any;
  data?: any;
}

export const callApi = async (url: string, payload: any = {}, method: Method = "GET", params?: any): Promise<AxiosResponse | void> => {
  const AccessToken =  localStorage.getItem("user") || '';
  const {token} =JSON.parse(AccessToken) as any
  if (!token) {
      console.error("Access token not found");
      return;
  }
  const axiosParams: AxiosParams = {
      url,
      method,
      headers: {
        Authorization: `${token}`, 
        "Content-Type": "application/json",
      },
      params: params,
      data: payload,
  };

  if (method === "GET") {
      axiosParams.params = payload;
  } else {
      axiosParams.data = payload;
  }
  try {
      const resp = await Axios(axiosParams);
      console.log(resp,"axios_response")
      return resp;
  } catch (err:any) {
       throw err;
  }
};


// export const callApi = async (url: string, data: any = {}, method: Method = "GET", params?: any): Promise<AxiosResponse | void> => {
//     const AccessToken = localStorage.getItem("token");
//     const axiosParams: AxiosParams = {
//       url,
//       method,
//       headers: {
//         "Content-Type": "application/json",
//         Authorization: AccessToken,
//       },
//       params: params,
//       data: data,
//     };
//     if (method === "GET") {
//       axiosParams.params = data;
//     } else {
//       axiosParams.data = data;
//     }
   
//     try {
//       const resp = await Axios(axiosParams);
//       return resp;
//     } catch (err) {
//       console.log(err);
//       return;
//     }
//   };