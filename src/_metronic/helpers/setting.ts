

const devMode = true;

const baseUrl="http://api.mygrand11.com/"
// const baseUrlNew = base8001


const BaseSetting = {
    name: "gameclass",
    displayName: "gameclass",
    appVersionCode: "1",
    baseUrl,
    // baseUrlNew,
    socketUrl: baseUrl,
    api: `${baseUrl}`,
    shareEndPoint: baseUrl,
    endpoints: {
      ForSignUp : 'auth/account/moderator',
    },
  };
  
  export default BaseSetting;